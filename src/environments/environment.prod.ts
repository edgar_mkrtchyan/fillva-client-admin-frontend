export const environment = {
  production: true,
  appVersion: 'v726Fillva',
  USERDATA_KEY: 'authf649fc9a5f55',
  isMockEnabled: false,
  apiUrl: 'https://dev-back.fillva.com'
};
