import { NgModule, APP_INITIALIZER } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { ClipboardModule } from 'ngx-clipboard';
import { TranslateModule } from '@ngx-translate/core';
import { InlineSVGModule } from 'ng-inline-svg';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthService } from './modules/auth/_services/auth.service';
import { environment } from 'src/environments/environment';
// Highlight JS
import { HighlightModule, HIGHLIGHT_OPTIONS } from 'ngx-highlightjs';
import { SplashScreenModule } from './_fillva/partials/layout/splash-screen/splash-screen.module';
import { ConfigurationService } from './core/services/configuration.service';
import { Constants } from './core/services/constants.service';
import { UserService } from './core/services/user.service';
import { AuthInterceptorService } from './core/services/auth-interceptor.service';
import { CommonFunctions } from './common/components/common.component';
import { AccountService } from './core/services/account.service';
import { UserAccountsService } from './core/services/user-accounts.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LogsService } from './core/services/logs.service';
import { AdminGuardService } from './core/services/admin-guard.service';
import { ManagementGuardService } from './core/services/management-guard.service';
import { EmployeeGuardService } from './core/services/employee-guard.service';
import { CountriesService } from './core/services/countries.service';
import { DashboardService } from './core/services/dashboard.service';
import { ProductivityService } from './core/services/productivity.service';
import { VADashboardService } from './core/services/va-dashboard.service';
import { BusinessInsightsService } from './core/services/business-insights.service';
import { FinanceService } from './core/services/finance.service';

function appInitializer(authService: AuthService) {
  return () => {
    return new Promise((resolve) => {
      authService.getUserByToken().subscribe().add(resolve);
    });
  };
}


@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    SplashScreenModule,
    TranslateModule.forRoot(),
    HttpClientModule,
    HighlightModule,
    ClipboardModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    InlineSVGModule.forRoot(),
    NgbModule,
  ],
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: appInitializer,
      multi: true,
      deps: [AuthService],
    },
    {
      provide: HIGHLIGHT_OPTIONS,
      useValue: {
        coreLibraryLoader: () => import('highlight.js/lib/core'),
        languages: {
          xml: () => import('highlight.js/lib/languages/xml'),
          typescript: () => import('highlight.js/lib/languages/typescript'),
          scss: () => import('highlight.js/lib/languages/scss'),
          json: () => import('highlight.js/lib/languages/json')
        },
      },
    },
    ConfigurationService,
    Constants,
    UserService,
    CommonFunctions,
    AccountService,
    UserAccountsService,
    LogsService,
    AdminGuardService,
    ManagementGuardService,
    EmployeeGuardService,
    CountriesService,
    DashboardService,
    VADashboardService,
    ProductivityService,
    BusinessInsightsService,
    FinanceService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptorService,
      multi: true
    }
  ],
  bootstrap: [AppComponent],
})
export class AppModule { }
