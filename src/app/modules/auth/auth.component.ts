import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CommonFunctions } from 'src/app/common/components/common.component';
import { Constants } from 'src/app/core/services/constants.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {

  today: Date = new Date();

  constructor(private common: CommonFunctions,
              private constants: Constants) { }

  ngOnInit(): void {
  }

  logoUrl() {
    const url = this.common.getUrl();
    if (url.indexOf('autods') > -1) {
      return this.constants.WHITE_LABEL['autods']['auth_logo'];
    } else if (url.indexOf('triplemars') > -1) {
      return this.constants.WHITE_LABEL['triplemars']['auth_logo'];
    } else if (url.indexOf('academie') > -1) {
      return this.constants.WHITE_LABEL['academie']['auth_logo'];
    } else {
      return this.constants.WHITE_LABEL['fillva']['auth_logo'];
    }
  }

}
