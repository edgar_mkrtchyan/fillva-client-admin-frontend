import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UserModel } from '../../_models/user.model';
import { environment } from '../../../../../environments/environment';
import { AuthModel } from '../../_models/auth.model';

const API_USERS_URL = `${environment.apiUrl}`;

@Injectable({
  providedIn: 'root',
})
export class AuthHTTPService {
  constructor(private http: HttpClient) { }

  // public methods
  login(email: string, password: string): Observable<any> {
    const payload = new URLSearchParams();
    payload.append('email', email);
    payload.append('password', password);
    return this.http.post<AuthModel>(`${API_USERS_URL}/api/user/login`, payload.toString(), { headers: new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' }) });
  }

  forgotPassword(email: string): Observable<boolean> {
    const payload = new URLSearchParams();
    payload.append('email', email);
    const httpHeaders = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded'
    });
    return this.http.post<boolean>(`${API_USERS_URL}/api/user/reset-password`, payload.toString(), {
      headers: httpHeaders
    });
  }

  getUserByToken(token): Observable<UserModel> {
    const httpHeaders = new HttpHeaders({
      Authorization: `Bearer ${token}`,
      'Content-Type': 'application/json; charset=utf-8'
    });
    return this.http.get<UserModel>(`${API_USERS_URL}/api/me`, {
      headers: httpHeaders,
    });
  }
}
