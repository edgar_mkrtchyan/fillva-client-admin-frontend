import { AuthModel } from './auth.model';

export class UserModel extends AuthModel {
  userId: number;
  firstName: string;
  lastName: string;
  userName: string;
  email: string;
  permissions: Array<string>;
  numberOfSeats: number;
  daily_start_time: string;
  tick_period: number;

  setUser(user: any) {
    this.userId = user.userId || 0;
    this.firstName = user.firstName || '';
    this.lastName = user.lastName || '';
    this.userName = user.userName || '';
    this.permissions = user.permissions || [];
    this.numberOfSeats = user.numberOfSeats || 0;
    this.daily_start_time = user.daily_start_time || '';
    this.tick_period = user.tick_period || 0;
  }
}
