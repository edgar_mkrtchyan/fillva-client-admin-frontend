
import {catchError, map} from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { BaseService } from '../../common/classes/base-web.service';
import { ConfigurationService } from './configuration.service';
import { UserService } from './user.service';
import { BaseMetrics } from 'src/app/common/interfaces/base-metrics.interface';
import { RecentKPIHit } from 'src/app/common/interfaces/recent-kpi-hit.interface';
import { TimeSpent } from 'src/app/common/interfaces/time-spent.interface';
import { Alerts } from 'src/app/common/interfaces/alerts.interface';
import { ActivityPercentage } from 'src/app/common/interfaces/activity-percentage.interface';
import { RecentActivity } from 'src/app/common/interfaces/recent-activity.interface';
import { LatestKPIS } from 'src/app/common/interfaces/latest-kpis.interface';
import { ManagerVACurrentState } from 'src/app/common/interfaces/va-current-state.interface';

@Injectable()

export class DashboardService extends BaseService {

    constructor(public config: ConfigurationService, public httpClient: HttpClient, private user: UserService) {
        super(config);
    }

    getBaseMetrics(payload: URLSearchParams): Observable<any> {
        return this.httpClient.post(this.endpoint('api', 'dashboard', 'manager', 'base-metrics'),
                                    payload.toString(), this.httpOptionsAuth()).pipe(
                map((response: HttpResponse<BaseMetrics>) => response),
                catchError((error: HttpErrorResponse) => this.handleHttpError(error)));
    }

    getBaseMetricsForVA(payload: URLSearchParams, userId: number): Observable<any> {
        return this.httpClient.post(this.endpoint('api', 'dashboard', 'manager', 'base-metrics', userId.toString()),
            payload.toString(), this.httpOptionsAuth()).pipe(
                map((response: HttpResponse<BaseMetrics>) => response),
                catchError((error: HttpErrorResponse) => this.handleHttpError(error)));
    }

    getRecentKPIHit(payload: URLSearchParams): Observable<any> {
        return this.httpClient.post(this.endpoint('api', 'dashboard', 'manager',
                                                  'recent-kpi-hit'), payload.toString(), this.httpOptionsAuth()).pipe(
                map((response: HttpResponse<RecentKPIHit>) => response),
                catchError((error: HttpErrorResponse) => this.handleHttpError(error)));
    }

    getProductivityData(payload: URLSearchParams): Observable<any> {
        return this.httpClient.post(this.endpoint('api', 'dashboard', 'manager', 'productivity'),
                                    payload.toString(), this.httpOptionsAuth()).pipe(
            map((response: HttpResponse<TimeSpent>) => response),
            catchError((error: HttpErrorResponse) => this.handleHttpError(error)));
    }

    getAlertsData(): Observable<any> {
        return this.httpClient.get(this.endpoint('api', 'dashboard', 'manager', 'alerts'), this.httpOptionsAuth()).pipe(
                map((response: HttpResponse<Alerts>) => response),
                catchError((error: HttpErrorResponse) => this.handleHttpError(error)));
    }

    getActivityPercentage(payload: URLSearchParams): Observable<any> {
        return this.httpClient.post(this.endpoint('api', 'dashboard', 'manager', 'activity'),
                                    payload.toString(), this.httpOptionsAuth()).pipe(
            map((response: HttpResponse<ActivityPercentage>) => response),
            catchError((error: HttpErrorResponse) => this.handleHttpError(error)));
    }

    getRecentActivity(): Observable<any> {
        return this.httpClient.post(this.endpoint('api', 'dashboard', 'manager', 'recent-activity'), this.httpOptionsAuth()).pipe(
            map((response: HttpResponse<RecentActivity>) => response),
                catchError((error: HttpErrorResponse) => this.handleHttpError(error)));
    }

    getLatestKPIs(count: number): Observable<any> {
        return this.httpClient.get(this.endpoint('api', 'dashboard', 'manager', 'latest-kpis', count.toString()), this.httpOptions()).pipe(
            map((response: HttpResponse<LatestKPIS>) => response),
            catchError((error: HttpErrorResponse) => this.handleHttpError(error)));
    }

    getBaseMetricsPerVa(payload: URLSearchParams, userId: number): Observable<any> {
        return this.httpClient.post(this.endpoint('api', 'dashboard', 'manager', 'base-metrics', userId.toString()),
            payload.toString(), this.httpOptionsAuth()).pipe(
                map((response: HttpResponse<BaseMetrics>) => response),
                catchError((error: HttpErrorResponse) => this.handleHttpError(error)));
    }

    getCurrentlyActiveVAs(): Observable<any> {
        return this.httpClient.get(this.endpoint('api', 'dashboard', 'manager', 'current-state'), this.httpOptions()).pipe(
            map((response: HttpResponse<ManagerVACurrentState>) => response),
            catchError((error: HttpErrorResponse) => this.handleHttpError(error)));
    }
}
