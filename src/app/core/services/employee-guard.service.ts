import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { UserService } from './user.service';
import { CommonFunctions } from '../../common/components/common.component';

@Injectable()

export class EmployeeGuardService implements CanActivate {

    constructor(public user: UserService,
                public router: Router,
                private common: CommonFunctions) { }

    canActivate(): boolean {
        const isVa = this.common.isVA();
        if (!isVa) {
            this.router.navigateByUrl('/dashboard');
            return false;
        }
        return true;
    }
}
