import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';

@Injectable()

export class ConfigurationService {

    constructor() {

    }

    get restfulApiBase(): string {
        return environment.apiUrl;
    }

    get isDev(): boolean {
        return !environment.production;
    }

}
