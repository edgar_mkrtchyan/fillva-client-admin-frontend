
import { catchError, map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { BaseService } from '../../common/classes/base-web.service';
import { ConfigurationService } from './configuration.service';
import { UrlParams } from '../../common/classes/url-params.class';
import { UserService } from './user.service';
import { UserObject } from 'src/app/common/interfaces/user.interface';

@Injectable()

export class AccountService extends BaseService {

    constructor(public config: ConfigurationService, public httpClient: HttpClient, private user: UserService) {
        super(config);
    }

    refreshToken(payload: URLSearchParams): Observable<any> {
        return this.httpClient.post(this.endpoint('api', 'token'), payload.toString(), this.httpOptionsAuth()).pipe(
            map((response: HttpResponse<object>) => response),
            catchError((error: HttpErrorResponse) => this.handleHttpError(error)));
    }

    getUserData(): Observable<any> {
        return this.httpClient.get(this.endpoint('api', 'me'), this.httpOptions()).pipe(
            map((response: HttpResponse<UserObject>) => response),
            catchError((error: HttpErrorResponse) => this.handleHttpError(error)));
    }

    logoutUser(): Observable<any> {
        return this.httpClient.post(this.endpoint('api', 'user', 'logout'), this.httpOptions()).pipe(
            map((response: HttpResponse<UserObject>) => response),
            catchError((error: HttpErrorResponse) => this.handleHttpError(error)));
    }

    uploadImage(formData: FormData, userId: number): Observable<any> {
        return this.httpClient.post(this.endpoint('api', 'user', userId.toString(), 'image-upload'), formData).pipe(
            map((response: HttpResponse<object>) => response),
            catchError((error: HttpErrorResponse) => this.handleHttpError(error)));
    }

    deleteProfileImage(userId: number): Observable<any> {
        return this.httpClient.delete(this.endpoint('api', 'user', userId.toString(), 'image-upload')).pipe(
            map((response: HttpResponse<object>) => response),
            catchError((error: HttpErrorResponse) => this.handleHttpError(error)));
    }
}
