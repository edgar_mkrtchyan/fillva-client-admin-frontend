
import {catchError, map} from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { BaseService } from '../../common/classes/base-web.service';
import { ConfigurationService } from './configuration.service';
import { UserService } from './user.service';
import { TimeSpentPerKPI } from 'src/app/common/interfaces/time-spent-per-kpi.interface';
import { CostPerKPI } from 'src/app/common/interfaces/cost-per-kpi.interface';
import { TimeInvestedInEachKPI } from 'src/app/common/interfaces/time-invested-in-each-kpi.interface';
import { TopKPIS } from 'src/app/common/interfaces/top-kpis.interface';
import { KPISCount } from 'src/app/common/interfaces/kpis-count.interface';

@Injectable()

export class BusinessInsightsService extends BaseService {

    constructor(public config: ConfigurationService, public httpClient: HttpClient, private user: UserService) {
        super(config);
    }

    getTimeSpentPerKPI(payload: URLSearchParams): Observable<any> {
        return this.httpClient.post(this.endpoint('api', 'dashboard', 'manager', 'time-spent-per-kpi'),
                                    payload.toString(), this.httpOptionsAuth()).pipe(
            map((response: HttpResponse<TimeSpentPerKPI>) => response),
            catchError((error: HttpErrorResponse) => this.handleHttpError(error)));
    }

    getCostPerKPI(payload: URLSearchParams): Observable<any> {
        return this.httpClient.post(this.endpoint('api', 'dashboard', 'manager', 'cost-per-kpi'),
            payload.toString(), this.httpOptionsAuth()).pipe(
                map((response: HttpResponse<CostPerKPI>) => response),
                catchError((error: HttpErrorResponse) => this.handleHttpError(error)));
    }

    getTimeInvestedInEachKPI(payload: URLSearchParams): Observable<any> {
        return this.httpClient.post(this.endpoint('api', 'dashboard', 'manager', 'time-invested-in-each-kpi'),
            payload.toString(), this.httpOptionsAuth()).pipe(
                map((response: HttpResponse<TimeInvestedInEachKPI>) => response),
                catchError((error: HttpErrorResponse) => this.handleHttpError(error)));
    }

    getTopKPIS(payload: URLSearchParams): Observable<any> {
        return this.httpClient.post(this.endpoint('api', 'dashboard', 'manager', 'top-kpis'),
            payload.toString(), this.httpOptionsAuth()).pipe(
                map((response: HttpResponse<TopKPIS>) => response),
                catchError((error: HttpErrorResponse) => this.handleHttpError(error)));
    }

    getKPISCount(payload: URLSearchParams): Observable<any> {
        return this.httpClient.post(this.endpoint('api', 'dashboard', 'manager', 'kpis-count'),
            payload.toString(), this.httpOptionsAuth()).pipe(
                map((response: HttpResponse<KPISCount>) => response),
                catchError((error: HttpErrorResponse) => this.handleHttpError(error)));
    }
}
