import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { UserService } from './user.service';
import { CommonFunctions } from '../../common/components/common.component';

@Injectable()

export class AdminGuardService implements CanActivate {

    constructor(public user: UserService,
                public router: Router,
                private common: CommonFunctions) { }

    canActivate(): boolean {
        const isAdmin = this.common.isAdmin();
        if (!isAdmin) {
            this.router.navigateByUrl('/dashboard');
            return false;
        }
        return true;
    }
}
