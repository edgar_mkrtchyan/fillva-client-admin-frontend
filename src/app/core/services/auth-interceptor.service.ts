import {take, filter, switchMap, catchError, finalize} from 'rxjs/operators';
import { HttpHandler, HttpInterceptor, HttpRequest, HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable ,  throwError } from 'rxjs';
import { BehaviorSubject, Subscription } from '../../../../node_modules/rxjs';
import { JwtHelperService } from '@auth0/angular-jwt';
import { CommonFunctions } from 'src/app/common/components/common.component';
import { UserService } from './user.service';
import { AccountService } from './account.service';
import { TokenObject } from 'src/app/common/interfaces/token.interface';

@Injectable()
export class AuthInterceptorService implements HttpInterceptor {

  isRefreshingToken: boolean;
  tokenSubject: BehaviorSubject<string> = new BehaviorSubject<string>(null);
  jwtHelper = new JwtHelperService();

  constructor(private user: UserService, private ws: AccountService,
              private common: CommonFunctions) {
    this.isRefreshingToken = false;
  }

  addToken(req: HttpRequest<any>, token: string): HttpRequest<any> {
    return req.clone({ setHeaders: { Authorization: 'Bearer ' + token } });
  }

  refreshTheToken(): Observable<TokenObject> {
    const payload = new URLSearchParams();
    payload.append('refresh_token', this.user.refreshToken);

    return this.ws.refreshToken(payload);
  }

  getMainAuthToken() {
    return this.user.accessToken;
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<any> {
    if (this.getMainAuthToken() && !req.url.includes('token') && !req.url.includes('login')) {
      return next.handle(this.addToken(req, this.getMainAuthToken())).pipe(
        catchError(error => {
          if (error instanceof HttpErrorResponse) {
            const status = (error as HttpErrorResponse).status;
            switch ((error as HttpErrorResponse).status) {
              case 400:
                return this.handle400Error(error);
              case 401:
                return this.handle401Error(req, next);
              case 403:
                return this.handle403Error(error);
              case 404:
                return this.handle404Error(error);
            }
          } else {
            return throwError(error);
          }
        }));
    } else {
      return next.handle(req);
    }
  }

  handle401Error(req: HttpRequest<any>, next: HttpHandler) {
    // Logging the user out if the refresh token is expired
    if (!this.user.refreshToken || this.user.refreshToken === 'undefined' || this.jwtHelper.isTokenExpired(this.user.refreshToken)) {
      this.logoutUser();
    }
    // Logging the user out if the refresh token is expired
    if (!this.isRefreshingToken) {
      this.isRefreshingToken = true;

      this.tokenSubject.next(null);

      return this.refreshTheToken().pipe(
        switchMap(refreshResponse => {
          if (refreshResponse) {
            this.user.accessToken = refreshResponse.access_token;
            this.user.refreshToken = refreshResponse.refresh_token;
            this.tokenSubject.next(refreshResponse.access_token);
            return next.handle(this.addToken(req, refreshResponse.access_token));
          } else {
            return this.logoutUser();
          }
        }),
        catchError(error => {
          if (error.status === 403) {
            this.common.logout();
            return throwError(error);
          }
        }),
        finalize(() => {
          this.isRefreshingToken = false;
      }));
    } else {
      return this.tokenSubject.pipe(
        filter(token => token != null),
        take(1),
        switchMap(token => {
          return next.handle(this.addToken(req, token));
        }));
    }
  }

  handle400Error(error) {
    if (error && error.status === 400 && error.error && error.error.error === 'invalid_grant') {
      return this.logoutUser();
    }

    return throwError(error);
  }

  handle404Error(error) {
    if (error && error.status === 404) {
      return throwError(error);
    }
  }

  logoutUser() {
    this.common.logout(true);
    return throwError('');
  }

  handle403Error(error) {
    if (error && error.status === 403) {
      return this.logoutUser();
    }
  }
}
