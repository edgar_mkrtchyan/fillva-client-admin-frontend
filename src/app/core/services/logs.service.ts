
import {catchError, map} from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { BaseService } from '../../common/classes/base-web.service';
import { ConfigurationService } from './configuration.service';
import { UserService } from './user.service';
import { Events } from 'src/app/common/interfaces/events.interface';
import { CustomKPIs } from 'src/app/common/interfaces/custom-kpis.interface';

@Injectable()

export class LogsService extends BaseService {

    constructor(public config: ConfigurationService, public httpClient: HttpClient, private user: UserService) {
        super(config);
    }

    getLogs(kpiOnly: string): Observable<any> {
        return this.httpClient.get(this.endpointWithGETParam(
                                                                {
                                                                    name: 'only_kpi',
                                                                    value: kpiOnly
                                                                },
                                                                'api', 'logs'), this.httpOptions()).pipe(
            map((response: HttpResponse<Events>) => response),
            catchError((error: HttpErrorResponse) => this.handleHttpError(error)));
    }

    addCustomKPI(payload: URLSearchParams): Observable<any> {
        return this.httpClient.post(this.endpoint('api', 'user', 'add-custom-kpi-event'), payload.toString(), this.httpOptionsAuth()).pipe(
            map((response: HttpResponse<object>) => response),
            catchError((error: HttpErrorResponse) => this.handleHttpError(error)));
    }

    getCustomKPIs(): Observable<any> {
        return this.httpClient.get(this.endpoint('api', 'user', 'custom-kpis-events'), this.httpOptions()).pipe(
                map((response: HttpResponse<CustomKPIs>) => response),
                catchError((error: HttpErrorResponse) => this.handleHttpError(error)));
    }

    deleteCustomKPI(kpiId: number): Observable<any> {
        return this.httpClient.delete(this.endpoint('api', 'user', 'custom-kpi-event', kpiId.toString()), this.httpOptions()).pipe(
            map((response: HttpResponse<any>) => response),
            catchError((error: HttpErrorResponse) => this.handleHttpError(error)));
    }

    editCustomKPI(kpiId: number, payload: URLSearchParams): Observable<any> {
        return this.httpClient.put(this.endpoint('api', 'user', 'custom-kpi-event', kpiId.toString()),
            payload.toString(), this.httpOptionsAuth()).pipe(
                map((response: HttpResponse<object>) => response),
                catchError((error: HttpErrorResponse) => this.handleHttpError(error)));
    }
}
