import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { UserService } from './user.service';
import { CommonFunctions } from '../../common/components/common.component';

@Injectable()

export class ManagementGuardService implements CanActivate {

    constructor(public user: UserService,
                public router: Router,
                private common: CommonFunctions) { }

    canActivate(): boolean {
        const isAdmin = this.common.isAdmin();
        const isManager = this.common.isManager();
        if (!isAdmin && !isManager) {
            this.router.navigateByUrl('/dashboard');
            return false;
        }
        return true;
    }
}
