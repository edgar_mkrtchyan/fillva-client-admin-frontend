import { Injectable } from '@angular/core';
import { ChatColors } from 'src/app/common/interfaces/chat-colors.interface';
import { WidgetsList } from 'src/app/common/interfaces/widgets-list.interface';

@Injectable()

export class Constants {
    readonly PERMISSIONS_LIST: Array<string> = ['ADMIN', 'MANAGER', 'VA'];
    readonly CHART_COLORS: ChatColors = {
        primary: '#6993FF',
        success: '#1BC5BD',
        info: '#8950FC',
        warning: '#FFA800',
        danger: '#F64E60',
        gray: '#cccccc',
        brand: '#ff7f06'
    };
    readonly MONTHS_MAP: object = {
        0: 'January',
        1: 'February',
        2: 'March',
        3: 'April',
        4: 'May',
        5: 'June',
        6: 'July',
        7: 'August',
        8: 'September',
        9: 'October',
        10: 'November',
        11: 'December'
    };
    readonly HOURS: Array<string> = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11'];
    readonly MINUTES: Array<string> = ['00', '10', '20', '30', '40', '50'];
    readonly AMPM: Array<string> = ['AM', 'PM'];
    readonly WIDGETS_LIST: Array<WidgetsList> = [
        {
            key: 'recent_kpi_hit',
            label: 'Recent KPI Hit',
            area: 'dashboard'
        },
        {
            key: 'time_invested_in_each_kpi',
            label: 'Time Invested in Each KPI',
            area: 'insights'
        },
        {
            key: 'productivity',
            label: 'Productivity',
            area: 'dashboard'
        },
        {
            key: 'activity',
            label: 'Activity',
            area: 'dashboard'
        },
        {
            key: 'time_spent_per_kpi',
            label: 'Time Spent Per KPI',
            area: 'insights'
        },
        {
            key: 'cost_per_kpi',
            label: 'Cost per KPI',
            area: 'insights'
        },
        {
            key: 'latest_kpis',
            label: 'Latest KPI-s',
            area: 'dashboard'
        },
        {
            key: 'alerts',
            label: 'Alerts',
            area: 'dashboard'
        },
        {
            key: 'top_five_kpis',
            label: 'Top 5 KPI-s',
            area: 'insights'
        },
        {
            key: 'kpis_count',
            label: 'KPI-s Count',
            area: 'insights'
        },
        {
            key: 'recent_activity',
            label: 'Recent Activity',
            area: 'dashboard'
        },
        {
            key: 'manager_current_state',
            label: 'Currently Active VA-s',
            area: 'dashboard'
        },
    ];
    readonly IMAGE_PLACEHOLDER: string = '../../../../../../../assets/media/svg/avatars/001-boy.svg';
    readonly WHITE_LABEL: object = {
        autods: {
            auth_logo: 'assets/media/logos/logo-letter-13-autods.png',
            aside_logo: './assets/media/logos/logo-light-autods.png',
            splash_logo: './assets/media/logos/logo-dark-autods.png',
            favicon: './assets//media//logos//autods-favicon.png',
            name: 'AutoDS',
            url: 'https://www.autods.com/'
        },
        triplemars: {
            auth_logo: 'assets/media/logos/logo-letter-13-triplemars.png',
            aside_logo: './assets/media/logos/logo-light-triplemars.png',
            splash_logo: './assets/media/logos/logo-dark-triplemars.png',
            favicon: './assets//media//logos//triplemars-favicon.png',
            name: 'Triplemars',
            url: 'https://www.triplemars.com/'
        },
        academie: {
            auth_logo: 'assets/media/logos/academie/logo-letter-13-academie.png',
            aside_logo: './assets/media/logos/academie/logo-light-academie.png',
            splash_logo: './assets/media/logos/academie/logo-dark-academie.png',
            favicon: './assets//media//logos//academie//academie-favicon.jpeg',
            name: 'Ecommerce Academie',
            url: 'https://www.ecommerceacademie.com/'
        },
        fillva: {
            auth_logo: 'assets/media/logos/logo-letter-13.png',
            aside_logo: './assets/media/logos/logo-light.png',
            splash_logo: './assets/media/logos/logo-dark.png',
            favicon: './assets//media//logos//favicon.ico',
            name: 'Fillva',
            url: 'https://www.fillva.com/'
        }
    };
}
