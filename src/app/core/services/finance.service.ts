
import {catchError, map} from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { BaseService } from '../../common/classes/base-web.service';
import { ConfigurationService } from './configuration.service';
import { UserService } from './user.service';
import { CostPerTeam } from 'src/app/common/interfaces/cost-per-team.interface';
import { CostPerEmployee } from 'src/app/common/interfaces/cost-per-employee.interface';
import { FinanceBaseMetrics } from 'src/app/common/interfaces/finance-base-metrics.interface';

@Injectable()

export class FinanceService extends BaseService {

    constructor(public config: ConfigurationService, public httpClient: HttpClient, private user: UserService) {
        super(config);
    }

    getCostPerTeam(payload: URLSearchParams): Observable<any> {
        return this.httpClient.post(this.endpoint('api', 'manager', 'finance', 'cost-per-team'),
                                    payload.toString(), this.httpOptionsAuth()).pipe(
            map((response: HttpResponse<CostPerTeam>) => response),
            catchError((error: HttpErrorResponse) => this.handleHttpError(error)));
    }

    getCostPerEmployee(payload: URLSearchParams): Observable<any> {
        return this.httpClient.post(this.endpoint('api', 'manager', 'finance', 'cost-per-employee'),
            payload.toString(), this.httpOptionsAuth()).pipe(
                map((response: HttpResponse<CostPerEmployee>) => response),
                catchError((error: HttpErrorResponse) => this.handleHttpError(error)));
    }

    getFinanceBaseMetrics(payload: URLSearchParams): Observable<any> {
        return this.httpClient.post(this.endpoint('api', 'manager', 'finance', 'metrics'),
            payload.toString(), this.httpOptionsAuth()).pipe(
                map((response: HttpResponse<FinanceBaseMetrics>) => response),
                catchError((error: HttpErrorResponse) => this.handleHttpError(error)));
    }
}
