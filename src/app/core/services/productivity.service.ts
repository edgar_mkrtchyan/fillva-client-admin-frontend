
import {catchError, map} from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { BaseService } from '../../common/classes/base-web.service';
import { ConfigurationService } from './configuration.service';
import { UserService } from './user.service';
import { TimeSpentOnSites } from 'src/app/common/interfaces/time-spent-on-sites.interface';
import { ProductivityStatistics } from 'src/app/common/interfaces/productivity-statistics.interface';
import { ProductivityChartData } from 'src/app/common/interfaces/productivity-chart.interface';
import { RecentActivity } from 'src/app/common/interfaces/recent-activity.interface';
import { LatestKPIS } from 'src/app/common/interfaces/latest-kpis.interface';

@Injectable()

export class ProductivityService extends BaseService {

    constructor(public config: ConfigurationService, public httpClient: HttpClient, private user: UserService) {
        super(config);
    }

    getTimeSpentOnWebsites(payload: URLSearchParams, employeeId: number): Observable<any> {
        return this.httpClient.post(this.endpoint('api', 'productivity', 'employee', employeeId.toString(), 'time-spent-on-websites'),
                                    payload.toString(), this.httpOptionsAuth()).pipe(
            map((response: HttpResponse<TimeSpentOnSites>) => response),
            catchError((error: HttpErrorResponse) => this.handleHttpError(error)));
    }

    getStatistics(payload: URLSearchParams, employeeId: number): Observable<any> {
        return this.httpClient.post(this.endpoint('api', 'productivity', 'statistics', 'employee', employeeId.toString()),
            payload.toString(), this.httpOptionsAuth()).pipe(
                map((response: HttpResponse<ProductivityStatistics>) => response),
                catchError((error: HttpErrorResponse) => this.handleHttpError(error)));
    }

    getProductivityChartData(payload: URLSearchParams, employeeId: number): Observable<any> {
        return this.httpClient.post(this.endpoint('api', 'productivity', 'employee', employeeId.toString(), 'activity-chart'),
            payload.toString(), this.httpOptionsAuth()).pipe(
                map((response: HttpResponse<ProductivityChartData>) => response),
                catchError((error: HttpErrorResponse) => this.handleHttpError(error)));
    }

    getRecentActivity(payload: URLSearchParams, employeeId: number): Observable<any> {
        return this.httpClient.post(this.endpoint('api', 'manager', 'employee', employeeId.toString(), 'screenshots'),
                                    payload.toString(), this.httpOptionsAuth()).pipe(
            map((response: HttpResponse<RecentActivity>) => response),
            catchError((error: HttpErrorResponse) => this.handleHttpError(error)));
    }

    getLatestKPIsPerVA(payload: URLSearchParams, userId: number): Observable<any> {
        return this.httpClient.post(this.endpoint('api', 'productivity', 'employee', userId.toString(), 'latest-activity'),
                                    payload.toString(), this.httpOptionsAuth()).pipe(
            map((response: HttpResponse<LatestKPIS>) => response),
            catchError((error: HttpErrorResponse) => this.handleHttpError(error)));
    }
}
