
import {catchError, map} from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { BaseService } from '../../common/classes/base-web.service';
import { ConfigurationService } from './configuration.service';
import { UserObject } from '../../common/interfaces/user.interface';
import { Permissions } from '../../common/interfaces/permissions.interface';
import { TeamObject } from 'src/app/common/interfaces/team.interface';

@Injectable()

export class UserAccountsService extends BaseService {

    constructor(public config: ConfigurationService, public httpClient: HttpClient) {
        super(config);
    }

    getUsers(): Observable<any> {
        return this.httpClient.get(this.endpoint('api', 'users'), this.httpOptions()).pipe(
            map((response: HttpResponse<UserObject>) => response),
            catchError((error: HttpErrorResponse) => this.handleHttpError(error)));
    }

    getManagerUsers(managerId: number): Observable<any> {
        return this.httpClient.get(this.endpoint('api', 'users', 'manager', managerId.toString()), this.httpOptions()).pipe(
            map((response: HttpResponse<UserObject>) => response),
            catchError((error: HttpErrorResponse) => this.handleHttpError(error)));
    }

    changeUserStatus(userId: number, payload: URLSearchParams): Observable<any> {
        return this.httpClient.put(this.endpoint('api', 'user', userId.toString()),
                                   payload.toString(), this.httpOptionsAuth()).pipe(
            map((response: HttpResponse<object>) => response),
            catchError((error: HttpErrorResponse) => this.handleHttpError(error)));
    }

    createUser(payload: URLSearchParams): Observable<any> {
        return this.httpClient.post(this.endpoint('api', 'admin', 'user'), payload.toString(), this.httpOptionsAuth()).pipe(
            map((response: HttpResponse<object>) => response),
            catchError((error: HttpErrorResponse) => this.handleHttpError(error)));
    }

    getUserPermissions(userId: number): Observable<any> {
        return this.httpClient.get(this.endpoint('api', 'user', userId.toString(), 'permissions'), this.httpOptions()).pipe(
            map((response: HttpResponse<Permissions>) => response),
            catchError((error: HttpErrorResponse) => this.handleHttpError(error)));
    }

    updateUserData(userId: number, payload: URLSearchParams): Observable<any> {
        return this.httpClient.put(this.endpoint('api', 'user', userId.toString()),
                                   payload.toString(), this.httpOptionsAuth()).pipe(
            map((response: HttpResponse<object>) => response),
            catchError((error: HttpErrorResponse) => this.handleHttpError(error)));
    }

    setUserPermissions(userId: number, payload: URLSearchParams): Observable<any> {
        return this.httpClient.put(this.endpoint('api', 'user', userId.toString(), 'permissions'),
                                    payload.toString(), this.httpOptionsAuth()).pipe(
            map((response: HttpResponse<object>) => response),
            catchError((error: HttpErrorResponse) => this.handleHttpError(error)));
    }

    deleteUser(userId: number): Observable<any> {
        return this.httpClient.delete(this.endpoint('api', 'user', userId.toString()), this.httpOptionsAuth()).pipe(
            map((response: HttpResponse<object>) => response),
            catchError((error: HttpErrorResponse) => this.handleHttpError(error)));
    }

    changeUserPasswordByAdmin(payload: URLSearchParams): Observable<any> {
        return this.httpClient.post(this.endpoint('api', 'admin', 'password'), payload.toString(), this.httpOptionsAuth()).pipe(
            map((response: HttpResponse<object>) => response),
            catchError((error: HttpErrorResponse) => this.handleHttpError(error)));
    }

    updateUserDataByUser(userId: number, payload: URLSearchParams): Observable<any> {
        return this.httpClient.put(this.endpoint('api', 'user', userId.toString()), payload.toString(), this.httpOptionsAuth()).pipe(
            map((response: HttpResponse<object>) => response),
            catchError((error: HttpErrorResponse) => this.handleHttpError(error)));
    }

    changeUserPasswordByUser(userId: number, payload: URLSearchParams): Observable<any> {
        return this.httpClient.post(this.endpoint('api', 'user', userId.toString(), 'password'),
                                                   payload.toString(), this.httpOptionsAuth()).pipe(
            map((response: HttpResponse<object>) => response),
            catchError((error: HttpErrorResponse) => this.handleHttpError(error)));
    }

    getTeams(managerId: number): Observable<any> {
        return this.httpClient.get(this.endpoint('api', 'manager', managerId.toString(), 'teams'), this.httpOptions()).pipe(
            map((response: HttpResponse<TeamObject>) => response),
            catchError((error: HttpErrorResponse) => this.handleHttpError(error)));
    }

    createTeam(payload: URLSearchParams, managerId: number): Observable<any> {
        // tslint:disable-next-line:max-line-length
        return this.httpClient.post(this.endpoint('api', 'manager', managerId.toString(), 'teams'), payload.toString(), this.httpOptionsAuth()).pipe(
            map((response: HttpResponse<object>) => response),
            catchError((error: HttpErrorResponse) => this.handleHttpError(error)));
    }

    updateTeam(payload: URLSearchParams, teamId: number): Observable<any> {
        return this.httpClient.put(this.endpoint('api', 'manager', 'teams', teamId.toString()),
            payload.toString(), this.httpOptionsAuth()).pipe(
                map((response: HttpResponse<object>) => response),
                catchError((error: HttpErrorResponse) => this.handleHttpError(error)));
    }

    deleteTeam(teamId: number): Observable<any> {
        return this.httpClient.delete(this.endpoint('api', 'manager', 'teams', teamId.toString()), this.httpOptionsAuth()).pipe(
            map((response: HttpResponse<object>) => response),
            catchError((error: HttpErrorResponse) => this.handleHttpError(error)));
    }

    addAllowedWebSites(payload: URLSearchParams, teamId: number): Observable<any> {
        // tslint:disable-next-line:max-line-length
        return this.httpClient.put(this.endpoint('api', 'manager', 'teams', teamId.toString(), 'allowed-websites'), payload.toString(), this.httpOptionsAuth()).pipe(
            map((response: HttpResponse<object>) => response),
            catchError((error: HttpErrorResponse) => this.handleHttpError(error)));
    }

    addEmployeeToTeam(teamId: number, employeeId: string): Observable<any> {
        return this.httpClient.post(this.endpoint('api', 'manager', 'teams', teamId.toString(), 'users', employeeId),
                this.httpOptionsAuth()).pipe(
                map((response: HttpResponse<object>) => response),
                catchError((error: HttpErrorResponse) => this.handleHttpError(error)));
    }

    removeEmployeeFromTeam(teamId: number, employeeId: string): Observable<any> {
        return this.httpClient.delete(this.endpoint('api', 'manager', 'teams', teamId.toString(), 'users', employeeId),
            this.httpOptionsAuth()).pipe(
                map((response: HttpResponse<object>) => response),
                catchError((error: HttpErrorResponse) => this.handleHttpError(error)));
    }

    getTeamEmployees(teamId: number): Observable<any> {
        return this.httpClient.get(this.endpoint('api', 'manager', 'teams', teamId.toString()), this.httpOptions()).pipe(
            map((response: HttpResponse<UserObject>) => response),
            catchError((error: HttpErrorResponse) => this.handleHttpError(error)));
    }
}
