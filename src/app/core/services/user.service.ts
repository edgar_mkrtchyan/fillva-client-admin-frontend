import { Injectable } from '@angular/core';

@Injectable()

export class UserService {

    private authenticated: boolean;
    private userAccessToken: string;
    private userRefreshToken: string;
    private userFirstName: string;
    private userLastName: string;
    private userId: number;
    private numberOfSeats: number;
    private isdemomode: boolean;

    constructor() {
        this.authenticated = false;
        this.userAccessToken = '';
        this.userRefreshToken = '';
        this.userFirstName = '';
        this.userLastName = '';
        this.userId = 0;
        this.isdemomode = false;
    }

    get isAuthenticated(): boolean {
        return this.authenticated || localStorage.getItem('authenticated') === 'true';
    }

    set isAuthenticated(flag: boolean) {
        this.authenticated = flag;
        if (flag) {
            localStorage.setItem('authenticated', 'true');
        } else {
            localStorage.removeItem('authenticated');
        }
    }

    set accessToken(token: string) {
        this.userAccessToken = token;
        localStorage.setItem('access_token', token);
    }

    get accessToken(): string {
        return this.userAccessToken || localStorage.getItem('access_token');
    }

    set refreshToken(token: string) {
        this.userRefreshToken = token;
        localStorage.setItem('refresh_token', token);
    }

    get refreshToken(): string {
        return this.userRefreshToken || localStorage.getItem('refresh_token');
    }

    set userName(userName: string) {
        this.userFirstName = userName;
        localStorage.setItem('user_name', userName);
    }

    get userName(): string {
        return this.userFirstName || localStorage.getItem('user_name');
    }

    set userSurname(userSurname: string) {
        this.userSurname = this.userSurname;
        localStorage.setItem('user_surname', userSurname);
    }

    get userSurname(): string {
        return this.userLastName || localStorage.getItem('user_surname');
    }

    set userid(userid: number) {
        if (userid) {
            this.userId = userid;
            localStorage.setItem('user_id', userid.toString());
        }
    }

    get userid(): number {
        return this.userId || JSON.parse(localStorage.getItem('user_id'));
    }

    set numberofseats(numberofseats: number) {
        if (numberofseats) {
            this.numberOfSeats = numberofseats;
            localStorage.setItem('number_of_seats', numberofseats.toString());
        }
    }

    get numberofseats(): number {
        return this.numberOfSeats || JSON.parse(localStorage.getItem('number_of_seats'));
    }

    get isDemoMode(): boolean {
        return this.isdemomode || localStorage.getItem('isdemomode') === 'true';
    }

    set isDemoMode(flag: boolean) {
        this.isdemomode = flag;
        if (flag) {
            localStorage.setItem('isdemomode', 'true');
        } else {
            localStorage.removeItem('isdemomode');
        }
    }

}
