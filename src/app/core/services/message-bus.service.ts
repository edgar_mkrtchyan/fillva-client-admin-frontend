import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';

import { IActionMessage } from '../../common/interfaces/activity-percentage.interface';

@Injectable()

export class MessageBusService {

    subject: BehaviorSubject<IActionMessage>;

    constructor() {
        this.subject = new BehaviorSubject<IActionMessage>({ action: 'no-ops' });
    }

    send(message: IActionMessage) {
        this.subject.next(message);
    }

    receive(): Observable<any> {
        return this.subject.asObservable();
    }

}
