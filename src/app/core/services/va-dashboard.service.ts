
import {catchError, map} from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { BaseService } from '../../common/classes/base-web.service';
import { ConfigurationService } from './configuration.service';
import { UserService } from './user.service';
import { TimeSpentOnSites } from 'src/app/common/interfaces/time-spent-on-sites.interface';
import { ProductivityStatistics } from 'src/app/common/interfaces/productivity-statistics.interface';
import { ProductivityChartData } from 'src/app/common/interfaces/productivity-chart.interface';
import { RecentActivity } from 'src/app/common/interfaces/recent-activity.interface';
import { VAProductivityActivityObject } from 'src/app/common/interfaces/va-activity-productivity.interface';
import { VACurrentState } from 'src/app/common/interfaces/va-current-state.interface';

@Injectable()

export class VADashboardService extends BaseService {

    constructor(public config: ConfigurationService, public httpClient: HttpClient, private user: UserService) {
        super(config);
    }

    getVAProductivityActivity(payload: URLSearchParams, employeeId: number): Observable<any> {
        return this.httpClient.post(this.endpoint('api', 'va', 'activity-productivity', employeeId.toString()),
                                    payload.toString(), this.httpOptionsAuth()).pipe(
            map((response: HttpResponse<VAProductivityActivityObject>) => response),
            catchError((error: HttpErrorResponse) => this.handleHttpError(error)));
    }

    getCurrentlyActiveState(userId: number): Observable<any> {
        return this.httpClient.get(this.endpoint('api', 'dashboard', 'va', 'current-state', userId.toString()), this.httpOptions()).pipe(
            map((response: HttpResponse<VACurrentState>) => response),
                catchError((error: HttpErrorResponse) => this.handleHttpError(error)));
    }
}
