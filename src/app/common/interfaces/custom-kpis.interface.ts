export interface CustomKPIs {
    custom_events: CustomKPIsObject[];
}

export interface CustomKPIsObject {
    id: number;
    name: string;
    action: string;
    url: string;
    title: string;
}
