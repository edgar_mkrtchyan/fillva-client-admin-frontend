export interface WidgetsList {
    key: string;
    label: string;
    area: string;
}

