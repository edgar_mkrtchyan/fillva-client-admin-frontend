export interface ProductivityStatistics {
    data: ProductivityStatisticsObject[];
}

export interface ProductivityStatisticsObject {
    kpi_name: string;
    value: number;
}
