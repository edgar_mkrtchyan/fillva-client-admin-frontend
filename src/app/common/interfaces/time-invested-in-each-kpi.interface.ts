export interface TimeInvestedInEachKPI {
    data: TimeInvestedInEachKPIObject[];
}

export interface TimeInvestedInEachKPIObject {
    kpi_name: string;
    value: string;
}
