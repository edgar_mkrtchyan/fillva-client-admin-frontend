export interface Users {
    users: UserObject[];
}

export interface Employees {
    employees: UserObject[];
}

export interface UserObject {
    id?: number;
    email?: string;
    first_name?: string;
    last_name?: string;
    user_name?: string;
    is_active?: number;
    user_id?: number;
    rate?: number;
    avatar?: string;
    position?: string;
    last_log?: string;
    last_log_time?: string;
    team_id?: number;
    team_name?: string;
    daily_start_time?: string;
    number_of_seats?: number;
    tick_period?: number;
}

