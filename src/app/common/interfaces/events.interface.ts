export interface Events {
    logs: EventLogs[];
}

export interface EventLogs {
    id?: number;
    user_id?: number;
    email?: string;
    secs?: number;
    first_name?: string;
    last_name?: string;
    start_time?: string;
    tab_url?: string;
    tab_title?: string;
    tab_audible?: string;
    tab_favIconUrl?: string;
    ip?: string;
    rdp?: string;
    tab_incognito?: string;
    screenshotUrl?: string;
    action?: string;
    server_time?: string;
    account_name?: string;
    workstation_name?: string;
    custom_kpi?: boolean;
    source_network_address?: string;
}

export interface CellOrder {
    id?: number;
    first_name?: number;
    last_name?: number;
    ip?: number;
    action?: number;
    tab_url?: number;
    tab_title?: number;
    screenshotUrl?: number;
    server_time?: number;
    tab_favIconUrl?: number;
    actions?: number;
    avatar?: number;
    ext_version?: number;
    rdp_log?: number;
}
