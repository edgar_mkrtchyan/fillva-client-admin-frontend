export interface ChatColors {
    primary: string;
    success: string;
    info: string;
    warning: string;
    danger: string;
    gray: string;
    brand: string;
}

