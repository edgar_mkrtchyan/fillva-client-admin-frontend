export interface Alerts {
    data: AlertsObject[];
}

export interface AlertsObject {
    firstName: string;
    lastName: string;
    action: string;
    avatar: string;
}
