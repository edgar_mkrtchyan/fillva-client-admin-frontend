export interface BaseMetrics {
    metrics: BaseMetricsObject;
}

export interface BaseMetricsObject {
    worked: ValueObject;
    activity: ValueObject;
    totalCost: ValueObject;
    nonKpi: ValueObject;
}

export interface ValueObject {
    value: string;
    difference: string;
}
