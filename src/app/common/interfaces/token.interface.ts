export interface TokenObject {
    access_token: string;
    refresh_token: string;
}
