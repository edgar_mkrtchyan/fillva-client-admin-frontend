export interface TimeSpentOnSites {
    data: TimeSpentOnSitesObject[];
}

export interface TimeSpentOnSitesObject {
    url: string;
    value: number;
}
