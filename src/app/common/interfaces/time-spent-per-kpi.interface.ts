export interface TimeSpentPerKPI {
    data: TimeSpentPerKPIObject[];
}

export interface TimeSpentPerKPIObject {
    kpi_name: string;
    average: string;
}
