export interface CostPerEmployee {
    data: CostPerEmployeeObject[];
}

export interface CostPerEmployeeObject {
    employee: string;
    cost: string;
}
