export interface DateObject {
    startTime: string;
    endTime: string;
}
