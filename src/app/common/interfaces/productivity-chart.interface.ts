export interface ProductivityChartData {
    data: ProductivityChartObject[];
}

export interface ProductivityChartObject {
    date: string;
    activity: number;
    productivity: number;
}
