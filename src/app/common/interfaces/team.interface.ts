export interface Teams {
    teams: TeamObject[];
}

export interface TeamObject {
    id: number;
    team_name: string;
    employees_count: number;
    allowed_websites?: Array<string>;
}

