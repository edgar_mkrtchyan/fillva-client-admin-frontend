export interface FinanceBaseMetrics {
    data: FinanceBaseMetricsObject;
}

export interface FinanceBaseMetricsObject {
    employeesWorked: number;
    recordedTime: string;
    cost: number;
}
