export interface TopKPIS {
    data: TopKPISObject;
}

export interface TopKPISObject {
    dates: Array<string>;
    kpis: KPIObject[];
}

export interface KPIObject {
    name: string;
    data: Array<number>;
}
