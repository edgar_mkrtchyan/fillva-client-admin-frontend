export interface CostPerKPI {
    data: CostPerKPIObject[];
}

export interface CostPerKPIObject {
    kpi_name: string;
    cost: number;
    count: number;
}
