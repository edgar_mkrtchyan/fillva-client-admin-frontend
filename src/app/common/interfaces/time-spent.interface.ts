export interface TimeSpent {
    data: TimeSpentObject[];
}

export interface TimeSpentObject {
    firstName: string;
    lastName: string;
    image_url: string;
    value: number;
}
