export interface VACurrentState {
    data: VACurrentStateObject;
}

export interface VACurrentStateObject {
    inShift: boolean;
    ip?: string;
    since?: number;
    howLong?: string;
}

export interface ManagerVACurrentState {
    data: ManagerVACurrentStateObject[];
}

export interface ManagerVACurrentStateObject {
    firstName: string;
    lastName: string;
    avatar: string;
    ip?: string;
    since?: number;
    howLong?: string;
}
