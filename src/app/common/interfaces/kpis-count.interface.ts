export interface KPISCount {
    data: KPISCountObject;
}

export interface KPISCountObject {
    dates: Array<string>;
    kpis: KPICountObject[];
}

export interface KPICountObject {
    name: string;
    data: Array<number>;
}
