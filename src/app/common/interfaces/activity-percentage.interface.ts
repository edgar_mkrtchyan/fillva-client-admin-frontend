
export interface ActivityPercentage {
    data: ActivityPercentageObject[];
}

export interface ActivityPercentageObject {
    firstName: string;
    lastName: string;
    image_url: string;
    value: number;
}
