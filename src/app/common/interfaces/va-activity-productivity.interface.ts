export interface VAProductivityActivity {
    data: VAProductivityActivityObject[];
}

export interface VAProductivityActivityObject {
    date: string;
    activity: number;
    productivity: number;
}
