export interface RecentKPIHit {
    time: Array<string>;
    activity: ActivityObject[];
    kpis: KPIsObject[];
}

export interface ActivityObject {
    name: string;
    data: Array<number>;
}

export interface KPIsObject {
    time: string;
    kpi_name: string;
}
