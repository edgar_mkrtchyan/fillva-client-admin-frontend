export interface CostPerTeam {
    data: CostPerTeamObject[];
}

export interface CostPerTeamObject {
    team: string;
    cost: string;
}

