export interface LatestKPIS {
    data: LatestKPISObject[];
}

export interface LatestKPISObject {
    action: string;
    server_time: string;
}
