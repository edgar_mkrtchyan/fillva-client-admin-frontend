export interface RecentActivity {
    data: RecentActivityObject[];
}

export interface RecentActivityObject {
    firstName: string;
    lastName: string;
    image_url: string;
    screenshots: ScreenshotsObject[];
}

export interface ScreenshotsObject {
    url: string;
    activity: number;
    server_time: string;
    activity_percentage?: string;
}
