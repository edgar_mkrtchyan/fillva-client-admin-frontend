export class UrlParams {
    params: {};

    constructor(urlParams: UrlParams = null) {
        if (urlParams) {
            this.params = JSON.parse(JSON.stringify(urlParams.params));
        } else {
            this.params = {};
        }
    }

    get keys() {
        return Object.keys(this.params || {});
    }

    get(key: string): any {
        return this.params[key];
    }

    set(key: string, value: any) {
        this.params[key] = value;
    }

    clone(): UrlParams {
        const urlParams: UrlParams = new UrlParams();
        urlParams.params = JSON.parse(JSON.stringify(this.params));

        return urlParams;
    }

    toString(): string {
        const keyValues: string[] = [];
        const keys: string[] = this.keys;

        keys.forEach(element => {
            keyValues.push(`${ element }=${ JSON.stringify(this.params[keys[element]]) }`);
        });

        return keyValues.join('&');
    }
}
