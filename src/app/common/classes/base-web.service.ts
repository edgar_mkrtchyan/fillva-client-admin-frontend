import { throwError as observableThrowError,  Observable } from 'rxjs';
import { HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { ConfigurationService } from '../../core/services/configuration.service';
import { Param } from '../interfaces/param.interface';

export class BaseService {

    constructor(public config: ConfigurationService) {}

    httpOptions(): {} {
        return { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
    }

    httpOptionsForDeleteBody(requestBody: object): {} {
        return {
            headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
            body: requestBody
        };
    }

    httpOptionsAuth(): {} {
        return { headers: new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' }) };
    }

    httpOptionsAuthPhotoUpload(token): {} {
        return { headers: new HttpHeaders({ Authorization : 'Bearer ' + token }) };
    }

    endpoint(...resources: string[]): string {
        const endpoint = `${ this.config.restfulApiBase }/${ resources.join('/') }`;

        return endpoint;
    }

    endpointWithGETParam(param: Param, ...resources: string[]): string {
        const endpoint = `${ this.config.restfulApiBase }/${ resources.join('/') + '?' + param.name + '=' + param.value }`;

        return endpoint;
    }

    endpointWithMultipleGETParams(params: string, ...resources: string[]): string {
        const endpoint = `${ this.config.restfulApiBase }/${ resources.join('/') + '?' + params }`;

        return endpoint;
    }

    handleHttpError(httpError: HttpErrorResponse) {
        let userErrorMessage = '';
        let internalErrorMessage = '';

        if (httpError.error && httpError.error.message) {
            userErrorMessage = httpError.error.message;
        } else {
            userErrorMessage = httpError.message;
        }

        internalErrorMessage = `Http status[${ httpError.status }], url[${ httpError.url }], message[${ httpError.message }]`;

        return observableThrowError(httpError);
    }

}
