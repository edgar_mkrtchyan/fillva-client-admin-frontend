
import { finalize } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Subscription } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';
import { UserService } from '../../core/services/user.service';
import { JwtHelperService } from '@auth0/angular-jwt';
import { AccountService } from '../../core/services/account.service';
import { TokenObject } from '../../common/interfaces/token.interface';
import { ConfigurationService } from 'src/app/core/services/configuration.service';
import { Router } from '@angular/router';

declare let moment: any;

@Injectable()

export class CommonFunctions {

    refreshTokenSubscription: Subscription;
    jwtHelper = new JwtHelperService();
    logoutUserSubscription: Subscription;

    constructor(private user: UserService,
                private config: ConfigurationService,
                private router: Router,
                private as: AccountService) {
        this.logoutUserSubscription = null;
        this.refreshTokenSubscription = null;
    }

    cleanupSubscription(subscription: Subscription) {
        if (subscription) {
            subscription.unsubscribe();
            subscription = null;
        }
    }

    refreshToken() {
        return new Promise((resolve, reject) => {
            if (!this.jwtHelper.isTokenExpired(this.user.refreshToken)) {
                const payload = new URLSearchParams();

                payload.append('refresh_token', this.user.refreshToken);

                this.refreshTokenSubscription = this.as.refreshToken(payload).pipe(
                    finalize(() => {
                        this.cleanupSubscription(this.refreshTokenSubscription);
                    }))
                    .subscribe(
                        (imResponse: TokenObject) => {
                            this.user.accessToken = imResponse.access_token;
                            this.user.refreshToken = imResponse.refresh_token;
                            resolve('');
                            if (this.config.isDev) {
                                console.log(`CommonFunctions.refreshToken Completed`);
                            }
                        },
                        (error: HttpErrorResponse) => {
                            this.logOut();
                            reject();
                            if (this.config.isDev) {
                                console.log(`CommonFunctions.refreshToken Failed`);
                            }
                        }
                    );
            } else {
                this.logOut();
                resolve('');
            }
        });
    }

    logOut() {
        this.logout(true);
    }

    isAdmin() {
        const userRoles = this.getUserRoles();
        if (userRoles) {
            return userRoles.indexOf('ADMIN') > -1;
        } else {
            return false;
        }
    }

    isManager() {
        const userRoles = this.getUserRoles();
        if (userRoles) {
            return userRoles.indexOf('MANAGER') > -1;
        } else {
            return false;
        }
    }

    isTeamManager() {
        const userRoles = this.getUserRoles();
        if (userRoles) {
            return userRoles.indexOf('TEAM-MANAGER') > -1;
        } else {
            return false;
        }
    }

    isGroupManager() {
        const userRoles = this.getUserRoles();
        if (userRoles) {
            return userRoles.indexOf('GROUP-MANAGER') > -1;
        } else {
            return false;
        }
    }

    isVA() {
        const userRoles = this.getUserRoles();
        if (userRoles) {
            return userRoles.indexOf('VA') > -1;
        } else {
            return false;
        }
    }

    getUserRoles() {
        if (this.user.accessToken) {
            const decodedToken = this.jwtHelper.decodeToken(this.user.accessToken);
            if (decodedToken) {
                // tslint:disable-next-line:no-string-literal
                const roles = decodedToken['authorities'];
                return roles;
            } else {
                return [];
            }
        }
    }

    sortByKey(array: Array<object>, key: string) {
        return array.sort((a, b) => {
            const x = a[key]; const y = b[key];
            return ((x < y) ? -1 : ((x > y) ? 1 : 0));
        });
    }

    sortByKeyDesc(array: Array<object>, key: string) {
        return array.sort((a, b) => {
            const x = a[key]; const y = b[key];
            return ((x > y) ? -1 : ((x < y) ? 1 : 0));
        });
    }

    logout(refresh?: boolean): void {
        localStorage.clear();
        this.user.isAuthenticated = false;
        location.reload();
    }

    convertDateIntoLocalTimeZone(data: string) {
        if (!data) {
            return '';
        } else {
            const utcDate = moment.utc(data);
            return moment(utcDate).local().format('YYYY-MM-DD HH:mm:ss');
        }
    }

    pad(value: number) {
        if (value < 10) {
            return '0' + value;
        } else {
            return value;
        }
    }

    convertChartDateIntoLocalTimeZone(data: string) {
        if (!data) {
            return '';
        } else {
            const utcDate = moment.utc(data);
            return moment(utcDate).local().format('HH:mm');
        }
    }

    getUrl() {
        return window.location.hostname;
    }
}
