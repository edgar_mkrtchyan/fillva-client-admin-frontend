import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnChanges, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { CommonFunctions } from 'src/app/common/components/common.component';
import { DateObject } from 'src/app/common/interfaces/date.interface';
import { RecentActivity, RecentActivityObject } from 'src/app/common/interfaces/recent-activity.interface';
import { UserObject } from 'src/app/common/interfaces/user.interface';
import { ConfigurationService } from 'src/app/core/services/configuration.service';
import { ProductivityService } from 'src/app/core/services/productivity.service';

declare let moment: any;
declare let $: any;

@Component({
  selector: 'app-screenshots',
  templateUrl: './screenshots.component.html',
  styleUrls: ['./screenshots.component.scss']
})
export class ScreenshotsComponent implements OnInit, OnDestroy {

  getDataInProgress: boolean;
  getDataSubscription: Subscription;
  private unsubscribe: Subscription[] = [];
  data: RecentActivityObject[];
  colourNames: Array<string>;
  selectedStartDate: string;
  selectedEndDate: string;
  selectedEmployee: UserObject;

  constructor(private ws: ProductivityService,
              public common: CommonFunctions,
              private config: ConfigurationService) {
    this.data = [];
    this.colourNames = ['text-warning', 'text-success', 'text-danger', 'text-primary'];
    this.selectedStartDate = moment().subtract(6, 'days');
    this.selectedEndDate = moment();
    this.selectedStartDate = Math.floor(parseInt(this.selectedStartDate.valueOf(), 10) / 1000).toString();
    this.selectedEndDate = Math.floor(parseInt(this.selectedEndDate.valueOf(), 10) / 1000).toString();
    this.getDataInProgress = false;
    this.getDataSubscription = null;
  }

  ngOnInit(): void {

  }

  ngOnDestroy(): void {
    this.unsubscribe.forEach((cb) => cb.unsubscribe());
  }

  selectEmployee(employee: UserObject) {
    this.selectedEmployee = employee;
    this.getData();
  }

  getData() {
    const payload = new URLSearchParams();
    payload.append('startTime', this.selectedStartDate);
    payload.append('endTime', this.selectedEndDate);
    payload.append('type', 'screenshots');
    this.getDataInProgress = true;
    this.getDataSubscription = this.ws.getRecentActivity(payload, this.selectedEmployee.id).pipe()
      .subscribe(
        (imResponse: RecentActivity) => {
          this.getDataInProgress = false;
          this.data = imResponse.data;
          // Splitting into chunks of 6
          this.data = this.chunks(this.data, 6);
          $.getScript('https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.3/js/lightbox.js', () => { });
          if (this.config.isDev) {
            console.log(`VaRecentActivityComponent.getData Completed`);
          }
        },
        (error: HttpErrorResponse) => {
          if (this.config.isDev) {
            console.log(`VaRecentActivityComponent.getData Failed`);
          }
        }
      );
    this.unsubscribe.push(this.getDataSubscription);
  }

  returnFullImageUrl(imageName: string) {
    return this.config.restfulApiBase + '/screenshots/' + imageName + '.jpg';
  }

  definePercentageColor(value: number) {
    if (value < 50) {
      return 'bg-danger';
    } else if (value > 50 && value < 65) {
      return 'bg-warning';
    } else {
      return 'bg-success';
    }
  }

  returnColourName(index: number) {
    let colourName = '';
    if (index > this.colourNames.length - 1) {
      index = index - this.colourNames.length - 1;
    }
    colourName = this.colourNames[index];
    return colourName;
  }

  dateChanged($event: DateObject) {
    this.selectedStartDate = $event.startTime;
    this.selectedEndDate = $event.endTime;
    this.selectedStartDate = Math.floor(parseInt(this.selectedStartDate.valueOf(), 10) / 1000).toString();
    this.selectedEndDate = Math.floor(parseInt(this.selectedEndDate.valueOf(), 10) / 1000).toString();
    this.getData();
  }

  chunks(array, size) {
    let results = [];
    results = [];
    while (array.length) {
      results.push(array.splice(0, size));
    }
    return results;
  }

}
