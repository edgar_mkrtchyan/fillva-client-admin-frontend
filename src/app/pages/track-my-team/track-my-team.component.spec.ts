import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TrackMyTeamComponent } from './track-my-team.component';

describe('TrackMyTeamComponent', () => {
  let component: TrackMyTeamComponent;
  let fixture: ComponentFixture<TrackMyTeamComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TrackMyTeamComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TrackMyTeamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
