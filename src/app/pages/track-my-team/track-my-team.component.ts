import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { CommonFunctions } from 'src/app/common/components/common.component';
import { CustomKPIs, CustomKPIsObject } from 'src/app/common/interfaces/custom-kpis.interface';
import { CellOrder, EventLogs, Events } from 'src/app/common/interfaces/events.interface';
import { ConfigurationService } from 'src/app/core/services/configuration.service';
import { LogsService } from 'src/app/core/services/logs.service';
import { UserService } from 'src/app/core/services/user.service';
import { AddCustomKpiModalComponent } from './add-custom-kpi-modal/add-custom-kpi-modal.component';
import { RemoveCustomKpiModalComponent } from './remove-custom-kpi-modal/remove-custom-kpi-modal.component';

declare let $: any;
declare let moment: any;

@Component({
  selector: 'app-track-my-team',
  templateUrl: './track-my-team.component.html',
  styleUrls: ['./track-my-team.component.scss']
})
export class TrackMyTeamComponent implements OnInit, OnDestroy {

  getDataInProgress: boolean;
  kpiEventsEnabled: boolean;
  getDataSubscription: Subscription;
  events: EventLogs[];
  filterForm: FormGroup;
  sortOrder: string;
  sortedBy: string;
  eventsReserve: EventLogs[];
  selectedStartDate: string;
  selectedEndDate: string;
  datePicked: boolean;
  cellOrder: CellOrder;
  kpiEventsOnlyCellOrder: CellOrder;
  table: any;
  dateColumnIndex: number;
  private unsubscribe: Subscription[] = [];
  kpis: CustomKPIsObject[];

  constructor(private formBuilder: FormBuilder,
              private common: CommonFunctions,
              private modalService: NgbModal,
              private user: UserService,
              private config: ConfigurationService,
              private ws: LogsService) {
    this.getDataInProgress = false;
    this.kpiEventsEnabled = true;
    this.filterForm = null;
    this.sortOrder = '';
    this.sortedBy = '';
    this.selectedStartDate = moment().format('MM-DD-YYYY');
    this.selectedEndDate = moment().format('MM-DD-YYYY');
    this.datePicked = false;
    this.events = [];
    this.cellOrder = {
      id: 0,
      first_name: 1,
      last_name: 2,
      ip: 3,
      action: 4,
      tab_url: 5,
      tab_title: 6,
      screenshotUrl: 7,
      server_time: 8,
      actions: 10,
      ext_version: 12,
      rdp_log: 13
    };
    this.kpiEventsOnlyCellOrder = {
      first_name: 1,
      action: 4,
      screenshotUrl: 7,
      server_time: 8,
      tab_favIconUrl: 9,
      avatar: 10
    };
    this.table = null;
    this.dateColumnIndex = 8;
    this.kpis = [];
  }

  ngOnInit(): void {
    // First getting the list of Custom KPI-s to be able to show either Set or Remove buttons
    this.getCustomKPIs();
    $.getScript('https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.3/js/lightbox.js', () => {});
  }

  ngOnDestroy() {
    this.unsubscribe.forEach((sb) => sb.unsubscribe());
  }

  buildFilterForm() {
    this.filterForm = this.formBuilder.group({
      id: [''],
      name: [''],
      surname: [''],
      proxy_ip: [''],
      action: [''],
      title: [''],
      screenshot: ['']
    });
  }

  toggleKPIEvents($event: any) {
    this.kpiEventsEnabled = $event.target.checked ? true : false;
    setTimeout(() => {
      if (this.kpiEventsEnabled) {
        if ($.fn.dataTable.isDataTable('#kpiLogsTable')) {
          this.table.draw();
        } else {
          this.initKPIDataTable();
        }
      } else {
        if ($.fn.dataTable.isDataTable('#logsTable')) {
          this.table.draw();
        } else {
          this.initDataTable();
        }
      }
    }, 9);
  }

  // Used to identify the current sort order in the UI
  returnSortOrderArrow(sortColumnIdentifier: string) {
    if (sortColumnIdentifier === this.sortedBy) {
      if (this.sortOrder === 'asc') {
        return 'fa-sort-up';
      } else if (this.sortOrder === 'desc') {
        return 'fa-sort-down';
      }
    } else {
      return 'fa-sort';
    }
  }

  openAddCustomKPIEventModal(eventName: string, tabUrl: string, tabTitle: string) {
    const modalRef = this.modalService.open(AddCustomKpiModalComponent);
    modalRef.componentInstance.eventName = eventName;
    modalRef.componentInstance.tabUrl = tabUrl;
    modalRef.componentInstance.tabTitle = tabTitle;

    modalRef.result.then((res) => {
      if (res === 'refresh') {
        this.getCustomKPIs();
      }
    });
  }

  openRemoveCustomKPIEventModal(eventId: string) {
    const modalRef = this.modalService.open(RemoveCustomKpiModalComponent);
    modalRef.componentInstance.eventId = eventId;

    modalRef.result.then((res) => {
      if (res === 'refresh') {
        // Removing the Custom KPI from the list to create the correct button
        this.kpis = this.kpis.filter((kpi) => kpi.id !== parseInt(eventId, 10));
        this.table.draw();
      }
    });
  }

  returnFullImageUrl(imageName: string) {
    return this.config.restfulApiBase + '/screenshots/' + imageName + '.jpg';
  }

  // ALL LOGS
  initDataTable() {
    const that = this;
    const orderBy = [[this.cellOrder.id, 'desc']];

    this.table = $('#logsTable').DataTable({
      // responsive: true,
      searchDelay: 500,
      processing: true,
      serverSide: true,
      pageLength: 25,
      searching: true,
      order: orderBy,
      autoWidth: false,
      sDom: 'Rrtlip',
      // tslint:disable-next-line:object-literal-shorthand
      initComplete: function() {
        that.colFilters(true);
      },
      // tslint:disable-next-line:object-literal-shorthand
      drawCallback: function(settings) {
        $('.add-custom-kpi').on('click', function() {
          const actionName = $(this).attr('data-action');
          const actionUrl = $(this).attr('data-tab-url');
          const actionTitle = $(this).attr('data-tab-title');
          that.openAddCustomKPIEventModal(actionName, actionUrl, actionTitle);
        });

        $('.remove-custom-kpi').on('click', function() {
          const customKPIId = $(this).attr('data-custom-kpi-id');
          that.openRemoveCustomKPIEventModal(customKPIId);
        });
      },
      ajax: {
        url: this.config.restfulApiBase + '/api/logs2',
        type: 'POST',
        beforeSend: (request) => {
          request.setRequestHeader('Authorization', 'Bearer ' + this.user.accessToken);
        },
        data: {
          // Add dynamic parameters to the data object sent to the server
          only_kpi: false
        }
      },
      columns: [
        { data: that.cellOrder.id, width: '50px' },
        { data: that.cellOrder.first_name, width: '100px', visible: true },
        { data: that.cellOrder.last_name, width: '100px', visible: true },
        { data: that.cellOrder.ip, visible: true, width: '100px' },
        {
          data: that.cellOrder.action, visible: true, mRender: (data, type, row) => {
            return data.replace(/(<([^>]+)>)/gi, '');
          }},
        { data: that.cellOrder.tab_url, visible: false },
        {
          data: that.cellOrder.tab_title, width: '100px', visible: true, mRender: (data, type, row) => {
            const url = row[that.cellOrder.tab_url];
            let domain = '';
            if (url.indexOf('://') !== -1) {
              domain = url.split('/')[2];
            }
            if (domain !== '') {
              data = '[' + domain + '] ' + data;
            }
            return data;
          }
        },
        {
          data: that.cellOrder.screenshotUrl, orderable: false, visible: true, mRender: (data, type, row) => {
            if (row[that.cellOrder.action] === 'logged_in' || row[that.cellOrder.action] === 'logged_out') {
              return '';
            } else {
              // tslint:disable-next-line:max-line-length
              data = '<a class="btn btn-sm btn-success" href="' + this.returnFullImageUrl(data) + '" title="' + row[that.cellOrder.server_time] + '" data-lightbox="image">View</a>';
              return data;
            }
          }
        },
        {
          data: that.cellOrder.server_time, visible: true, width: '120px', mRender: (data, type, row) => {
            return that.common.convertDateIntoLocalTimeZone(data);
          }
        },
        { data: that.cellOrder.ext_version, orderable: false, visible: true },
        {
          data: that.cellOrder.rdp_log, visible: true, orderable: false, mRender: function (data, type, row, meta) {
            if (row[that.cellOrder.rdp_log] !== '') {
              return `<span class="label label-lg label-light-warning label-inline">RDP</span>`;
            } else {
              return '';
            }
          } },
        {
          targets: -1,
          title: 'Custom KPI',
          orderable: false,
          width: '100px',
          mRender: function (data, type, row, meta) {
            // If a custom KPI is set - show the button to remove it
            const kpiSet = that.kpis.filter((kpi) =>
              kpi.name.toLowerCase() === row[that.cellOrder.action].toLowerCase() &&
              kpi.title.toLowerCase() === row[that.cellOrder.tab_title].toLowerCase() &&
              kpi.url.toLowerCase() === row[that.cellOrder.tab_url].toLowerCase()
            );
            if (kpiSet.length > 0) {
              return '\
                <a href="javascript:;" data-custom-kpi-id="' + kpiSet[0].id + '" class="remove-custom-kpi btn btn-sm btn-danger">\
                  Remove\
                </a>\
              ';
            } else {
              return '\
                <a href="javascript:;" data-action="' + row[that.cellOrder.action].replace(/(<([^>]+)>)/gi, '') + '" data-tab-url="' + row[that.cellOrder.tab_url] + '" data-tab-title="' + row[that.cellOrder.tab_title] + '" class="add-custom-kpi btn btn-sm btn-success">\
                  Set\
                </a>\
              ';
            }
          },
        }
      ],
    });
  }

  colFilters(reset) {
    const that = this;

    if (reset) {
      $('#logsTable thead tr:eq(1)').remove();
      $('#logsTable thead tr').clone(true).appendTo('#logsTable thead');
    }

    $('#logsTable thead tr:eq(1) th').each(function(i) {
      const title = $(this).text();

      $(this).attr('data-toggle', '');
      $(this).removeClass('sorting');
      $(this).removeClass('sorting_desc');
      $(this).removeClass('sorting_asc');
      $(this).unbind('click');

      i = parseInt($(this).attr('data-col-org'), 10);

      $(this).attr('data-toggle', '');

      if (i === that.cellOrder.server_time) {
        $(this).html('<input class="txtDateSearch form-control form-control-sm" style="width:130px" id="txtSearch_' + i + '" type="text" placeholder="Select Dates" />');

        $('#txtSearch_' + i).daterangepicker({
          maxDate: moment(),
          startDate: moment(that.selectedStartDate).format('MM-DD-YYYY'),
          endDate: moment(that.selectedEndDate).format('MM-DD-YYYY'),
          maxYear: parseInt(moment().format('YYYY'), 10),
          locale: {
            format: 'MM-DD-YYYY',
            cancelLabel: 'Clear'
          },
          autoUpdateInput: false,
          separator: '-',
        // tslint:disable-next-line:only-arrow-functions
        }, function(start, end) {
          that.selectedStartDate = moment(start).format('MM-DD-YYYY');
          that.selectedEndDate = moment(end).format('MM-DD-YYYY');
          if (that.table.column(that.dateColumnIndex).search() !== $('#txtSearch_' + that.dateColumnIndex).val()) {
            that.table.column(i).search($('#txtSearch_' + that.dateColumnIndex).val());
            that.table.draw();
          }
        });

      } else {
        // tslint:disable-next-line:max-line-length
        if (i === that.cellOrder.screenshotUrl || i === that.cellOrder.actions + 1 || i === 9 || i === 10) {
          $(this).html('<input class="txtSearch form-control hidden form-control-sm" id="txtSearch_' + i + '" type="text" placeholder="Search ' + title + '" />');
        } else {
          // tslint:disable-next-line:max-line-length
          $(this).html('<input class="txtSearch form-control form-control-sm" id="txtSearch_' + i + '" type="text" placeholder="Search ' + title + '" />');
        }
      }

      let lastTimeout = null;

      $('.txtDateSearch', this).on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD.M.YY') + '-' + picker.endDate.format('DD.M.YY'));
        if (that.table.column(that.dateColumnIndex).search() !== this.value) {
          that.table.column(that.dateColumnIndex).search(this.value);
          that.table.draw();
        } else {

        }
      });

      $('.txtDateSearch', this).on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
        that.table.column(that.dateColumnIndex).search(this.value);
        that.table.draw();
      });

      $('input.txtSearch', this).on('keyup change', function() {
        if (that.table.column(i).search() !== this.value) {
          that.table.column(i).search(this.value);
          if (lastTimeout) {
            clearTimeout(lastTimeout);
          }
          // tslint:disable-next-line:only-arrow-functions
          lastTimeout = setTimeout(function() {
            lastTimeout = false;
            that.table.draw();
          }, 1000);

        }
      });
    });
    // Loading lightbox
    $.getScript('https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.3/js/lightbox.js', () => { });
  }
  // ALL LOGS

  // KPI LOGS
  initKPIDataTable() {
    const that = this;

    const orderBy = [[this.cellOrder.server_time, 'desc']];

    this.table = $('#kpiLogsTable').DataTable({
      // responsive: true,
      searchDelay: 500,
      processing: true,
      serverSide: true,
      pageLength: 25,
      searching: true,
      order: orderBy,
      autoWidth: false,
      sDom: 'Rrtlip',
      // tslint:disable-next-line:object-literal-shorthand
      initComplete: function () {
        that.kpiColFilters(true);
      },
      // tslint:disable-next-line:object-literal-shorthand
      drawCallback: function(settings) {

      },
      ajax: {
        url: this.config.restfulApiBase + '/api/logs2',
        type: 'POST',
        beforeSend: (request) => {
          request.setRequestHeader('Authorization', 'Bearer ' + this.user.accessToken);
        },
        data: {
          only_kpi: true
        }
      },
      columns: [
        { data: that.cellOrder.id, visible: false, width: '50px' },
        {
          data: that.cellOrder.first_name, width: '100px', visible: true, mRender: (data, type, row) => {
            if (row[that.kpiEventsOnlyCellOrder.avatar]) {
              return `
                <table class="table table-borderless mb-0">
                  <tr>
                    <td class="pl-0 pr-0 pb-0 pt-0 p-0 w-50px">
                      <div class="symbol symbol-50 symbol-light mr-2">
                        <span class="symbol-label">
                          <img src="` + row[that.kpiEventsOnlyCellOrder.avatar] + `" alt="" class="h-75 align-self-center">
                        </span>
                      </div>
                    </td>
                    <td class="pl-0 pr-0 pb-0 pt-0">
                      ` + data + `
                    </td>
                  </tr>
              `;
            } else {
              return `
              <table class="table table-borderless mb-0">
                <tr>
                  <td class="pl-0 pr-0 pb-0 pt-0 p-0 w-50px">
                    <div class="symbol symbol-50 symbol-light mr-2"><span class="symbol-label"><img src="../../../../../../../assets/media/svg/avatars/001-boy.svg" alt="" class="h-75 align-self-end"></span></div>
                  </td>
                  <td class="pl-0 pr-0 pb-0 pt-0">
                    ` + data + `
                  </td>
                </tr>
            `;
            }
          }
        },
        { data: that.cellOrder.last_name, visible: false, width: '100px' },
        { data: that.cellOrder.ip, visible: false, width: '100px' },
        {
          data: that.cellOrder.action, visible: true, mRender: (data, type, row) => {
            return data.replace(/(<([^>]+)>)/gi, '');
          }
        },
        {
          data: that.cellOrder.tab_url, visible: true, mRender: (data, type, row) => {
            let domain = '';
            if (data.indexOf('://') !== -1) {
              domain = data.split('/')[2];
            }
            if (row[that.kpiEventsOnlyCellOrder.tab_favIconUrl]) {
              return '<img class="site-favicon" src="' + row[that.kpiEventsOnlyCellOrder.tab_favIconUrl] + '">' + domain;
            } else {
              return domain;
            }
          }
        },
        {
          data: that.cellOrder.tab_title, width: '100px', visible: false, mRender: (data, type, row) => {
            const url = row[that.cellOrder.tab_url];
            let domain = '';
            if (url.indexOf('://') !== -1) {
              domain = url.split('/')[2];
            }
            if (domain !== '') {
              data = '[' + domain + '] ' + data;
            }
            return data;
          }
        },
        {
          data: that.cellOrder.screenshotUrl, orderable: false, visible: true, mRender: (data, type, row) => {
            if (row[that.kpiEventsOnlyCellOrder.action] === 'logged_in' || row[that.kpiEventsOnlyCellOrder.action] === 'logged_out') {
              return '';
            } else {
              // tslint:disable-next-line:max-line-length
              data = '<a class="btn btn-sm btn-success" href="' + this.returnFullImageUrl(data) + '" title="' + row[that.cellOrder.server_time] + '" data-lightbox="image">View</a>';
              return data;
            }
          }
        },
        {
          data: that.cellOrder.server_time, visible: true, width: '120px', mRender: (data, type, row) => {
            return that.common.convertDateIntoLocalTimeZone(data);
          }
        },
        // { data: that.cellOrder.tab_favIconUrl, visible: false },
        {
          targets: -1,
          title: 'Custom KPI',
          orderable: false,
          visible: false,
          width: '100px',
          mRender: function (data, type, row, meta) {
            return '\
							<a href="javascript:;" data-action="' + row[that.cellOrder.action].replace(/(<([^>]+)>)/gi, '') + '" data-tab-url="' + row[that.cellOrder.tab_url] + '" data-tab-title="' + row[that.cellOrder.tab_title] + '" class="add-custom-kpi btn btn-sm btn-primary">\
								Set\
							</a>\
						';
          },
        }
      ],
    });
  }

  kpiColFilters(reset) {
    const that = this;

    if (reset) {
      $('#kpiLogsTable thead tr:eq(1)').remove();
      $('#kpiLogsTable thead tr').clone(true).appendTo('#kpiLogsTable thead');
    }

    $('#kpiLogsTable thead tr:eq(1) th').each(function (i) {
      const title = $(this).text();

      $(this).attr('data-toggle', '');
      $(this).removeClass('sorting');
      $(this).removeClass('sorting_desc');
      $(this).removeClass('sorting_asc');
      $(this).unbind('click');

      i = parseInt($(this).attr('data-col-org'), 10);

      $(this).attr('data-toggle', '');

      if (i === that.cellOrder.server_time) {
        $(this).html('<input class="txtKPIDateSearch form-control form-control-sm" style="width:130px" id="txtKPISearch_' + i + '" type="text" placeholder="Select Dates" />');

        $('#txtKPISearch_' + i).daterangepicker({
          maxDate: moment(),
          startDate: moment(that.selectedStartDate).format('MM-DD-YYYY'),
          endDate: moment(that.selectedEndDate).format('MM-DD-YYYY'),
          maxYear: parseInt(moment().format('YYYY'), 10),
          locale: {
            format: 'MM-DD-YYYY',
            cancelLabel: 'Clear'
          },
          autoUpdateInput: false,
          separator: '-',
          // tslint:disable-next-line:only-arrow-functions
        }, function (start, end) {
          that.selectedStartDate = moment(start).format('MM-DD-YYYY');
          that.selectedEndDate = moment(end).format('MM-DD-YYYY');
          if (that.table.column(that.dateColumnIndex).search() !== $('#txtKPISearch_' + that.dateColumnIndex).val()) {
            that.table.column(i).search($('#txtKPISearch_' + that.dateColumnIndex).val());
            that.table.draw();
          }
        });

      } else {
        // tslint:disable-next-line:max-line-length
        if (i === that.cellOrder.screenshotUrl || i === that.cellOrder.actions) {
          $(this).html('<input class="txtKPISearch form-control hidden form-control-sm" id="txtKPISearch_' + i + '" type="text" placeholder="Search ' + title + '" />');
        } else {
          // tslint:disable-next-line:max-line-length
          $(this).html('<input class="txtKPISearch form-control form-control-sm" id="txtKPISearch_' + i + '" type="text" placeholder="Search ' + title + '" />');
        }
      }

      let lastTimeout = null;

      $('.txtKPIDateSearch', this).on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('DD.M.YY') + '-' + picker.endDate.format('DD.M.YY'));
        if (that.table.column(that.dateColumnIndex).search() !== this.value) {
          that.table.column(that.dateColumnIndex).search(this.value);
          that.table.draw();
        } else {

        }
      });

      $('.txtKPIDateSearch', this).on('cancel.daterangepicker', function (ev, picker) {
        $(this).val('');
        that.table.column(that.dateColumnIndex).search(this.value);
        that.table.draw();
      });

      $('input.txtKPISearch', this).on('keyup change', function () {
        if (that.table.column(i).search() !== this.value) {
          that.table.column(i).search(this.value);
          if (lastTimeout) {
            clearTimeout(lastTimeout);
          }
          // tslint:disable-next-line:only-arrow-functions
          lastTimeout = setTimeout(function () {
            lastTimeout = false;
            that.table.draw();
          }, 1000);

        }
      });
    });
    // Loading lightbox
    $.getScript('https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.3/js/lightbox.js', () => { });
  }
  // ALL LOGS

  getCustomKPIs() {
    this.getDataInProgress = true;
    this.getDataSubscription = this.ws.getCustomKPIs().pipe(
      finalize(() => {
        this.common.cleanupSubscription(this.getDataSubscription);
      }))
      .subscribe(
        (imResponse: CustomKPIs) => {
          this.getDataInProgress = false;
          this.kpis = imResponse.custom_events;
          // Drawing the tables
          setTimeout(() => {
            if (this.kpiEventsEnabled) {
              if (!$.fn.DataTable.isDataTable('#kpiLogsTable')) {
                this.initKPIDataTable();
              } else {
                this.table.draw();
              }
            } else {
              if (!$.fn.DataTable.isDataTable('#logsTable')) {
                this.initDataTable();
              } else {
                this.table.draw();
              }
            }
          }, 9);
          if (this.config.isDev) {
            console.log(`ManageUsersComponent.getCustomKPIs Completed`);
          }
        },
        (error: HttpErrorResponse) => {
          if (this.config.isDev) {
            console.log(`ManageUsersComponent.getCustomKPIs Failed`);
          }
        }
      );
    this.unsubscribe.push(this.getDataSubscription);
  }

}
