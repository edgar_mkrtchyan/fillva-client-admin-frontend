import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { CommonFunctions } from 'src/app/common/components/common.component';
import { ConfigurationService } from 'src/app/core/services/configuration.service';
import { LogsService } from 'src/app/core/services/logs.service';
import { UserAccountsService } from 'src/app/core/services/user-accounts.service';

@Component({
  selector: 'app-remove-custom-kpi-modal',
  templateUrl: './remove-custom-kpi-modal.component.html',
  styleUrls: ['./remove-custom-kpi-modal.component.scss']
})
export class RemoveCustomKpiModalComponent implements OnInit, OnDestroy {

  @Input() eventId: number;

  deleteInProgress: boolean;
  deleteCustomKPIEventSubscription: Subscription;
  private unsubscribe: Subscription[] = [];

  constructor(private ws: LogsService,
              private config: ConfigurationService,
              public activeModal: NgbActiveModal) {
    this.deleteInProgress = false;
    this.deleteCustomKPIEventSubscription = null;
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.unsubscribe.forEach((sb) => sb.unsubscribe());
  }

  removeCustomKPI() {
    this.deleteInProgress = true;
    this.deleteCustomKPIEventSubscription = this.ws.deleteCustomKPI(this.eventId).pipe()
      .subscribe(
        (imResponse: any) => {
          this.closeModal('refresh');
          if (this.config.isDev) {
            console.log(`DeleteUserModalComponent.deleteUser Completed`);
          }
        },
        (error: HttpErrorResponse) => {
          if (this.config.isDev) {
            console.log(`DeleteUserModalComponent.deleteUser Failed`);
          }
        }
    );
    this.unsubscribe.push(this.deleteCustomKPIEventSubscription);
  }

  closeModal(refresh?: string) {
    if (refresh) {
      this.activeModal.close('refresh');
    } else {
      this.activeModal.close();
    }
  }

}
