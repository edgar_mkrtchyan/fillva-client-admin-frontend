import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RemoveCustomKpiModalComponent } from './remove-custom-kpi-modal.component';

describe('RemoveCustomKpiModalComponent', () => {
  let component: RemoveCustomKpiModalComponent;
  let fixture: ComponentFixture<RemoveCustomKpiModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RemoveCustomKpiModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RemoveCustomKpiModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
