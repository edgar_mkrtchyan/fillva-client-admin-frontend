import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddCustomKpiModalComponent } from './add-custom-kpi-modal.component';

describe('AddCustomKpiModalComponent', () => {
  let component: AddCustomKpiModalComponent;
  let fixture: ComponentFixture<AddCustomKpiModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddCustomKpiModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddCustomKpiModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
