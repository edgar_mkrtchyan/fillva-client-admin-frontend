import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { ConfigurationService } from 'src/app/core/services/configuration.service';
import { LogsService } from 'src/app/core/services/logs.service';

@Component({
  selector: 'app-add-custom-kpi-modal',
  templateUrl: './add-custom-kpi-modal.component.html',
  styleUrls: ['./add-custom-kpi-modal.component.scss']
})
export class AddCustomKpiModalComponent implements OnInit, OnDestroy {

  kpiNameForm: FormGroup;
  saveInProgress: boolean;
  customKPISubscription: Subscription;
  @Input() eventName;
  @Input() tabUrl;
  @Input() tabTitle;
  subscriptions: Subscription[] = [];

  constructor(public activeModal: NgbActiveModal,
              private ws: LogsService,
              private config: ConfigurationService,
              private formBuilder: FormBuilder) {
    this.kpiNameForm = null;
    this.saveInProgress = false;
    this.customKPISubscription = null;
  }

  ngOnInit(): void {
    this.buildKPINameForm();
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sb => sb.unsubscribe());
  }

  buildKPINameForm() {
    this.kpiNameForm = this.formBuilder.group({
      custom_kpi_name: [''],
      custom_kpi_action: new FormControl({ value: this.eventName, disabled: true }, Validators.required),
      custom_kpi_url: [this.tabUrl],
      custom_kpi_page_title: [this.tabTitle]
    });
  }

  addCustomKPIEvent() {
    const controls = this.kpiNameForm.controls;

    const payload = new URLSearchParams();
    payload.append('action', controls.custom_kpi_action.value);
    payload.append('name', controls.custom_kpi_name.value);
    payload.append('url', controls.custom_kpi_url.value);
    payload.append('title', controls.custom_kpi_page_title.value);

    // Creating the user
    this.saveInProgress = true;
    this.customKPISubscription = this.ws.addCustomKPI(payload).pipe()
      .subscribe(
        (imResponse: any) => {
          this.closeModal('refresh');
          this.saveInProgress = false;
          if (this.config.isDev) {
            console.log(`AddCustomKpiModalComponent.addCustomKPIEvent Completed`);
          }
        },
        (error: HttpErrorResponse) => {
          if (this.config.isDev) {
            console.log(`AddCustomKpiModalComponent.addCustomKPIEvent Failed`);
          }
        }
      );
    this.subscriptions.push(this.customKPISubscription);
  }

  closeModal(refresh?: string) {
    if (refresh) {
      this.activeModal.close('refresh');
    } else {
      this.activeModal.close();
    }
  }

}
