import { HttpErrorResponse } from '@angular/common/http';
import { ChangeDetectionStrategy } from '@angular/compiler/src/compiler_facade_interface';
import { AfterContentChecked, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import { CommonFunctions } from 'src/app/common/components/common.component';
import { UserObject } from 'src/app/common/interfaces/user.interface';
import { AccountService } from 'src/app/core/services/account.service';
import { ConfigurationService } from 'src/app/core/services/configuration.service';
import { Constants } from 'src/app/core/services/constants.service';
import { CountriesService } from 'src/app/core/services/countries.service';
import { UserAccountsService } from 'src/app/core/services/user-accounts.service';
import { UserService } from 'src/app/core/services/user.service';
import { AuthService, UserModel } from 'src/app/modules/auth';

@Component({
  selector: 'app-client-profile',
  templateUrl: './client-profile.component.html',
  styleUrls: ['./client-profile.component.scss']
})
export class ClientProfileComponent implements OnInit, AfterContentChecked {

  saveUserDataSubscription: Subscription;
  saveUserDatainProgress: boolean;
  savePasswordInProgress: boolean;
  updateUserPasswordSubscription: Subscription;
  user: UserObject;
  getUserInProgress: boolean;
  userForm: FormGroup;
  passwordForm: FormGroup;
  errorMessage: string;
  user$: Observable<UserModel>;
  paymentInformationForm: FormGroup;
  countries: Array<object>;
  avatarFile: File;
  avatar: string;
  uploadImageSubscription: Subscription;
  activeSection: string;
  avatarChanged: boolean;
  imageReSaved: boolean;
  public isDemoServiceOn: boolean;
  shouldShow: boolean;

  constructor(private ws: AccountService,
              private us: UserAccountsService,
              private config: ConfigurationService,
              private auth: AuthService,
              public common: CommonFunctions,
              private cd: ChangeDetectorRef,
              private userService: UserService,
              public constants: Constants,
              private countriesService: CountriesService,
              public userservice: UserService,
              private formBuilder: FormBuilder) {
    this.getUserInProgress = false;
    this.userForm = null;
    this.passwordForm = null;
    this.saveUserDataSubscription = null;
    this.saveUserDatainProgress = false;
    this.errorMessage = '';
    this.updateUserPasswordSubscription = null;
    this.savePasswordInProgress = false;
    this.user$ = this.auth.currentUserSubject.asObservable();
    this.paymentInformationForm = null;
    this.countries = this.countriesService.getData();
    this.avatar = '';
    this.avatarFile = null;
    this.uploadImageSubscription = null;
    this.activeSection = 'account-info';
    this.avatarChanged = false;
    this.imageReSaved = false;
    this.isDemoServiceOn = this.userservice.isDemoMode;
    this.shouldShow = false;
  }

  ngOnInit(): void {
    this.buildPasswordForm();
    this.buildUserForm();
    if (this.common.isVA()) {
      this.buildPaymentINformationForm();
    }
    this.shouldShow = this.shouldShowToUser();
  }

  ngAfterContentChecked(): void {
    this.cd.detectChanges();
  }

  buildUserForm() {

    let ampm = this.constants.AMPM[0];
    let hour = this.constants.HOURS[0];
    let minute = this.constants.MINUTES[0];

    this.user$.subscribe((result) => {
      const dailyStartTime = result.daily_start_time;

      if (dailyStartTime) {
        const splitDailyStartTime = dailyStartTime.split(' ');
        const hourMinute = splitDailyStartTime[0];
        ampm = splitDailyStartTime[1];
        const hourMinuteSplit = hourMinute.split(':');
        hour = hourMinuteSplit[0];
        minute = hourMinuteSplit[1];
      }
    });

    this.userForm = this.formBuilder.group({
      email: new FormControl({ disabled: true, readonly: true }),
      username: [this.user?.user_name, [Validators.required, Validators.min(3)]],
      name: [this.user?.first_name, [Validators.required, Validators.min(3)]],
      surname: [this.user?.last_name],
      hour: [hour],
      minutes: [minute],
      ampm: [ampm]
    });
  }

  buildPasswordForm() {
    this.passwordForm = this.formBuilder.group({
      old_password: ['', [Validators.required, Validators.min(8)]],
      password: ['', [Validators.required, Validators.min(8)]],
      repeat_password: ['', [Validators.required, Validators.min(8)]]
    });
  }

  buildPaymentINformationForm() {
    this.paymentInformationForm = this.formBuilder.group({
      address: [''],
      country: [''],
      city: [''],
      zip: [''],
      phone: [''],
      business_number: [''],
      correspondent_bank: [''],
      correspondent_bank_swift: [''],
      correspondent_bank_account: [''],
      beneficiary_bank: [''],
      beneficiary_bank_address: [''],
      beneficiary_bank_swift: [''],
      beneficiary_account: ['']
    });
  }

  saveUserData() {
    const controls = this.userForm.controls;
    const payload = new URLSearchParams();
    payload.append('firstName', controls.name.value);

    if (controls.surname.value) {
      payload.append('lastName', controls.surname.value);
    }

    if (controls.username.value) {
      payload.append('userName', controls.username.value);
    }

    if (this.common.isVA()) {
      const stringToSave = controls.hour.value + ':' + controls.minutes.value + ' ' + controls.ampm.value;
      payload.append('daily_start_time', stringToSave);
    }

    this.saveUserDatainProgress = true;
    this.saveUserDataSubscription = this.us.updateUserDataByUser(this.userService.userid, payload).pipe()
      .subscribe(
        (imResponse: any) => {
          this.saveUserDatainProgress = false;
          if (this.config.isDev) {
            console.log(`UserProfileEditComponent.getUserData Completed`);
          }
        },
        (error: HttpErrorResponse) => {
          if (this.config.isDev) {
            console.log(`UserProfileEditComponent.getUserData Failed`);
          }
        }
      );
  }

  updatePassword() {
    const controls = this.passwordForm.controls;

    if (controls.password.value !== controls.repeat_password.value) {
      this.errorMessage = 'Passwords do not match.';
    } else {
      this.errorMessage = '';

      const payload = new URLSearchParams();
      payload.append('old_password', controls.old_password.value);
      payload.append('password', controls.password.value);

      this.savePasswordInProgress = true;
      this.saveUserDataSubscription = this.us.changeUserPasswordByUser(this.userService.userid, payload).pipe()
        .subscribe(
          (imResponse: any) => {
            this.savePasswordInProgress = false;
            if (this.config.isDev) {
              console.log(`UserProfileEditComponent.updatePassword Completed`);
            }
          },
          (error: HttpErrorResponse) => {
            this.savePasswordInProgress = false;
            // tslint:disable-next-line:no-string-literal
            this.errorMessage = error['error']['message'];
            if (this.config.isDev) {
              console.log(`UserProfileEditComponent.updatePassword Failed`);
            }
          }
        );
    }
  }

  selectAvatar($event) {
    const file: File = $event.target.files[0];
    this.avatarFile = file;
    const myReader: FileReader = new FileReader();
    this.avatarChanged = true;
    myReader.onloadend = (loadEvent: any) => {
      this.avatar = loadEvent.target.result;
    };

    myReader.readAsDataURL(file);
  }

  removeAvatar() {
    this.avatar = null;
    this.avatarChanged = true;
  }

  uploadImageThenSave() {
    this.saveUserDatainProgress = true;
    const formData = new FormData();
    formData.append('image', this.avatarFile);
    this.uploadImageSubscription = this.ws.uploadImage(formData, this.userService.userid).pipe()
      .subscribe(
        (imResponse: any) => {
          this.saveUserData();
          this.imageReSaved = true;
          console.log(`AddTeamComponent.uploadImageThenSave Completed`);
        },
        (error: HttpErrorResponse) => {
          console.log(`AddTeamComponent.uploadImageThenSave Failed`);
        }
      );
  }

  setSectionActive(sectionIdentifier: string) {
    this.activeSection = sectionIdentifier;
  }

  processInfoSave() {
    if (this.avatarChanged && this.avatar) {
      this.uploadImageThenSave();
    } else {
      this.saveUserData();
    }
  }

  deleteProfileImage() {
    this.uploadImageSubscription = this.ws.deleteProfileImage(this.userService.userid).pipe()
      .subscribe(
        (imResponse: any) => {
          location.reload();
          console.log(`AddTeamComponent.deleteProfileImage Completed`);
        },
        (error: HttpErrorResponse) => {
          console.log(`AddTeamComponent.deleteProfileImage Failed`);
        }
      );
  }

  toggleDemoMode($event: any) {
    if ($event.target.checked) {
      this.userservice.isDemoMode = true;
    } else {
      this.userservice.isDemoMode = false;
    }
  }

  shouldShowToUser() {
    let email = '';
    this.user$.subscribe((result) => {
      email = result.email;
    });
    return email.indexOf('maor') > -1 || email.indexOf('fillva.com') > -1;
  }

}
