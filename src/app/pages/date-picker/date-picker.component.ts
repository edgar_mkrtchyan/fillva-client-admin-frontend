import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DateObject } from 'src/app/common/interfaces/date.interface';

declare let moment: any;
declare let $: any;

@Component({
  selector: 'app-date-picker',
  templateUrl: './date-picker.component.html',
  styleUrls: ['./date-picker.component.scss']
})
export class DatePickerComponent implements OnInit {

  selectedStartDate: string;
  selectedEndDate: string;
  @Output() emitDateChange: EventEmitter<DateObject> = new EventEmitter<DateObject>();

  constructor() {
    this.selectedStartDate = moment().subtract(6, 'days');
    this.selectedEndDate = moment();
  }

  ngOnInit(): void {
    this.initializeDatePicker();
  }

  initializeDatePicker() {
    const picker = $('#kt_dashboard_daterangepicker');
    const start = this.selectedStartDate;
    const end = this.selectedEndDate;
    const that = this;

    function cb(start, end, label?) {
      let title = '';
      let range = '';

      if ((end - start) < 100 || label === 'Today') {
        title = 'Today:';
        range = start.format('MMM D');
      } else if (label === 'Yesterday') {
        title = 'Yesterday:';
        range = start.format('MMM D');
      } else {
        range = start.format('MMM D') + ' - ' + end.format('MMM D');
      }

      $('#kt_dashboard_daterangepicker_date').html(range);
      $('#kt_dashboard_daterangepicker_title').html(title);

      that.selectedStartDate = start;
      that.selectedEndDate = end;

      // Emit the change event here
      that.emitDateChange.emit({ startTime: that.selectedStartDate, endTime: that.selectedEndDate });
    }

    function createLabel(start, end, label?) {
      let title = '';
      let range = '';

      if ((end - start) < 100 || label === 'Today') {
        title = 'Today:';
        range = start.format('MMM D');
      } else if (label === 'Yesterday') {
        title = 'Yesterday:';
        range = start.format('MMM D');
      } else {
        range = start.format('MMM D') + ' - ' + end.format('MMM D');
      }

      $('#kt_dashboard_daterangepicker_date').html(range);
      $('#kt_dashboard_daterangepicker_title').html(title);
    }

    createLabel(start, end);

    picker.daterangepicker({
      startDate: start,
      endDate: end,
      opens: 'left',
      applyClass: 'btn-primary',
      cancelClass: 'btn-light-primary',
      ranges: {
        // tslint:disable-next-line:quotemark
        // tslint:disable-next-line:object-literal-key-quotes
        'Today': [moment(), moment()],
        // tslint:disable-next-line:object-literal-key-quotes
        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
        'This Month': [moment().startOf('month'), moment().endOf('month')],
        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
      }
    }, cb);
  }

}
