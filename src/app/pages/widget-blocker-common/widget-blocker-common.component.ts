import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/core/services/user.service';

@Component({
  selector: 'app-widget-blocker-common',
  templateUrl: './widget-blocker-common.component.html',
  styleUrls: ['./widget-blocker-common.component.scss']
})
export class WidgetBlockerCommonComponent implements OnInit {

  isDemoMode: boolean;

  constructor(private user: UserService) {
    this.isDemoMode = this.user.isDemoMode;
  }

  ngOnInit(): void {
  }

}
