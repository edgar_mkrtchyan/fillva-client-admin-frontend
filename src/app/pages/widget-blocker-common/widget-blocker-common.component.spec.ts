import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WidgetBlockerCommonComponent } from './widget-blocker-common.component';

describe('WidgetBlockerCommonComponent', () => {
  let component: WidgetBlockerCommonComponent;
  let fixture: ComponentFixture<WidgetBlockerCommonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WidgetBlockerCommonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetBlockerCommonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
