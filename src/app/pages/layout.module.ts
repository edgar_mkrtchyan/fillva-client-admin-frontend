import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InlineSVGModule } from 'ng-inline-svg';
import { PagesRoutingModule } from './pages-routing.module';
import {
  NgbDropdownModule,
  NgbProgressbarModule,
} from '@ng-bootstrap/ng-bootstrap';
import { TranslationModule } from '../modules/i18n/translation.module';
import { LayoutComponent } from './_layout/layout.component';
import { ScriptsInitComponent } from './_layout/init/scipts-init/scripts-init.component';
import { HeaderMobileComponent } from './_layout/components/header-mobile/header-mobile.component';
import { AsideComponent } from './_layout/components/aside/aside.component';
import { FooterComponent } from './_layout/components/footer/footer.component';
import { HeaderComponent } from './_layout/components/header/header.component';
import { HeaderMenuComponent } from './_layout/components/header/header-menu/header-menu.component';
import { TopbarComponent } from './_layout/components/topbar/topbar.component';
import { ExtrasModule } from '../_fillva/partials/layout/extras/extras.module';
import { LanguageSelectorComponent } from './_layout/components/topbar/language-selector/language-selector.component';
import { CoreModule } from '../_fillva/core';
import { SubheaderModule } from '../_fillva/partials/layout/subheader/subheader.module';
import { AsideDynamicComponent } from './_layout/components/aside-dynamic/aside-dynamic.component';
import { HeaderMenuDynamicComponent } from './_layout/components/header/header-menu-dynamic/header-menu-dynamic.component';
import { TrackMyTeamComponent } from './track-my-team/track-my-team.component';
import { DataTableModule } from '@pascalhonegger/ng-datatable';
import { CrystalLightboxModule } from '@crystalui/angular-lightbox';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ManageUsersComponent } from './manage-users/manage-users.component';
import { AddUserModalComponent } from './manage-users/add-user-modal/add-user-modal.component';
import { EditUserModalComponent } from './manage-users/edit-user-modal/edit-user-modal.component';
import { DeleteUserModalComponent } from './manage-users/delete-user-modal/delete-user-modal.component';
import { ChangeUserPasswordByAdminModalComponent } from './manage-users/change-user-password-by-admin-modal/change-user-password-by-admin-modal.component';
import { ClientProfileComponent } from './client-profile/client-profile.component';
import { AddCustomKpiModalComponent } from './track-my-team/add-custom-kpi-modal/add-custom-kpi-modal.component';
import { RemoveCustomKpiModalComponent } from './track-my-team/remove-custom-kpi-modal/remove-custom-kpi-modal.component';
import { CustomKpiManagementComponent } from './settings/custom-kpi-management/custom-kpi-management.component';
import { EditCustomKpiModalComponent } from './settings/custom-kpi-management/edit-custom-kpi-modal/edit-custom-kpi-modal.component';
import { DeleteCustomKpiModalComponent } from './settings/custom-kpi-management/delete-custom-kpi-modal/delete-custom-kpi-modal.component';
import { FinanceComponent } from './finance/finance.component';
import { CostPerTeamComponent } from '../_fillva/partials/content/widgets/stats/cost-per-team/cost-per-team.component';
import { CostPerVaComponent } from '../_fillva/partials/content/widgets/stats/cost-per-va/cost-per-va.component';
import { ProductivityComponent } from './productivity/productivity.component';
import { ProductivityChartComponent } from '../_fillva/partials/content/widgets/stats/productivity-chart/productivity-chart.component';
import { VaLatestActivityComponent } from '../_fillva/partials/content/widgets/lists/va-latest-activity/va-latest-activity.component';
import { VaCostPerKpiComponent } from '../_fillva/partials/content/widgets/stats/va-cost-per-kpi/va-cost-per-kpi.component';
import { VaTopKpiStatsComponent } from '../_fillva/partials/content/widgets/stats/va-top-kpi-stats/va-top-kpi-stats.component';
import { VaTimeSpentOnSitesStatsComponent } from '../_fillva/partials/content/widgets/stats/va-time-spent-on-sites-stats/va-time-spent-on-sites-stats.component';
// tslint:disable-next-line:max-line-length
import { VaTimeSpentPerKpiComponent } from '../_fillva/partials/content/widgets/stats/va-time-spent-per-kpi/va-time-spent-per-kpi.component';
import { VaRecentActivityComponent } from '../_fillva/partials/content/widgets/lists/va-recent-activity/va-recent-activity.component';
import { ScreenshotsComponent } from './screenshots/screenshots.component';
import { EmployeesSelectComponent } from './employees-select/employees-select.component';
import { TeamsComponent } from './manager/teams/teams.component';
import { AddTeamModalComponent } from './manager/teams/add-team-modal/add-team-modal.component';
import { EditTeamModalComponent } from './manager/teams/edit-team-modal/edit-team-modal.component';
import { DeleteTeamModalComponent } from './manager/teams/delete-team-modal/delete-team-modal.component';
import { TeamEmployeesComponent } from './manager/teams/team-employees/team-employees.component';
import { EmployeeFinanceComponent } from './employee-finance/employee-finance.component';
import { AllowedWebsitesModalComponent } from './manager/teams/allowed-websites-modal/allowed-websites-modal.component';
import { TeamsSelectComponent } from './manager/teams/teams-select/teams-select.component';
import { WidgetBlockerCommonComponent } from './widget-blocker-common/widget-blocker-common.component';
import { WidgetsManagementModalComponent } from './widgets-management-modal/widgets-management-modal.component';
import { PagesDatePickerComponent } from './pages-date-picker/pages-date-picker.component';
import { EllipsisPipe } from '../_fillva/core/pipes/ellipsis.pipe';
import { KpisSelectModalComponent } from '../_fillva/partials/content/widgets/stats/kpi-count/kpis-select-modal/kpis-select-modal.component';

@NgModule({
  declarations: [
    LayoutComponent,
    ScriptsInitComponent,
    HeaderMobileComponent,
    AsideComponent,
    FooterComponent,
    HeaderComponent,
    HeaderMenuComponent,
    TopbarComponent,
    LanguageSelectorComponent,
    AsideDynamicComponent,
    HeaderMenuDynamicComponent,
    TrackMyTeamComponent,
    ManageUsersComponent,
    AddUserModalComponent,
    EditUserModalComponent,
    DeleteUserModalComponent,
    ChangeUserPasswordByAdminModalComponent,
    ClientProfileComponent,
    AddCustomKpiModalComponent,
    RemoveCustomKpiModalComponent,
    CustomKpiManagementComponent,
    EditCustomKpiModalComponent,
    DeleteCustomKpiModalComponent,
    FinanceComponent,
    CostPerTeamComponent,
    CostPerVaComponent,
    ProductivityComponent,
    ProductivityChartComponent,
    VaLatestActivityComponent,
    VaCostPerKpiComponent,
    VaTopKpiStatsComponent,
    VaTimeSpentOnSitesStatsComponent,
    VaTimeSpentPerKpiComponent,
    VaRecentActivityComponent,
    ScreenshotsComponent,
    EmployeesSelectComponent,
    TeamsComponent,
    AddTeamModalComponent,
    EditTeamModalComponent,
    DeleteTeamModalComponent,
    TeamEmployeesComponent,
    EmployeeFinanceComponent,
    AllowedWebsitesModalComponent,
    TeamsSelectComponent,
    WidgetBlockerCommonComponent,
    WidgetsManagementModalComponent,
    PagesDatePickerComponent,
    EllipsisPipe
  ],
  imports: [
    CommonModule,
    PagesRoutingModule,
    TranslationModule,
    InlineSVGModule,
    ExtrasModule,
    NgbDropdownModule,
    NgbProgressbarModule,
    CoreModule,
    SubheaderModule,
    DataTableModule,
    CrystalLightboxModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  entryComponents: [
    AddUserModalComponent,
    EditUserModalComponent,
    DeleteUserModalComponent,
    ChangeUserPasswordByAdminModalComponent,
    AddCustomKpiModalComponent,
    RemoveCustomKpiModalComponent,
    AddTeamModalComponent,
    EditTeamModalComponent,
    DeleteTeamModalComponent,
    WidgetsManagementModalComponent,
    KpisSelectModalComponent
  ],
  exports: [
    CostPerTeamComponent,
    CostPerVaComponent,
    ProductivityChartComponent,
    VaLatestActivityComponent,
    VaCostPerKpiComponent,
    VaTopKpiStatsComponent,
    VaTimeSpentOnSitesStatsComponent,
    VaTimeSpentPerKpiComponent
  ]
})
export class LayoutModule { }
