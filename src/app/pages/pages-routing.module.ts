import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminGuardService } from '../core/services/admin-guard.service';
import { EmployeeGuardService } from '../core/services/employee-guard.service';
import { ManagementGuardService } from '../core/services/management-guard.service';
import { Dashboard2Component } from '../_fillva/partials/content/dashboards/dashboard2/dashboard2.component';
import { ClientProfileComponent } from './client-profile/client-profile.component';
import { EmployeeFinanceComponent } from './employee-finance/employee-finance.component';
import { FinanceComponent } from './finance/finance.component';
import { ManageUsersComponent } from './manage-users/manage-users.component';
import { TeamEmployeesComponent } from './manager/teams/team-employees/team-employees.component';
import { TeamsComponent } from './manager/teams/teams.component';
import { ProductivityComponent } from './productivity/productivity.component';
import { ScreenshotsComponent } from './screenshots/screenshots.component';
import { CustomKpiManagementComponent } from './settings/custom-kpi-management/custom-kpi-management.component';
import { TrackMyTeamComponent } from './track-my-team/track-my-team.component';
import { LayoutComponent } from './_layout/layout.component';

const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      {
        path: 'dashboard',
        loadChildren: () =>
          import('./dashboard/dashboard.module').then((m) => m.DashboardModule),
      },
      {
        path: '',
        redirectTo: '/dashboard',
        pathMatch: 'full',
      },
      {
        path: 'track-my-team',
        canActivate: [ManagementGuardService],
        component: TrackMyTeamComponent
      },
      {
        path: 'user-management',
        canActivate: [ManagementGuardService],
        component: ManageUsersComponent
      },
      {
        path: 'client-profile',
        component: ClientProfileComponent
      },
      {
        path: 'manager/settings/custom-kpi-events',
        component: CustomKpiManagementComponent,
        canActivate: [ManagementGuardService]
      },
      {
        path: 'manager/teams',
        component: TeamsComponent,
        canActivate: [ManagementGuardService]
      },
      {
        path: 'manager/teams/:id',
        component: TeamEmployeesComponent,
        canActivate: [ManagementGuardService]
      },
      {
        path: 'finance',
        component: FinanceComponent,
        canActivate: [ManagementGuardService]
      },
      {
        path: 'employee/finance',
        component: EmployeeFinanceComponent,
        canActivate: [EmployeeGuardService]
      },
      {
        path: 'productivity',
        component: ProductivityComponent,
        canActivate: [ManagementGuardService]
      },
      {
        path: 'business-insights',
        component: Dashboard2Component,
        canActivate: [ManagementGuardService]
      },
      {
        path: 'screenshots',
        component: ScreenshotsComponent,
        canActivate: [ManagementGuardService]
      },
      {
        path: '**',
        redirectTo: 'error/404',
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule { }
