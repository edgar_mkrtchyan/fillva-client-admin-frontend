import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { BaseMetricsObject } from 'src/app/common/interfaces/base-metrics.interface';
import { DateObject } from 'src/app/common/interfaces/date.interface';
import { FinanceBaseMetrics, FinanceBaseMetricsObject } from 'src/app/common/interfaces/finance-base-metrics.interface';
import { ConfigurationService } from 'src/app/core/services/configuration.service';
import { Constants } from 'src/app/core/services/constants.service';
import { FinanceService } from 'src/app/core/services/finance.service';

declare let moment: any;
declare let $: any;

@Component({
  selector: 'app-finance',
  templateUrl: './finance.component.html',
  styleUrls: ['./finance.component.scss']
})
export class FinanceComponent implements OnInit, OnDestroy {

  currentYear: number;
  currentMonth: number;
  currentMonthDisplayName: string;
  selectedStartDate: string;
  selectedEndDate: string;
  getBaseMetricsInProgress: boolean;
  getBaseMetricsSubscription: Subscription;
  baseMetrics: FinanceBaseMetricsObject;
  private unsubscribe: Subscription[] = [];

  constructor(private constants: Constants,
              private config: ConfigurationService,
              private ws: FinanceService) {
    const date = new Date();
    this.currentYear = date.getFullYear();
    this.currentMonth = date.getMonth();
    this.currentMonthDisplayName = this.constants.MONTHS_MAP[this.currentMonth];
    this.selectedStartDate = moment().subtract(6, 'days').startOf('day');
    this.selectedEndDate = moment().endOf('day');
    this.selectedStartDate = Math.floor(parseInt(this.selectedStartDate.valueOf(), 10) / 1000).toString();
    this.selectedEndDate = Math.floor(parseInt(this.selectedEndDate.valueOf(), 10) / 1000).toString();
    this.unsubscribe = [];
    this.getBaseMetricsInProgress = false;
    this.getBaseMetricsSubscription = null;
    this.baseMetrics = null;
  }

  ngOnInit(): void {
    this.getBaseMetrics();
  }

  ngOnDestroy(): void {
    this.unsubscribe.forEach((cb) => cb.unsubscribe());
  }

  monthSelector(direction: string) {
    if (direction === 'next') {
      this.currentMonth++;
      if (this.currentMonth > 11) {
        this.currentYear++;
        this.currentMonth = 0;
      }
      this.currentMonthDisplayName = this.constants.MONTHS_MAP[this.currentMonth];
    } else {
      this.currentMonth--;
      if (this.currentMonth < 0) {
        this.currentMonth = 11;
        this.currentYear--;
      }
      this.currentMonthDisplayName = this.constants.MONTHS_MAP[this.currentMonth];
    }
  }

  dateChanged($event: DateObject) {
    this.selectedStartDate = $event.startTime;
    this.selectedEndDate = $event.endTime;
    this.selectedStartDate = Math.floor(parseInt(this.selectedStartDate.valueOf(), 10) / 1000).toString();
    this.selectedEndDate = Math.floor(parseInt(this.selectedEndDate.valueOf(), 10) / 1000).toString();
    this.getBaseMetrics();
  }

  getBaseMetrics() {
    const payload = new URLSearchParams();
    payload.append('startTime', this.selectedStartDate);
    payload.append('endTime', this.selectedEndDate);
    this.getBaseMetricsInProgress = true;
    this.getBaseMetricsSubscription = this.ws.getFinanceBaseMetrics(payload).pipe()
      .subscribe(
        (imResponse: FinanceBaseMetrics) => {
          this.getBaseMetricsInProgress = false;
          this.baseMetrics = imResponse.data;
          if (this.config.isDev) {
            console.log(`FinanceComponent.getBaseMetrics Completed`);
          }
        },
        (error: HttpErrorResponse) => {
          if (this.config.isDev) {
            console.log(`FinanceComponent.getBaseMetrics Failed`);
          }
        }
      );
    this.unsubscribe.push(this.getBaseMetricsSubscription);
  }

}
