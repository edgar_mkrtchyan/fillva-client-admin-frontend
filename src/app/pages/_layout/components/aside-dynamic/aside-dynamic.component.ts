import { Component, OnInit, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';
import { CommonFunctions } from 'src/app/common/components/common.component';
import { ConfigurationService } from 'src/app/core/services/configuration.service';
import { Constants } from 'src/app/core/services/constants.service';
import { AuthService, UserModel } from 'src/app/modules/auth';
import { LayoutService, DynamicAsideMenuService } from '../../../../_fillva/core';

@Component({
  selector: 'app-aside-dynamic',
  templateUrl: './aside-dynamic.component.html',
  styleUrls: ['./aside-dynamic.component.scss']
})
export class AsideDynamicComponent implements OnInit, OnDestroy {
  menuConfig: any;
  subscriptions: Subscription[] = [];

  disableAsideSelfDisplay: boolean;
  headerLogo: string;
  brandSkin: string;
  ulCSSClasses: string;
  asideMenuHTMLAttributes: any = {};
  asideMenuCSSClasses: string;
  asideMenuDropdown;
  brandClasses: string;
  asideMenuScroll = 1;
  asideSelfMinimizeToggle = false;
  user$: Observable<UserModel>;

  currentUrl: string;

  constructor(
    private layout: LayoutService,
    private router: Router,
    private config: ConfigurationService,
    private menu: DynamicAsideMenuService,
    private common: CommonFunctions,
    private constants: Constants,
    private auth: AuthService,
    private cdr: ChangeDetectorRef) {
    this.user$ = this.auth.currentUserSubject.asObservable();
  }

  ngOnInit(): void {
    // load view settings
    this.disableAsideSelfDisplay =
      this.layout.getProp('aside.self.display') === false;
    this.brandSkin = this.layout.getProp('brand.self.theme');
    this.headerLogo = this.getLogo();
    this.ulCSSClasses = this.layout.getProp('aside_menu_nav');
    this.asideMenuCSSClasses = this.layout.getStringCSSClasses('aside_menu');
    this.asideMenuHTMLAttributes = this.layout.getHTMLAttributes('aside_menu');
    this.asideMenuDropdown = this.layout.getProp('aside.menu.dropdown') ? '1' : '0';
    this.brandClasses = this.layout.getProp('brand');
    this.asideSelfMinimizeToggle = this.layout.getProp(
      'aside.self.minimize.toggle'
    );
    this.asideMenuScroll = this.layout.getProp('aside.menu.scroll') ? 1 : 0;
    // this.asideMenuCSSClasses = `${this.asideMenuCSSClasses} ${this.asideMenuScroll === 1 ? 'scroll my-4 ps ps--active-y' : ''}`;

    // router subscription
    this.currentUrl = this.router.url.split(/[?#]/)[0];
    const routerSubscr = this.router.events.pipe(
      filter(event => event instanceof NavigationEnd)
    ).subscribe((event: NavigationEnd) => {
      this.currentUrl = event.url;
      this.cdr.detectChanges();
    });
    this.subscriptions.push(routerSubscr);

    // menu load
    const menuSubscr = this.menu.menuConfig$.subscribe(res => {
      this.menuConfig = res;
      this.cdr.detectChanges();
    });
    this.subscriptions.push(menuSubscr);
  }

  private getLogo() {
    if (this.brandSkin === 'light') {
      return './assets/media/logos/logo-dark.png';
    } else {
      const url = this.common.getUrl();
      if (url.indexOf('autods') > -1) {
        return this.constants.WHITE_LABEL['autods']['aside_logo'];
      } else if (url.indexOf('triplemars') > -1) {
        return this.constants.WHITE_LABEL['triplemars']['aside_logo'];
      } else if (url.indexOf('academie') > -1) {
        return this.constants.WHITE_LABEL['academie']['aside_logo'];
      } else {
        return this.constants.WHITE_LABEL['fillva']['aside_logo'];
      }
    }
  }

  isMenuItemActive(path) {
    if (!this.currentUrl || !path) {
      return false;
    }

    if (this.currentUrl === path) {
      return true;
    }

    if (this.currentUrl.indexOf(path) > -1) {
      return true;
    }

    return false;
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sb => sb.unsubscribe());
  }

  allowedToAccess(roles) {
    let accessible = false;
    if (!roles) {
      accessible = true;
    } else {
      const userRoles = this.common.getUserRoles();
      if (userRoles) {
        roles.forEach(role => {
          if (userRoles.indexOf(role) > -1) {
            accessible = true;
          }
        });
      }
    }
    return accessible;
  }

  demoItem(demo) {
    let accessible = false;
    if (!demo) {
      accessible = true;
    } else {
      this.user$.subscribe((result) => {
        const userEmail = result.email;
        if (userEmail.indexOf('fillva') > -1 ||
            userEmail.indexOf('maor') > -1 ||
            userEmail.indexOf('josh') > -1 ||
            userEmail.indexOf('yuval') > -1 ||
            userEmail.indexOf('test.com') > -1) {
              accessible = true;
        }
      });
    }
    return accessible;
  }

  generateExtensionDownloadLink(link?: string) {
    if (this.config.isDev) {
      return '/assets/extension/fillva-chrome-extension-dev.zip';
    } else {
      return 'https://chrome.google.com/webstore/detail/fillva/hcdlhhbojjpjfipjncodkmjobolaglmj';
    }
  }
}
