import { Component, OnInit, AfterViewInit } from '@angular/core';
import { LayoutService } from '../../../../_fillva/core';
import KTLayoutFooter from '../../../../../assets/js/layout/base/footer';
import { CommonFunctions } from 'src/app/common/components/common.component';
import { Constants } from 'src/app/core/services/constants.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
})
export class FooterComponent implements OnInit, AfterViewInit {
  footerContainerCSSClasses: string;
  currentYear: string;
  siteUrl: string;
  brandName: string;

  constructor(private layout: LayoutService,
              private common: CommonFunctions,
              private constants: Constants) {
    const currentDate = new Date();
    this.currentYear = currentDate.getFullYear().toString();
  }

  ngOnInit(): void {
    this.footerContainerCSSClasses = this.layout.getStringCSSClasses(
      'footer_container'
    );
    this.defineBrandAndLogo();
  }

  ngAfterViewInit() {
    // Init Footer
    KTLayoutFooter.init('kt_footer');
  }

  defineBrandAndLogo() {
    const url = this.common.getUrl();
    if (url.indexOf('autods') > -1) {
      this.siteUrl = this.constants.WHITE_LABEL['autods']['url'];
      this.brandName = this.constants.WHITE_LABEL['autods']['name'];
    } else if (url.indexOf('triplemars') > -1) {
      this.siteUrl = this.constants.WHITE_LABEL['triplemars']['url'];
      this.brandName = this.constants.WHITE_LABEL['triplemars']['name'];
    } else if (url.indexOf('academie') > -1) {
      this.siteUrl = this.constants.WHITE_LABEL['academie']['url'];
      this.brandName = this.constants.WHITE_LABEL['academie']['name'];
    } else {
      this.siteUrl = this.constants.WHITE_LABEL['fillva']['url'];
      this.brandName = this.constants.WHITE_LABEL['fillva']['name'];
    }
  }
}
