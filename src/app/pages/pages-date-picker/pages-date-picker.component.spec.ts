import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PagesDatePickerComponent } from './pages-date-picker.component';

describe('PagesDatePickerComponent', () => {
  let component: PagesDatePickerComponent;
  let fixture: ComponentFixture<PagesDatePickerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PagesDatePickerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PagesDatePickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
