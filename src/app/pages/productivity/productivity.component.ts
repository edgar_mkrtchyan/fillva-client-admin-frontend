import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { BaseMetrics, BaseMetricsObject } from 'src/app/common/interfaces/base-metrics.interface';
import { DateObject } from 'src/app/common/interfaces/date.interface';
import { ProductivityStatistics, ProductivityStatisticsObject } from 'src/app/common/interfaces/productivity-statistics.interface';
import { TeamObject, Teams } from 'src/app/common/interfaces/team.interface';
import { UserObject } from 'src/app/common/interfaces/user.interface';
import { ConfigurationService } from 'src/app/core/services/configuration.service';
import { DashboardService } from 'src/app/core/services/dashboard.service';
import { ProductivityService } from 'src/app/core/services/productivity.service';
import { UserAccountsService } from 'src/app/core/services/user-accounts.service';
import { UserService } from 'src/app/core/services/user.service';

declare let moment: any;
declare let $: any;

@Component({
  selector: 'app-productivity',
  templateUrl: './productivity.component.html',
  styleUrls: ['./productivity.component.scss']
})
export class ProductivityComponent implements OnInit, OnDestroy {

  selectedEmployee: UserObject;
  selectedTeam: number;
  noAvatarImagePath: string;
  changeUserTeamInProgress: boolean;
  private subscription: Subscription[] = [];
  changeteamSubscription: Subscription;
  teamChanged: boolean;
  selectedStartDate: string;
  selectedEndDate: string;
  getProductivityStatisticsInProgress: boolean;
  getProductivityStatisticsSubscription: Subscription;
  productivityStatistics: ProductivityStatisticsObject[];
  colourCodes: Array<string>;
  getBaseMetricsInProgress: boolean;
  getBaseMetricsSubscription: Subscription;
  baseMetrics: BaseMetricsObject;

  constructor(private config: ConfigurationService,
              private ps: ProductivityService,
              private ds: DashboardService,
              private ws: UserAccountsService) {
    this.selectedEmployee = null;
    this.selectedTeam = null;
    this.noAvatarImagePath = '../../../assets/media/users/blank.png';
    this.changeteamSubscription = null;
    this.teamChanged = false;
    this.selectedStartDate = moment().subtract(6, 'days');
    this.selectedEndDate = moment();
    this.selectedStartDate = Math.floor(parseInt(this.selectedStartDate.valueOf(), 10) / 1000).toString();
    this.selectedEndDate = Math.floor(parseInt(this.selectedEndDate.valueOf(), 10) / 1000).toString();
    this.getProductivityStatisticsInProgress = false;
    this.getProductivityStatisticsSubscription = null;
    this.productivityStatistics = [];
    this.colourCodes = ['success', 'info', 'warning', 'primary'];
    this.getBaseMetricsInProgress = false;
    this.getBaseMetricsSubscription = null;
    this.baseMetrics = null;
  }

  ngOnInit(): void {

  }

  ngOnDestroy(): void {
    this.subscription.forEach((sb) => sb.unsubscribe());
  }

  dateChanged($event: DateObject) {
    this.selectedStartDate = $event.startTime;
    this.selectedEndDate = $event.endTime;
    this.selectedStartDate = Math.floor(parseInt(this.selectedStartDate.valueOf(), 10) / 1000).toString();
    this.selectedEndDate = Math.floor(parseInt(this.selectedEndDate.valueOf(), 10) / 1000).toString();
    this.getStatistics();
    this.getBaseMetrics();
  }

  selectEmployee(employee: UserObject) {
    this.selectedEmployee = employee;
    this.getStatistics();
    this.getBaseMetrics();
    this.selectedTeam = employee.team_id;
  }

  selectTeam(teamId: number) {
    this.selectedTeam = teamId;
    this.teamChanged = true;
  }

  saveTeamChange() {
    // Saving the user data
    this.changeUserTeamInProgress = true;
    this.changeteamSubscription = this.ws.addEmployeeToTeam(this.selectedTeam, this.selectedEmployee.id.toString()).pipe()
      .subscribe(
        (imResponse: any) => {
          this.changeUserTeamInProgress = false;
          this.teamChanged = false;
          if (this.config.isDev) {
            console.log(`ProductivityComponent.saveTeamChange Completed`);
          }
        },
        (error: HttpErrorResponse) => {
          if (this.config.isDev) {
            console.log(`ProductivityComponent.saveTeamChange Failed`);
          }
        }
    );
    this.subscription.push(this.changeteamSubscription);
  }

  getStatistics() {
    const payload = new URLSearchParams();
    payload.append('startTime', this.selectedStartDate);
    payload.append('endTime', this.selectedEndDate);
    this.getProductivityStatisticsInProgress = true;
    this.getProductivityStatisticsSubscription = this.ps.getStatistics(payload, this.selectedEmployee.id).pipe()
      .subscribe(
        (imResponse: ProductivityStatistics) => {
          this.getProductivityStatisticsInProgress = false;
          this.productivityStatistics = imResponse.data;
          if (this.config.isDev) {
            console.log(`Dashboard1Component.getStatistics Completed`);
          }
        },
        (error: HttpErrorResponse) => {
          if (this.config.isDev) {
            console.log(`Dashboard1Component.getStatistics Failed`);
          }
        }
      );
    this.subscription.push(this.getProductivityStatisticsSubscription);
  }

  returnColourCode(index: number) {
    return this.colourCodes[index];
  }

  getBaseMetrics() {
    const payload = new URLSearchParams();
    payload.append('startTime', this.selectedStartDate);
    payload.append('endTime', this.selectedEndDate);
    this.getBaseMetricsInProgress = true;
    this.getBaseMetricsSubscription = this.ds.getBaseMetricsPerVa(payload, this.selectedEmployee.id).pipe()
      .subscribe(
        (imResponse: BaseMetrics) => {
          this.getBaseMetricsInProgress = false;
          this.baseMetrics = imResponse.metrics;
          if (this.config.isDev) {
            console.log(`ProductivityComponent.getBaseMetrics Completed`);
          }
        },
        (error: HttpErrorResponse) => {
          if (this.config.isDev) {
            console.log(`ProductivityComponent.getBaseMetrics Failed`);
          }
        }
      );
    this.subscription.push(this.getBaseMetricsSubscription);
  }

  definePositionOrNagative(value: string) {
    if (value && value.indexOf('-') > -1) {
      return 'symbol-light-danger';
    } else {
      return 'symbol-light-success';
    }
  }

  defineArrowDirection(value: string) {
    if (value && value.indexOf('-') > -1) {
      return '&darr;';
    } else {
      return '&uarr;';
    }
  }

  differenceAvailable(value: string) {
    if (value === '00:00' || value === '0') {
      return false;
    } else {
      return true;
    }
  }

}
