import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TeamEmployeesComponent } from './team-employees.component';

describe('TeamEmployeesComponent', () => {
  let component: TeamEmployeesComponent;
  let fixture: ComponentFixture<TeamEmployeesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TeamEmployeesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamEmployeesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
