import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { CommonFunctions } from 'src/app/common/components/common.component';
import { Employees, UserObject, Users } from 'src/app/common/interfaces/user.interface';
import { ConfigurationService } from 'src/app/core/services/configuration.service';
import { UserAccountsService } from 'src/app/core/services/user-accounts.service';
import { UserService } from 'src/app/core/services/user.service';

@Component({
  selector: 'app-team-employees',
  templateUrl: './team-employees.component.html',
  styleUrls: ['./team-employees.component.scss']
})
export class TeamEmployeesComponent implements OnInit, OnDestroy {

  getUsersInProgress: boolean;
  users: UserObject[];
  usersWithoutTeam: UserObject[];
  managerEmployees: UserObject[];
  usersToAddToTeam: Array<string>;
  usersToRemoveFromTeam: Array<string>;
  private unsubscribe: Subscription[] = [];
  sortedBy: string;
  sortOrder: string;
  teamId: number;
  getTeamEmployeesInProgress: boolean;
  getTeamEmployeesSubscription: Subscription;
  getEmployeesSubscription: Subscription;
  getEmployeesInProgress: boolean;
  addEmployeesToTeamSubscription: Subscription;
  addEmployeesToTeamInProgress: boolean;
  removeEmployeesFromTeamInProgress: boolean;
  removeEmployeesFromTeamSubscription: Subscription;

  constructor(private ws: UserAccountsService,
              private common: CommonFunctions,
              private user: UserService,
              private route: ActivatedRoute,
              private config: ConfigurationService) {
    this.getUsersInProgress = false;
    this.getTeamEmployeesInProgress = false;
    this.users = [];
    this.usersWithoutTeam = [];
    this.sortedBy = '';
    this.sortOrder = '';
    this.usersToAddToTeam = [];
    this.usersToRemoveFromTeam = [];
    this.teamId = this.route.snapshot.params.id;
    this.getTeamEmployeesSubscription = null;
    this.getEmployeesInProgress = false;
    this.addEmployeesToTeamInProgress = false;
    this.addEmployeesToTeamSubscription = null;
    this.removeEmployeesFromTeamInProgress = false;
    this.removeEmployeesFromTeamSubscription = null;
    this.managerEmployees = [];
  }

  ngOnInit(): void {
    this.getEmployees();
  }

  ngOnDestroy() {
    this.unsubscribe.forEach((sb) => sb.unsubscribe());
  }

  getEmployees() {
    this.getEmployeesInProgress = true;
    // If the entered user is a manager - get only his users
    let api = this.ws.getUsers();
    if (this.common.isManager()) {
      api = this.ws.getManagerUsers(this.user.userid);
    }
    this.getEmployeesSubscription = api.pipe()
      .subscribe(
        (imResponse: Users) => {
          this.getEmployeesInProgress = false;
          this.getTeamEmployees();
          // Filtering the employees without a team
          this.usersWithoutTeam = imResponse.users.filter((user) => !user.team_id);
          this.managerEmployees = imResponse.users;
          if (this.config.isDev) {
            console.log(`ManageUsersComponent.getEmmployees Completed`);
          }
        },
        (error: HttpErrorResponse) => {
          if (this.config.isDev) {
            console.log(`ManageUsersComponent.getEmmployees Failed`);
          }
        }
      );
    this.unsubscribe.push(this.getEmployeesSubscription);
  }

  // Used to identify the current sort order in the UI
  returnSortOrderArrow(sortColumnIdentifier: string) {
    if (sortColumnIdentifier === this.sortedBy) {
      if (this.sortOrder === 'asc') {
        return 'fa-sort-up';
      } else if (this.sortOrder === 'desc') {
        return 'fa-sort-down';
      }
    } else {
      return 'fa-sort';
    }
  }

  sortOrderChange($event) {
    this.sortOrder = $event;
  }

  sortByChange($event) {
    this.sortedBy = $event;
  }

  toggleEmployeeInList(userId: string, action: string) {
    if (action === 'remove') {
      if (this.usersToRemoveFromTeam.indexOf(userId) > -1) {
        const index = this.usersToRemoveFromTeam.indexOf(userId);
        if (index > -1) {
          this.usersToRemoveFromTeam.splice(index, 1);
        }
      } else {
        this.usersToRemoveFromTeam.push(userId);
      }
    } else if (action === 'add') {
      if (this.usersToAddToTeam.indexOf(userId) > -1) {
        const index = this.usersToAddToTeam.indexOf(userId);
        if (index > -1) {
          this.usersToAddToTeam.splice(index, 1);
        }
      } else {
        this.usersToAddToTeam.push(userId);
      }
    }
  }

  getTeamEmployees() {
    this.getTeamEmployeesInProgress = true;
    this.getTeamEmployeesSubscription = this.ws.getTeamEmployees(this.teamId).pipe()
      .subscribe(
        (imResponse: Employees) => {
          this.getTeamEmployeesInProgress = false;
          this.users = imResponse.employees;
          if (this.config.isDev) {
            console.log(`ProductivityComponent.getTeamEmployees Completed`);
          }
        },
        (error: HttpErrorResponse) => {
          if (this.config.isDev) {
            console.log(`ProductivityComponent.getTeamEmployees Failed`);
          }
        }
      );
    this.unsubscribe.push(this.getTeamEmployeesSubscription);
  }

  addEmployeesToTeam() {
    // tslint:disable-next-line:prefer-for-of
    for (let index = 0; index < this.usersToAddToTeam.length; index++) {
      const user = this.usersToAddToTeam[index];
      this.addEmployeesToTeamInProgress = true;
      this.addEmployeesToTeamSubscription = this.ws.addEmployeeToTeam(this.teamId, user).pipe()
        .subscribe(
          (imResponse: any) => {
            if (index === this.usersToAddToTeam.length - 1) {
              this.addEmployeesToTeamInProgress = false;
              this.getEmployees();
              this.getTeamEmployees();
            }
            if (this.config.isDev) {
              console.log(`ProductivityComponent.addEmployeesToTeam Completed`);
            }
          },
          (error: HttpErrorResponse) => {
            if (this.config.isDev) {
              console.log(`ProductivityComponent.addEmployeesToTeam Failed`);
            }
          }
        );
    }
    this.unsubscribe.push(this.addEmployeesToTeamSubscription);
  }

  removeEmployeesFromTeam() {
    // tslint:disable-next-line:prefer-for-of
    for (let index = 0; index < this.usersToRemoveFromTeam.length; index++) {
      const user = this.usersToRemoveFromTeam[index];
      this.removeEmployeesFromTeamInProgress = true;
      this.removeEmployeesFromTeamSubscription = this.ws.removeEmployeeFromTeam(this.teamId, user).pipe()
        .subscribe(
          (imResponse: any) => {
            if (index === this.usersToRemoveFromTeam.length - 1) {
              this.removeEmployeesFromTeamInProgress = false;
              this.getEmployees();
              this.getTeamEmployees();
            }
            if (this.config.isDev) {
              console.log(`ProductivityComponent.addEmployeesToTeam Completed`);
            }
          },
          (error: HttpErrorResponse) => {
            if (this.config.isDev) {
              console.log(`ProductivityComponent.addEmployeesToTeam Failed`);
            }
          }
        );
    }
    this.unsubscribe.push(this.removeEmployeesFromTeamSubscription);
  }

  isEmployeeFromDefaultTeam(employee: UserObject) {
    const employeeTeam = this.managerEmployees.filter((em) => em.id === employee.id);
    if (employeeTeam.length > 0) {
      if (employeeTeam[0].team_name === 'Default') {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

}
