import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { CommonFunctions } from 'src/app/common/components/common.component';
import { TeamObject } from 'src/app/common/interfaces/team.interface';
import { ConfigurationService } from 'src/app/core/services/configuration.service';
import { Constants } from 'src/app/core/services/constants.service';
import { UserAccountsService } from 'src/app/core/services/user-accounts.service';
import { UserService } from 'src/app/core/services/user.service';

@Component({
  selector: 'app-edit-team-modal',
  templateUrl: './edit-team-modal.component.html',
  styleUrls: ['./edit-team-modal.component.scss']
})
export class EditTeamModalComponent implements OnInit, OnDestroy {

  @Input() team: TeamObject;
  editTeamInProgress: boolean;
  editTeamSubscription: Subscription;
  teamForm: FormGroup;
  private unsubscribe: Subscription[] = [];

  constructor(public activeModal: NgbActiveModal,
              private us: UserAccountsService,
              private formBuilder: FormBuilder,
              private config: ConfigurationService) {
    this.teamForm = null;
    this.editTeamSubscription = null;
    this.editTeamInProgress = false;
  }

  ngOnInit(): void {
    this.buildTeamForm();
  }

  ngOnDestroy() {
    this.unsubscribe.forEach((sb) => sb.unsubscribe());
  }

  buildTeamForm() {
    this.teamForm = this.formBuilder.group({
      team_name: [this.team.team_name, Validators.required]
    });
  }

  closeModal(refresh?: string) {
    if (refresh) {
      this.activeModal.close('refresh');
    } else {
      this.activeModal.close();
    }
  }

  saveTeam() {
    const controls = this.teamForm.controls;

    const payload = new URLSearchParams();
    payload.append('team_name', controls.team_name.value);

    // Saving the Team
    this.editTeamInProgress = true;
    this.editTeamSubscription = this.us.updateTeam(payload, this.team.id).pipe()
      .subscribe(
        (imResponse: any) => {
          this.closeModal('true');
          if (this.config.isDev) {
            console.log(`AddTeamModalComponent.saveTeam Completed`);
          }
        },
        (error: HttpErrorResponse) => {
          if (this.config.isDev) {
            console.log(`EditTeamModalComponent.saveTeam Failed`);
          }
        }
      );
    this.unsubscribe.push(this.editTeamSubscription);
  }

}
