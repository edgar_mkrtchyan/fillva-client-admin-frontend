import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { CommonFunctions } from 'src/app/common/components/common.component';
import { ConfigurationService } from 'src/app/core/services/configuration.service';
import { UserAccountsService } from 'src/app/core/services/user-accounts.service';

@Component({
  selector: 'app-delete-team-modal',
  templateUrl: './delete-team-modal.component.html',
  styleUrls: ['./delete-team-modal.component.scss']
})
export class DeleteTeamModalComponent implements OnInit, OnDestroy {

  @Input() teamId: number;

  deleteTeamInProgress: boolean;
  deleteTeamSubscription: Subscription;
  private unsubscribe: Subscription[] = [];

  constructor(private us: UserAccountsService,
              private common: CommonFunctions,
              private config: ConfigurationService,
              public activeModal: NgbActiveModal) {
    this.deleteTeamInProgress = false;
    this.deleteTeamSubscription = null;
  }

  ngOnInit(): void {
  }

  ngOnDestroy() {
    this.unsubscribe.forEach((sb) => sb.unsubscribe());
  }

  deleteTeam() {
    this.deleteTeamInProgress = true;
    this.deleteTeamSubscription = this.us.deleteTeam(this.teamId).pipe()
      .subscribe(
        (imResponse: any) => {
          this.closeModal('refresh');
          if (this.config.isDev) {
            console.log(`DeleteUserModalComponent.deleteUser Completed`);
          }
        },
        (error: HttpErrorResponse) => {
          if (this.config.isDev) {
            console.log(`DeleteUserModalComponent.deleteUser Failed`);
          }
        }
    );
    this.unsubscribe.push(this.deleteTeamSubscription);
  }

  closeModal(refresh?: string) {
    if (refresh) {
      this.activeModal.close('refresh');
    } else {
      this.activeModal.close();
    }
  }

}
