import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { CommonFunctions } from 'src/app/common/components/common.component';
import { TeamObject, Teams } from 'src/app/common/interfaces/team.interface';
import { ConfigurationService } from 'src/app/core/services/configuration.service';
import { UserAccountsService } from 'src/app/core/services/user-accounts.service';
import { UserService } from 'src/app/core/services/user.service';
import { AddTeamModalComponent } from './add-team-modal/add-team-modal.component';
import { AllowedWebsitesModalComponent } from './allowed-websites-modal/allowed-websites-modal.component';
import { DeleteTeamModalComponent } from './delete-team-modal/delete-team-modal.component';
import { EditTeamModalComponent } from './edit-team-modal/edit-team-modal.component';

@Component({
  selector: 'app-teams',
  templateUrl: './teams.component.html',
  styleUrls: ['./teams.component.scss']
})
export class TeamsComponent implements OnInit, OnDestroy {

  teams: TeamObject[];
  getTeamsSubscripition: Subscription;
  getTeamsInProgress: boolean;
  private unsubscribe: Subscription[] = [];
  sortedBy: string;
  sortOrder: string;

  constructor(private ws: UserAccountsService,
              private common: CommonFunctions,
              private user: UserService,
              private config: ConfigurationService,
              private modalService: NgbModal) {
    this.teams = [];
    this.getTeamsSubscripition = null;
    this.getTeamsInProgress = false;
    this.sortedBy = '';
    this.sortOrder = '';
   }

  ngOnInit(): void {
    this.getTeams();
  }

  ngOnDestroy() {
    this.unsubscribe.forEach((sb) => sb.unsubscribe());
  }

  editTeam(team: TeamObject) {
    const modalRef = this.modalService.open(EditTeamModalComponent);
    modalRef.componentInstance.team = team;

    modalRef.result.then((res) => {
      if (res === 'refresh') {
        this.getTeams();
      }
    });
  }

  deleteTeam(team: TeamObject) {
    const modalRef = this.modalService.open(DeleteTeamModalComponent);
    modalRef.componentInstance.teamId = team.id;

    modalRef.result.then((res) => {
      if (res === 'refresh') {
        this.getTeams();
      }
    });
  }

  openAddNewTeamModal() {
    const modalRef = this.modalService.open(AddTeamModalComponent);

    modalRef.result.then((res) => {
      if (res === 'refresh') {
        this.getTeams();
      }
    });
  }

  getTeams() {
    this.getTeamsInProgress = true;
    this.getTeamsSubscripition = this.ws.getTeams(this.user.userid).pipe(
      finalize(() => {
        this.common.cleanupSubscription(this.getTeamsSubscripition);
      }))
      .subscribe(
        (imResponse: Teams) => {
          this.getTeamsInProgress = false;
          this.teams = imResponse.teams;
          if (this.config.isDev) {
            console.log(`ManageUsersComponent.getUsers Completed`);
          }
        },
        (error: HttpErrorResponse) => {
          if (this.config.isDev) {
            console.log(`ManageUsersComponent.getUsers Failed`);
          }
        }
      );
    this.unsubscribe.push(this.getTeamsSubscripition);
  }

  // Used to identify the current sort order in the UI
  returnSortOrderArrow(sortColumnIdentifier: string) {
    if (sortColumnIdentifier === this.sortedBy) {
      if (this.sortOrder === 'asc') {
        return 'fa-sort-up';
      } else if (this.sortOrder === 'desc') {
        return 'fa-sort-down';
      }
    } else {
      return 'fa-sort';
    }
  }

  sortOrderChange($event) {
    this.sortOrder = $event;
  }

  sortByChange($event) {
    this.sortedBy = $event;
  }

  manageAllowedWebsites(teamId: number, allowedWebsites: Array<string>) {
    const modalRef = this.modalService.open(AllowedWebsitesModalComponent);
    modalRef.componentInstance.teamId = teamId;
    modalRef.componentInstance.allowedWebsites = allowedWebsites;

    modalRef.result.then((res) => {
      if (res === 'refresh') {
        this.getTeams();
      }
    });
  }

}
