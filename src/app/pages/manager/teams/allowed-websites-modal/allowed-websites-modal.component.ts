import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { CommonFunctions } from 'src/app/common/components/common.component';
import { ConfigurationService } from 'src/app/core/services/configuration.service';
import { Constants } from 'src/app/core/services/constants.service';
import { UserAccountsService } from 'src/app/core/services/user-accounts.service';
import { UserService } from 'src/app/core/services/user.service';

@Component({
  selector: 'app-allowed-websites-modal',
  templateUrl: './allowed-websites-modal.component.html',
  styleUrls: ['./allowed-websites-modal.component.scss']
})
export class AllowedWebsitesModalComponent implements OnInit, OnDestroy {

  actionInProgress: boolean;
  addWebSitesSubscription: Subscription;
  websiteForm: FormGroup;
  private unsubscribe: Subscription[] = [];
  @Input() teamId: number;
  @Input() allowedWebsites: Array<string>;
  websitesToRemove: Array<string>;
  websitesToAdd: Array<string>;

  constructor(public activeModal: NgbActiveModal,
              private us: UserAccountsService,
              private formBuilder: FormBuilder,
              private common: CommonFunctions,
              private user: UserService,
              private config: ConfigurationService,
              private constants: Constants) {
    this.websiteForm = null;
    this.addWebSitesSubscription = null;
    this.actionInProgress = false;
    this.websitesToRemove = [];
    this.websitesToAdd = [];
  }

  ngOnInit(): void {
    this.buildWebsiteForm();
    this.allowedWebsites.forEach(website => {
      this.websitesToAdd.push(website);
    });
  }

  ngOnDestroy() {
    this.unsubscribe.forEach((sb) => sb.unsubscribe());
  }

  buildWebsiteForm() {
    this.websiteForm = this.formBuilder.group({
      website: ['', Validators.required]
    });
  }

  closeModal(refresh?: string) {
    if (refresh) {
      this.activeModal.close('refresh');
    } else {
      this.activeModal.close();
    }
  }

  save() {
    const payload = new URLSearchParams();
    payload.append('allowed_websites', JSON.stringify(this.websitesToAdd));

    // Creating the Team
    this.actionInProgress = true;
    this.addWebSitesSubscription = this.us.addAllowedWebSites(payload, this.teamId).pipe()
      .subscribe(
        (imResponse: any) => {
          this.closeModal('true');
          if (this.config.isDev) {
            console.log(`AllowedWebsitesModalComponent.save Completed`);
          }
        },
        (error: HttpErrorResponse) => {
          if (this.config.isDev) {
            console.log(`AllowedWebsitesModalComponent.save Failed`);
          }
        }
      );
    this.unsubscribe.push(this.addWebSitesSubscription);
  }

  selectWebSiteToRemove(website: string, action: string) {
    if (action === 'remove') {
      if (this.websitesToRemove.indexOf(website) > -1) {
        const index = this.websitesToRemove.indexOf(website);
        if (index > -1) {
          this.websitesToRemove.splice(index, 1);
        }
      } else {
        this.websitesToRemove.push(website);
      }
    }
  }

  removeWebSitesFromList() {
    this.websitesToRemove.forEach(website => {
      const index = this.websitesToAdd.indexOf(website);
      if (index > -1) {
        this.websitesToAdd.splice(index, 1);
      }
      this.websitesToRemove = this.websitesToRemove.filter((ws) => ws !== website);
    });
  }

  addWebSiteToList() {
    const controls = this.websiteForm.controls;
    const website = controls.website.value;
    if (this.websitesToAdd.indexOf(website) < 0) {
      this.websitesToAdd.push(website);
      controls.website.setValue('');
    } else {
      controls.website.setValue('');
    }
  }

}
