import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllowedWebsitesModalComponent } from './allowed-websites-modal.component';

describe('AllowedWebsitesModalComponent', () => {
  let component: AllowedWebsitesModalComponent;
  let fixture: ComponentFixture<AllowedWebsitesModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllowedWebsitesModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AllowedWebsitesModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
