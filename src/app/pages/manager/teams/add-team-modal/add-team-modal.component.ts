import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { CommonFunctions } from 'src/app/common/components/common.component';
import { ConfigurationService } from 'src/app/core/services/configuration.service';
import { Constants } from 'src/app/core/services/constants.service';
import { UserAccountsService } from 'src/app/core/services/user-accounts.service';
import { UserService } from 'src/app/core/services/user.service';

@Component({
  selector: 'app-add-team-modal',
  templateUrl: './add-team-modal.component.html',
  styleUrls: ['./add-team-modal.component.scss']
})
export class AddTeamModalComponent implements OnInit, OnDestroy {

  createTeamInProgress: boolean;
  createTeamSubscription: Subscription;
  teamForm: FormGroup;
  private unsubscribe: Subscription[] = [];

  constructor(public activeModal: NgbActiveModal,
              private us: UserAccountsService,
              private formBuilder: FormBuilder,
              private common: CommonFunctions,
              private user: UserService,
              private config: ConfigurationService,
              private constants: Constants) {
    this.teamForm = null;
    this.createTeamSubscription = null;
    this.createTeamInProgress = false;
  }

  ngOnInit(): void {
    this.buildTeamForm();
  }

  ngOnDestroy() {
    this.unsubscribe.forEach((sb) => sb.unsubscribe());
  }

  buildTeamForm() {
    this.teamForm = this.formBuilder.group({
      team_name: ['', Validators.required]
    });
  }

  closeModal(refresh?: string) {
    if (refresh) {
      this.activeModal.close('refresh');
    } else {
      this.activeModal.close();
    }
  }

  addTeam() {
    const controls = this.teamForm.controls;

    const payload = new URLSearchParams();
    payload.append('team_name', controls.team_name.value);

    // Creating the Team
    this.createTeamInProgress = true;
    this.createTeamSubscription = this.us.createTeam(payload, this.user.userid).pipe()
      .subscribe(
        (imResponse: any) => {
          this.closeModal('true');
          if (this.config.isDev) {
            console.log(`AddTeamModalComponent.createTeam Completed`);
          }
        },
        (error: HttpErrorResponse) => {
          if (this.config.isDev) {
            console.log(`AddTeamModalComponent.createTeam Failed`);
          }
        }
    );
    this.unsubscribe.push(this.createTeamSubscription);
  }

}
