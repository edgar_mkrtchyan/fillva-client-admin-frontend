import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnDestroy, OnInit, EventEmitter, Output } from '@angular/core';
import { Subscription } from 'rxjs';
import { TeamObject, Teams } from 'src/app/common/interfaces/team.interface';
import { ConfigurationService } from 'src/app/core/services/configuration.service';
import { UserAccountsService } from 'src/app/core/services/user-accounts.service';
import { UserService } from 'src/app/core/services/user.service';

@Component({
  selector: 'app-teams-select',
  templateUrl: './teams-select.component.html',
  styleUrls: ['./teams-select.component.scss']
})
export class TeamsSelectComponent implements OnInit, OnDestroy {

  getTeamsInProgress: boolean;
  getTeamsSubscripition: Subscription;
  private unsubscribe: Subscription[] = [];
  teams: TeamObject[];
  @Input() selectedTeam: number;
  @Output() teamSelected: EventEmitter<number> = new EventEmitter<number>();

  constructor(private ws: UserAccountsService,
              private user: UserService,
              private config: ConfigurationService) {
      this.getTeamsInProgress = false;
      this.getTeamsSubscripition = null;
      this.teams = [];
  }

  ngOnInit(): void {
    this.getTeams();
  }

  ngOnDestroy() {
    this.unsubscribe.forEach((sb) => sb.unsubscribe());
  }

  getTeams() {
    this.getTeamsInProgress = true;
    this.getTeamsSubscripition = this.ws.getTeams(this.user.userid).pipe()
      .subscribe(
        (imResponse: Teams) => {
          this.getTeamsInProgress = false;
          this.teams = imResponse.teams;
          if (this.config.isDev) {
            console.log(`ProductivityComponent.getTeams Completed`);
          }
        },
        (error: HttpErrorResponse) => {
          if (this.config.isDev) {
            console.log(`ProductivityComponent.getTeams Failed`);
          }
        }
      );
    this.unsubscribe.push(this.getTeamsSubscripition);
  }

  selectTeam($event: any) {
    const teamId = $event.target.value;
    this.teamSelected.emit(teamId);
  }

}
