import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomKpiManagementComponent } from './custom-kpi-management.component';

describe('CustomKpiManagementComponent', () => {
  let component: CustomKpiManagementComponent;
  let fixture: ComponentFixture<CustomKpiManagementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CustomKpiManagementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomKpiManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
