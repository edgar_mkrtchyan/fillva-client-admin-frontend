import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { CommonFunctions } from 'src/app/common/components/common.component';
import { CustomKPIs, CustomKPIsObject } from 'src/app/common/interfaces/custom-kpis.interface';
import { ConfigurationService } from 'src/app/core/services/configuration.service';
import { LogsService } from 'src/app/core/services/logs.service';
import { UserService } from 'src/app/core/services/user.service';
import { DeleteCustomKpiModalComponent } from './delete-custom-kpi-modal/delete-custom-kpi-modal.component';
import { EditCustomKpiModalComponent } from './edit-custom-kpi-modal/edit-custom-kpi-modal.component';

@Component({
  selector: 'app-custom-kpi-management',
  templateUrl: './custom-kpi-management.component.html',
  styleUrls: ['./custom-kpi-management.component.scss']
})
export class CustomKpiManagementComponent implements OnInit, OnDestroy {

  getDataInProgress: boolean;
  getDataSubscription: Subscription;
  kpis: CustomKPIsObject[];
  private unsubscribe: Subscription[] = [];
  sortedBy: string;
  sortOrder: string;

  constructor(private ws: LogsService,
              private common: CommonFunctions,
              private user: UserService,
              private config: ConfigurationService,
              private modalService: NgbModal) {
    this.getDataInProgress = false;
    this.getDataSubscription = null;
    this.kpis = [];
    this.sortedBy = '';
    this.sortOrder = '';
  }

  ngOnInit(): void {
    this.getCustomKPIs();
  }

  ngOnDestroy() {
    this.unsubscribe.forEach((sb) => sb.unsubscribe());
  }

  getCustomKPIs() {
    this.getDataInProgress = true;
    this.getDataSubscription = this.ws.getCustomKPIs().pipe(
      finalize(() => {
        this.common.cleanupSubscription(this.getDataSubscription);
      }))
      .subscribe(
        (imResponse: CustomKPIs) => {
          this.getDataInProgress = false;
          this.kpis = imResponse.custom_events;
          if (this.config.isDev) {
            console.log(`ManageUsersComponent.getCustomKPIs Completed`);
          }
        },
        (error: HttpErrorResponse) => {
          if (this.config.isDev) {
            console.log(`ManageUsersComponent.getCustomKPIs Failed`);
          }
        }
      );
    this.unsubscribe.push(this.getDataSubscription);
  }

  editCustomKPI(kpi: CustomKPIsObject) {
    const modalRef = this.modalService.open(EditCustomKpiModalComponent);
    modalRef.componentInstance.kpi = kpi;

    modalRef.result.then((res) => {
      if (res === 'refresh') {
        this.getCustomKPIs();
      }
    });
  }

  deleteCustomKPI(kpi: CustomKPIsObject) {
    const modalRef = this.modalService.open(DeleteCustomKpiModalComponent);
    modalRef.componentInstance.kpi = kpi;

    modalRef.result.then((res) => {
      if (res === 'refresh') {
        this.getCustomKPIs();
      }
    });
  }

  // Used to identify the current sort order in the UI
  returnSortOrderArrow(sortColumnIdentifier: string) {
    if (sortColumnIdentifier === this.sortedBy) {
      if (this.sortOrder === 'asc') {
        return 'fa-sort-up';
      } else if (this.sortOrder === 'desc') {
        return 'fa-sort-down';
      }
    } else {
      return 'fa-sort';
    }
  }

  sortOrderChange($event) {
    this.sortOrder = $event;
  }

  sortByChange($event) {
    this.sortedBy = $event;
  }

}
