import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { CustomKPIsObject } from 'src/app/common/interfaces/custom-kpis.interface';
import { ConfigurationService } from 'src/app/core/services/configuration.service';
import { LogsService } from 'src/app/core/services/logs.service';

@Component({
  selector: 'app-edit-custom-kpi-modal',
  templateUrl: './edit-custom-kpi-modal.component.html',
  styleUrls: ['./edit-custom-kpi-modal.component.scss']
})
export class EditCustomKpiModalComponent implements OnInit, OnDestroy {

  kpiNameForm: FormGroup;
  saveInProgress: boolean;
  customKPISubscription: Subscription;
  @Input() kpi: CustomKPIsObject;
  subscriptions: Subscription[] = [];

  constructor(public activeModal: NgbActiveModal,
              private ws: LogsService,
              private config: ConfigurationService,
              private formBuilder: FormBuilder) {
    this.kpiNameForm = null;
    this.saveInProgress = false;
    this.customKPISubscription = null;
  }

  ngOnInit(): void {
    this.buildKPINameForm();
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sb => sb.unsubscribe());
  }

  buildKPINameForm() {
    this.kpiNameForm = this.formBuilder.group({
      custom_kpi_name: [this.kpi.name],
      custom_kpi_action: new FormControl({ value: this.kpi.action, disabled: true }, Validators.required),
      custom_kpi_url: [this.kpi.url],
      custom_kpi_page_title: [this.kpi.title]
    });
  }

  saveCustomKPIEvent() {
    const controls = this.kpiNameForm.controls;

    const payload = new URLSearchParams();
    payload.append('name', controls.custom_kpi_name.value);
    payload.append('url', controls.custom_kpi_url.value);
    payload.append('title', controls.custom_kpi_page_title.value);

    // Creating the user
    this.saveInProgress = true;
    this.customKPISubscription = this.ws.editCustomKPI(this.kpi.id, payload).pipe()
      .subscribe(
        (imResponse: any) => {
          this.closeModal('refresh');
          this.saveInProgress = false;
          if (this.config.isDev) {
            console.log(`EditCustomKpiModalComponent.saveCustomKPIEvent Completed`);
          }
        },
        (error: HttpErrorResponse) => {
          if (this.config.isDev) {
            console.log(`EditCustomKpiModalComponent.saveCustomKPIEvent Failed`);
          }
        }
      );
    this.subscriptions.push(this.customKPISubscription);
  }

  closeModal(refresh?: string) {
    if (refresh) {
      this.activeModal.close('refresh');
    } else {
      this.activeModal.close();
    }
  }

}
