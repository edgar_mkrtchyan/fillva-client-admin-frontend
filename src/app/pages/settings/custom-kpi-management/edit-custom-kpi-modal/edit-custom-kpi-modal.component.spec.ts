import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditCustomKpiModalComponent } from './edit-custom-kpi-modal.component';

describe('EditCustomKpiModalComponent', () => {
  let component: EditCustomKpiModalComponent;
  let fixture: ComponentFixture<EditCustomKpiModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditCustomKpiModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditCustomKpiModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
