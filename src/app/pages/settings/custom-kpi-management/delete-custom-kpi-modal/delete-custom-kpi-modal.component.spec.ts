import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteCustomKpiModalComponent } from './delete-custom-kpi-modal.component';

describe('DeleteCustomKpiModalComponent', () => {
  let component: DeleteCustomKpiModalComponent;
  let fixture: ComponentFixture<DeleteCustomKpiModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeleteCustomKpiModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteCustomKpiModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
