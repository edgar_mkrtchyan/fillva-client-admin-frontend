import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { CommonFunctions } from 'src/app/common/components/common.component';
import { CustomKPIsObject } from 'src/app/common/interfaces/custom-kpis.interface';
import { ConfigurationService } from 'src/app/core/services/configuration.service';
import { LogsService } from 'src/app/core/services/logs.service';

@Component({
  selector: 'app-delete-custom-kpi-modal',
  templateUrl: './delete-custom-kpi-modal.component.html',
  styleUrls: ['./delete-custom-kpi-modal.component.scss']
})
export class DeleteCustomKpiModalComponent implements OnInit, OnDestroy {

  @Input() kpi: CustomKPIsObject;

  deleteInProgress: boolean;
  deleteSubscription: Subscription;
  subscriptions: Subscription[] = [];

  constructor(private ws: LogsService,
              private common: CommonFunctions,
              private config: ConfigurationService,
              public activeModal: NgbActiveModal) {
    this.deleteInProgress = false;
    this.deleteSubscription = null;
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sb => sb.unsubscribe());
  }

  delete() {
    this.deleteInProgress = true;
    this.deleteSubscription = this.ws.deleteCustomKPI(this.kpi.id).pipe(
      finalize(() => {
        this.common.cleanupSubscription(this.deleteSubscription);
      }))
      .subscribe(
        (imResponse: any) => {
          this.closeModal('refresh');
          if (this.config.isDev) {
            console.log(`DeleteCustomKpiModalComponent.delete Completed`);
          }
        },
        (error: HttpErrorResponse) => {
          if (this.config.isDev) {
            console.log(`DeleteCustomKpiModalComponent.delete Failed`);
          }
        }
    );
    this.subscriptions.push(this.deleteSubscription);
  }

  closeModal(refresh?: string) {
    if (refresh) {
      this.activeModal.close('refresh');
    } else {
      this.activeModal.close();
    }
  }

}
