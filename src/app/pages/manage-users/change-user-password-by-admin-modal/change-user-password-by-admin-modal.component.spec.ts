import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangeUserPasswordByAdminModalComponent } from './change-user-password-by-admin-modal.component';

describe('ChangeUserPasswordByAdminModalComponent', () => {
  let component: ChangeUserPasswordByAdminModalComponent;
  let fixture: ComponentFixture<ChangeUserPasswordByAdminModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChangeUserPasswordByAdminModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangeUserPasswordByAdminModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
