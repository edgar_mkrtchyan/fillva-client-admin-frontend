import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { ConfigurationService } from 'src/app/core/services/configuration.service';
import { UserAccountsService } from 'src/app/core/services/user-accounts.service';

@Component({
  selector: 'app-change-user-password-by-admin-modal',
  templateUrl: './change-user-password-by-admin-modal.component.html',
  styleUrls: ['./change-user-password-by-admin-modal.component.scss']
})
export class ChangeUserPasswordByAdminModalComponent implements OnInit {

  passwordForm: FormGroup;
  errorMessage: string;
  savePasswordInProgress: boolean;
  changePasswordSubscription: Subscription;
  @Input() userId;

  constructor(public activeModal: NgbActiveModal,
              private us: UserAccountsService,
              private config: ConfigurationService,
              private formBuilder: FormBuilder) {
    this.passwordForm = null;
    this.errorMessage = '';
    this.savePasswordInProgress = false;
    this.changePasswordSubscription = null;
  }

  ngOnInit(): void {
    this.buildPasswordForm();
  }

  buildPasswordForm() {
    this.passwordForm = this.formBuilder.group({
      password: ['', [Validators.required, Validators.min(8)]],
      repeat_password: ['', [Validators.required, Validators.min(8)]],
    });
  }

  changeUserPassword() {
    const controls = this.passwordForm.controls;

    if (controls.password.value !== controls.repeat_password.value) {
      this.errorMessage = 'Passwords do not match.';
    } else {
      this.errorMessage = '';

      const payload = new URLSearchParams();
      payload.append('userId', this.userId);
      payload.append('new_password', controls.password.value);

      // Creating the user
      this.savePasswordInProgress = true;
      this.changePasswordSubscription = this.us.changeUserPasswordByAdmin(payload).pipe()
        .subscribe(
            (imResponse: any) => {
              this.closeModal();
              this.savePasswordInProgress = false;
              if (this.config.isDev) {
                console.log(`ChangeUserPasswordByAdminModalComponent.changeUserPassword Completed`);
              }
            },
            (error: HttpErrorResponse) => {
              if (this.config.isDev) {
                console.log(`ChangeUserPasswordByAdminModalComponent.changeUserPassword Failed`);
              }
            }
        );

    }
  }

  closeModal(refresh?: string) {
    if (refresh) {
      this.activeModal.close('refresh');
    } else {
      this.activeModal.close();
    }
  }

}
