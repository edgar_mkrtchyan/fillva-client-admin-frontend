import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnDestroy, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { CommonFunctions } from 'src/app/common/components/common.component';
import { Permissions } from 'src/app/common/interfaces/permissions.interface';
import { UserObject } from 'src/app/common/interfaces/user.interface';
import { ConfigurationService } from 'src/app/core/services/configuration.service';
import { Constants } from 'src/app/core/services/constants.service';
import { UserAccountsService } from 'src/app/core/services/user-accounts.service';

declare let $: any;

@Component({
  selector: 'app-edit-user-modal',
  templateUrl: './edit-user-modal.component.html',
  styleUrls: ['./edit-user-modal.component.scss']
})
export class EditUserModalComponent implements OnInit, OnDestroy {

  @Input() user: UserObject;
  userForm: FormGroup;
  editUserInProgress: boolean;
  createUserSubscription: Subscription;
  errorMessage: string;
  permissionsList: Array<string>;
  selectedPermissions: Array<string>;
  getCurrentPermissionsSubscription: Subscription;
  selectedPermissionsList: Array<string>;
  setUserPermissionsSubscription: Subscription;
  selectedTeam: number;

  constructor(public activeModal: NgbActiveModal,
              private us: UserAccountsService,
              private formBuilder: FormBuilder,
              public common: CommonFunctions,
              private config: ConfigurationService,
              private constants: Constants) {
    this.userForm = null;
    this.editUserInProgress = false;
    this.errorMessage = '';
    this.permissionsList = this.common.isManager() ? this.constants.PERMISSIONS_LIST.filter((permission) => permission === 'VA')
                                                   : this.constants.PERMISSIONS_LIST;
    this.selectedPermissions = [];
    this.createUserSubscription = null;
    this.getCurrentPermissionsSubscription = null;
    this.selectedPermissionsList = ['ADMIN'];
    this.setUserPermissionsSubscription = null;
    this.selectedTeam = null;
  }

  ngOnInit(): void {
    // Building the form
    this.buildUserForm();
    // Getting the current permissions
    this.getCurrentPermissions();
    this.selectedTeam = this.user.team_id;
  }

  ngOnDestroy(): void {
    this.common.cleanupSubscription(this.createUserSubscription);
  }

  buildUserForm() {
    this.userForm = this.formBuilder.group({
      name: [this.user.first_name, [Validators.required, Validators.min(3)]],
      surname: [this.user.last_name],
      username: [this.user.user_name],
      rate: [this.user.rate],
      position: [this.user.position],
      number_of_seats: [this.user.number_of_seats],
      tick_period: [this.user.tick_period]
    });
  }

  getCurrentPermissions() {
    this.getCurrentPermissionsSubscription = this.us.getUserPermissions(this.user.id).pipe()
      .subscribe(
        (imResponse: Permissions) => {
          this.selectedPermissionsList = imResponse.permissions;
          this.selectedPermissions = imResponse.permissions;
          setTimeout(() => {
            $('.permissions').chosen();
            this.permissionsChangeHandler();
          }, 9);
          if (this.config.isDev) {
            console.log(`EditUserModalComponent.getCurrentPermissions Completed`);
          }
        },
        (error: HttpErrorResponse) => {
          if (this.config.isDev) {
            console.log(`EditUserModalComponent.getCurrentPermissions Failed`);
          }
        }
      );
  }

  closeModal(refresh?: string) {
    if (refresh) {
      this.activeModal.close('refresh');
    } else {
      this.activeModal.close();
    }
  }

  editUser() {
    const controls = this.userForm.controls;
    if (this.selectedPermissions.length === 0) {
      this.errorMessage = 'You need to choose at least one permission.';
    } else {
      this.errorMessage = '';

      const payload = new URLSearchParams();
      payload.append('firstName', controls.name.value);

      if (controls.surname.value) {
        payload.append('lastName', controls.surname.value);
      }

      if (controls.username.value) {
        payload.append('userName', controls.username.value);
      }

      if (this.common.isManager()) {
        payload.append('rate', controls.rate.value);
        payload.append('position', controls.position.value);
        payload.append('team_id', this.selectedTeam.toString());
      }

      if (this.common.isAdmin()) {
        if (controls.number_of_seats.value) {
          payload.append('numberOfSeats', controls.number_of_seats.value);
        }
        if (controls.tick_period.value) {
          payload.append('tick_period', controls.tick_period.value);
        }
      }

      // Saving the user data
      this.editUserInProgress = true;
      this.createUserSubscription = this.us.updateUserData(this.user.id, payload).pipe(
        finalize(() => {
          this.common.cleanupSubscription(this.createUserSubscription);
        }))
        .subscribe(
          (imResponse: any) => {
            // Saving the user permissions
            this.setUserPermissions(this.user.id, this.selectedPermissions);
            this.closeModal('refresh');
            if (this.config.isDev) {
              console.log(`EditUserModalComponent.editUser Completed`);
            }
          },
          (error: HttpErrorResponse) => {
            if (this.config.isDev) {
              console.log(`EditUserModalComponent.editUser Failed`);
            }
          }
        );

    }
  }

  permissionsChangeHandler() {
    const that = this;
    $('.permissions').unbind('change');
    $('.permissions').on('change', function(evt, params) {
      const $s = $(this);

      that.selectedPermissions = $s.val();
    });
  }

  setUserPermissions(userId: number, permissions: Array<string>) {

    const payload = new URLSearchParams();
    permissions.forEach(permission => {
      payload.append('permissions[]', permission);
    });

    this.setUserPermissionsSubscription = this.us.setUserPermissions(userId, payload).pipe()
      .subscribe(
        (imResponse: any) => {
          this.closeModal('refresh');
          if (this.config.isDev) {
            console.log(`EditUserModalComponent.setUserPermissions Completed`);
          }
        },
        (error: HttpErrorResponse) => {
          if (this.config.isDev) {
            console.log(`EditUserModalComponent.setUserPermissions Failed`);
          }
        }
      );
  }

  selectTeam(teamId: number) {
    this.selectedTeam = teamId;
  }

}
