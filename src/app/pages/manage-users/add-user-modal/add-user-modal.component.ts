import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { CommonFunctions } from 'src/app/common/components/common.component';
import { UserRegistration } from 'src/app/common/interfaces/user-registration.interface';
import { ConfigurationService } from 'src/app/core/services/configuration.service';
import { Constants } from 'src/app/core/services/constants.service';
import { UserAccountsService } from 'src/app/core/services/user-accounts.service';
import { UserService } from 'src/app/core/services/user.service';

declare let $: any;

@Component({
  selector: 'app-add-user-modal',
  templateUrl: './add-user-modal.component.html',
  styleUrls: ['./add-user-modal.component.scss']
})
export class AddUserModalComponent implements OnInit, OnDestroy {

  userForm: FormGroup;
  createUserInProgress: boolean;
  createUserSubscription: Subscription;
  errorMessage: string;
  permissionsList: Array<string>;
  selectedPermissions: Array<string>;
  setUserPermissionsSubscription: Subscription;
  selectedTeam: number;

  constructor(public activeModal: NgbActiveModal,
              private us: UserAccountsService,
              private formBuilder: FormBuilder,
              public common: CommonFunctions,
              private user: UserService,
              private config: ConfigurationService,
              private constants: Constants) {
    this.userForm = null;
    this.createUserInProgress = false;
    this.errorMessage = '';
    // If the user is a Manager - he should see only VA in permissions list
    this.permissionsList = this.common.isManager() ? this.constants.PERMISSIONS_LIST.filter((permission) => permission === 'VA')
                                                 : this.constants.PERMISSIONS_LIST;
    this.selectedPermissions = [];
    this.createUserSubscription = null;
    this.selectedTeam = null;
  }

  ngOnInit(): void {
    // Building the form
    this.buildUserForm();
    setTimeout(() => {
      $('.permissions').chosen();
      this.permissionsChangeHandler();
    }, 9);
  }

  ngOnDestroy(): void {
    this.common.cleanupSubscription(this.createUserSubscription);
  }

  buildUserForm() {
    this.userForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      username: ['', [Validators.required, Validators.min(3)]],
      password: ['', [Validators.required, Validators.min(8)]],
      repeat_password: ['', [Validators.required, Validators.min(8)]],
      name: ['', [Validators.required, Validators.min(3)]],
      rate: [''],
      position: [''],
      number_of_seats: [''],
      tick_period: [''],
      surname: ['']
    });
  }

  closeModal(refresh?: string) {
    if (refresh) {
      this.activeModal.close('refresh');
    } else {
      this.activeModal.close();
    }
  }

  createUser() {
    const controls = this.userForm.controls;
    if (controls.password.value !== controls.repeat_password.value) {
      this.errorMessage = 'Passwords do not match.';
    } else if (this.selectedPermissions.length === 0) {
      this.errorMessage = 'You need to choose at least one permission.';
    } else {
      this.errorMessage = '';

      const payload = new URLSearchParams();
      payload.append('email', controls.email.value);
      payload.append('password', controls.password.value);
      payload.append('first_name', controls.name.value);
      payload.append('user_name', controls.username.value);

      if (controls.surname.value) {
        payload.append('last_name', controls.surname.value);
      }

      // If the user is Manager - submit the ID of that user as managerId to associate the newly created user with that manager
      if (this.common.isManager()) {
        payload.append('managerId', this.user.userid.toString());
        payload.append('rate', controls.rate.value);
        payload.append('position', controls.position.value);
        if (this.selectedTeam) {
          payload.append('team_id', this.selectedTeam.toString());
        }
      }

      if (this.common.isAdmin()) {
        if (controls.number_of_seats.value) {
          payload.append('numberOfSeats', controls.number_of_seats.value);
        }
        if (controls.tick_period.value) {
          payload.append('tick_period', controls.tick_period.value);
        }
      }

      // Creating the user
      this.createUserInProgress = true;
      this.createUserSubscription = this.us.createUser(payload).pipe(
        finalize(() => {
          this.common.cleanupSubscription(this.createUserSubscription);
        }))
        .subscribe(
          (imResponse: UserRegistration) => {
            if (imResponse.userId) {
              // Now setting the user permissions
              this.setUserPermissions(imResponse.userId, this.selectedPermissions);
              if (this.config.isDev) {
                console.log(`AddUserModalComponent.createUser Completed`);
              }
            } else { // Error
              this.errorMessage = imResponse.toString();
              this.createUserInProgress = false;
            }
          },
          (error: HttpErrorResponse) => {
            if (this.config.isDev) {
              console.log(`AddUserModalComponent.createUser Failed`);
            }
          }
        );

    }
  }

  permissionsChangeHandler() {
    const that = this;
    $('.permissions').unbind('change');
    $('.permissions').on('change', function(evt, params) {
      const $s = $(this);

      that.selectedPermissions = $s.val();
    });
  }

  setUserPermissions(userId: number, permissions: Array<string>) {

    const payload = new URLSearchParams();
    permissions.forEach(permission => {
      payload.append('permissions[]', permission);
    });

    this.setUserPermissionsSubscription = this.us.setUserPermissions(userId, payload).pipe()
      .subscribe(
        (imResponse: any) => {
          this.createUserInProgress = false;
          this.closeModal('refresh');
          if (this.config.isDev) {
            console.log(`AddUserModalComponent.setUserPermissions Completed`);
          }
        },
        (error: HttpErrorResponse) => {
          if (this.config.isDev) {
            console.log(`AddUserModalComponent.setUserPermissions Failed`);
          }
        }
      );
  }

  selectTeam(teamId: number) {
    this.selectedTeam = teamId;
  }

}
