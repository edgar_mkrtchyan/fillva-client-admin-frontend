import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { CommonFunctions } from 'src/app/common/components/common.component';
import { ConfigurationService } from 'src/app/core/services/configuration.service';
import { UserAccountsService } from 'src/app/core/services/user-accounts.service';

@Component({
  selector: 'app-delete-user-modal',
  templateUrl: './delete-user-modal.component.html',
  styleUrls: ['./delete-user-modal.component.scss']
})
export class DeleteUserModalComponent implements OnInit, OnDestroy {

  @Input() userId: number;

  deleteUserInProgress: boolean;
  deleteUserSubscription: Subscription;
  errorMessage: string;

  constructor(private us: UserAccountsService,
              private common: CommonFunctions,
              private config: ConfigurationService,
              public activeModal: NgbActiveModal) {
    this.deleteUserInProgress = false;
    this.deleteUserSubscription = null;
    this.errorMessage = '';
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.common.cleanupSubscription(this.deleteUserSubscription);
  }

  deleteUser() {
    this.deleteUserInProgress = true;
    this.errorMessage = '';
    this.deleteUserSubscription = this.us.deleteUser(this.userId).pipe(
      finalize(() => {
        this.common.cleanupSubscription(this.deleteUserSubscription);
      }))
      .subscribe(
          (imResponse: any) => {
            this.closeModal('refresh');
            if (this.config.isDev) {
              console.log(`DeleteUserModalComponent.deleteUser Completed`);
            }
          },
        (error: HttpErrorResponse) => {
            this.errorMessage = 'Something wrong happened. Please, contact the Administrator.';
            this.deleteUserInProgress = false;
            if (this.config.isDev) {
              console.log(`DeleteUserModalComponent.deleteUser Failed`);
            }
          }
      );
  }

  closeModal(refresh?: string) {
    if (refresh) {
      this.activeModal.close('refresh');
    } else {
      this.activeModal.close();
    }
  }

}
