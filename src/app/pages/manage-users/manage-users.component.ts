import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { CommonFunctions } from 'src/app/common/components/common.component';
import { UserObject, Users } from 'src/app/common/interfaces/user.interface';
import { ConfigurationService } from 'src/app/core/services/configuration.service';
import { UserAccountsService } from 'src/app/core/services/user-accounts.service';
import { UserService } from 'src/app/core/services/user.service';
import { AddUserModalComponent } from './add-user-modal/add-user-modal.component';
import { ChangeUserPasswordByAdminModalComponent } from './change-user-password-by-admin-modal/change-user-password-by-admin-modal.component';
import { DeleteUserModalComponent } from './delete-user-modal/delete-user-modal.component';
import { EditUserModalComponent } from './edit-user-modal/edit-user-modal.component';

@Component({
  selector: 'app-manage-users',
  templateUrl: './manage-users.component.html',
  styleUrls: ['./manage-users.component.scss']
})
export class ManageUsersComponent implements OnInit, OnDestroy {

  getUsersInProgress: boolean;
  getUsersSubscription: Subscription;
  changeUserStatusSubscription: Subscription;
  users: UserObject[];
  usersReserve: UserObject[];
  private unsubscribe: Subscription[] = [];
  sortedBy: string;
  sortOrder: string;
  numberOfSeats: number;
  filterForm: FormGroup;
  filterApplied: boolean;

  constructor(private ws: UserAccountsService,
              public common: CommonFunctions,
              private user: UserService,
              private config: ConfigurationService,
              private formBuilder: FormBuilder,
              private modalService: NgbModal) {
    this.getUsersInProgress = false;
    this.getUsersSubscription = null;
    this.changeUserStatusSubscription = null;
    this.users = [];
    this.usersReserve = [];
    this.sortedBy = '';
    this.sortOrder = '';
    this.numberOfSeats = this.user.numberofseats;
    this.filterApplied = false;
  }

  ngOnInit(): void {
    this.getUsers();
    this.buildFilterForm();
  }

  ngOnDestroy() {
    this.unsubscribe.forEach((sb) => sb.unsubscribe());
  }

  buildFilterForm() {
    this.filterForm = this.formBuilder.group({
      email: [''],
      name: [''],
      surname: ['']
    });
  }

  getUsers() {
    this.getUsersInProgress = true;
    // If the entered user is a manager - get only his users
    let api = this.ws.getUsers();
    if (this.common.isManager()) {
      api = this.ws.getManagerUsers(this.user.userid);
    }
    this.getUsersSubscription = api.pipe(
      finalize(() => {
        this.common.cleanupSubscription(this.getUsersSubscription);
      }))
      .subscribe(
        (imResponse: Users) => {
          this.getUsersInProgress = false;
          this.users = imResponse.users;
          this.usersReserve = imResponse.users;
          if (this.config.isDev) {
            console.log(`ManageUsersComponent.getUsers Completed`);
          }
        },
        (error: HttpErrorResponse) => {
          if (this.config.isDev) {
            console.log(`ManageUsersComponent.getUsers Failed`);
          }
        }
    );
    this.unsubscribe.push(this.getUsersSubscription);
  }

  changeStatus(user: UserObject, $event: any) {
    const value = $event.target.checked;
    const payload = new URLSearchParams();
    const activeValue = value === true ? '1' : 'false';
    payload.append('active', activeValue.toString());

    const userId = user.id;

    this.changeUserStatusSubscription = this.ws.changeUserStatus(userId, payload).pipe(
      finalize(() => {
        this.common.cleanupSubscription(this.changeUserStatusSubscription);
      }))
      .subscribe(
        (imResponse: any) => {
          if (this.config.isDev) {
            console.log(`ManageUsersComponent.changeStatus Completed`);
          }
        },
        (error: HttpErrorResponse) => {
          if (this.config.isDev) {
            console.log(`ManageUsersComponent.changeStatus Failed`);
          }
        }
    );
    this.unsubscribe.push(this.changeUserStatusSubscription);
  }

  editUser(user: UserObject) {
    const modalRef = this.modalService.open(EditUserModalComponent);
    modalRef.componentInstance.user = user;

    modalRef.result.then((res) => {
      if (res === 'refresh') {
        this.getUsers();
      }
    });
  }

  changePassword(user: UserObject) {
    const modalRef = this.modalService.open(ChangeUserPasswordByAdminModalComponent);
    modalRef.componentInstance.userId = user.id;

    modalRef.result.then((res) => {

    });
  }

  deleteUser(user: UserObject) {
    const modalRef = this.modalService.open(DeleteUserModalComponent);
    modalRef.componentInstance.userId = user.id;

    modalRef.result.then((res) => {
      if (res === 'refresh') {
        this.getUsers();
      }
    });
  }

  openAddNewUserModal() {
    const modalRef = this.modalService.open(AddUserModalComponent);

    modalRef.result.then((res) => {
      if (res === 'refresh') {
        this.getUsers();
      }
    });
  }

  // Used to identify the current sort order in the UI
  returnSortOrderArrow(sortColumnIdentifier: string) {
    if (sortColumnIdentifier === this.sortedBy) {
      if (this.sortOrder === 'asc') {
        return 'fa-sort-up';
      } else if (this.sortOrder === 'desc') {
        return 'fa-sort-down';
      }
    } else {
      return 'fa-sort';
    }
  }

  sortOrderChange($event) {
    this.sortOrder = $event;
  }

  sortByChange($event) {
    this.sortedBy = $event;
  }

  performFiltering() {
    this.filterApplied = true;
    const controls = this.filterForm.controls;
    const criteria = {};

    if (controls.email.value) {
      if (criteria.hasOwnProperty('email')) {
        // tslint:disable-next-line:no-string-literal
        criteria['email'].push(controls.email.value);
      } else {
        // tslint:disable-next-line:no-string-literal
        criteria['email'] = [];
        // tslint:disable-next-line:no-string-literal
        criteria['email'].push(controls.email.value);
      }
    }

    if (controls.name.value) {
      if (criteria.hasOwnProperty('first_name')) {
        // tslint:disable-next-line:no-string-literal
        criteria['first_name'].push(controls.name.value);
      } else {
        // tslint:disable-next-line:no-string-literal
        criteria['first_name'] = [];
        // tslint:disable-next-line:no-string-literal
        criteria['first_name'].push(controls.name.value);
      }
    }

    if (controls.surname.value) {
      if (criteria.hasOwnProperty('last_name')) {
        // tslint:disable-next-line:no-string-literal
        criteria['last_name'].push(controls.surname.value);
      } else {
        // tslint:disable-next-line:no-string-literal
        criteria['last_name'] = [];
        // tslint:disable-next-line:no-string-literal
        criteria['last_name'].push(controls.surname.value);
      }
    }

    this.users = this.findInObject(this.usersReserve, criteria);
  }

  resetFiltering() {
    this.filterApplied = false;
    this.users = this.usersReserve;
  }

  // Used to search in a JSON using multiple property values
  findInObject(array: UserObject[], criteria: object) {
    return array.filter((obj) => {
      return Object.keys(criteria).every((key) => {
        return (Array.isArray(criteria[key]) &&
          (criteria[key].some((criterion) => {
            return (obj[key].toString().toLowerCase().indexOf(criterion.toLowerCase()) > -1);
          })) || criteria[key].length === 0);
      });
    });
  }

}
