import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WidgetsBlockerComponent } from './widgets-blocker.component';

describe('WidgetsBlockerComponent', () => {
  let component: WidgetsBlockerComponent;
  let fixture: ComponentFixture<WidgetsBlockerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WidgetsBlockerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetsBlockerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
