import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/core/services/user.service';

@Component({
  selector: 'app-widgets-blocker',
  templateUrl: './widgets-blocker.component.html',
  styleUrls: ['./widgets-blocker.component.scss']
})
export class WidgetsBlockerComponent implements OnInit {

  isDemoMode: boolean;

  constructor(private user: UserService) {
    this.isDemoMode = this.user.isDemoMode;
  }

  ngOnInit(): void {
  }

}
