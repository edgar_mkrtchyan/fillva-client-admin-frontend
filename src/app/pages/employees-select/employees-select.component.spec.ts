import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeesSelectComponent } from './employees-select.component';

describe('EmployeesSelectComponent', () => {
  let component: EmployeesSelectComponent;
  let fixture: ComponentFixture<EmployeesSelectComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EmployeesSelectComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeesSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
