import { Component, OnInit } from '@angular/core';
import { Constants } from 'src/app/core/services/constants.service';

@Component({
  selector: 'app-employee-finance',
  templateUrl: './employee-finance.component.html',
  styleUrls: ['./employee-finance.component.scss']
})
export class EmployeeFinanceComponent implements OnInit {

  currentYear: number;
  currentMonth: number;
  currentMonthDisplayName: string;

  constructor(private constants: Constants) {
    const date = new Date();
    this.currentYear = date.getFullYear();
    this.currentMonth = date.getMonth();
    this.currentMonthDisplayName = this.constants.MONTHS_MAP[this.currentMonth];
  }

  ngOnInit(): void {}

  monthSelector(direction: string) {
    if (direction === 'next') {
      this.currentMonth++;
      if (this.currentMonth > 11) {
        this.currentYear++;
        this.currentMonth = 0;
      }
      this.currentMonthDisplayName = this.constants.MONTHS_MAP[this.currentMonth];
    } else {
      this.currentMonth--;
      if (this.currentMonth < 0) {
        this.currentMonth = 11;
        this.currentYear--;
      }
      this.currentMonthDisplayName = this.constants.MONTHS_MAP[this.currentMonth];
    }
  }

}
