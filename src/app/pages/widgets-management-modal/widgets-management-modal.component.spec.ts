import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WidgetsManagementModalComponent } from './widgets-management-modal.component';

describe('WidgetsManagementModalComponent', () => {
  let component: WidgetsManagementModalComponent;
  let fixture: ComponentFixture<WidgetsManagementModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WidgetsManagementModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetsManagementModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
