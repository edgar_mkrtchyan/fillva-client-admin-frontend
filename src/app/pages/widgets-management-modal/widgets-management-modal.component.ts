import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { WidgetsList } from 'src/app/common/interfaces/widgets-list.interface';
import { Constants } from 'src/app/core/services/constants.service';

@Component({
  selector: 'app-widgets-management-modal',
  templateUrl: './widgets-management-modal.component.html',
  styleUrls: ['./widgets-management-modal.component.scss']
})
export class WidgetsManagementModalComponent implements OnInit {

  widgetsList: Array<string>;
  widgetsPerSection: WidgetsList[];

  constructor(public activeModal: NgbActiveModal,
              private router: Router,
              public constants: Constants) {
    const area = this.router.url.indexOf('dashboard') > - 1 ? 'dashboard' : 'insights';
    this.widgetsPerSection = this.constants.WIDGETS_LIST.filter((widget) => widget.area === area);
    this.widgetsList = this.getSavedWidgets() || this.constants.WIDGETS_LIST.map((key) => {
      return key.key;
    });
  }

  ngOnInit(): void {
  }

  closeModal(refresh?: string) {
    if (refresh) {
      this.activeModal.close('refresh');
    } else {
      this.activeModal.close();
    }
  }

  save() {
    localStorage.setItem('selected_widgets', JSON.stringify(this.widgetsList));
    location.reload();
  }

  toggleWidgetInList($event: any, key: string) {
    const checked = $event.target.checked;
    if (!checked) {
      const index = this.widgetsList.indexOf(key);
      if (index > -1) {
        this.widgetsList.splice(index, 1);
      }
    } else {
      this.widgetsList.push(key);
    }
  }

  selectedToShow(key: string) {
    const selectedWidgets = localStorage.getItem('selected_widgets');
    if (selectedWidgets) {
      return JSON.parse(selectedWidgets).indexOf(key) > -1;
    } else {
      return true;
    }
  }

  getSavedWidgets() {
    return JSON.parse(localStorage.getItem('selected_widgets'));
  }

}
