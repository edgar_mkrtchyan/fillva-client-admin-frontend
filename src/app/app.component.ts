import {
  Component,
  ChangeDetectionStrategy,
  OnDestroy,
  OnInit,
  Inject,
} from '@angular/core';
import { TranslationService } from './modules/i18n/translation.service';
// language list
import { locale as enLang } from './modules/i18n/vocabs/en';
import { locale as chLang } from './modules/i18n/vocabs/ch';
import { locale as esLang } from './modules/i18n/vocabs/es';
import { locale as jpLang } from './modules/i18n/vocabs/jp';
import { locale as deLang } from './modules/i18n/vocabs/de';
import { locale as frLang } from './modules/i18n/vocabs/fr';
import { SplashScreenService } from './_fillva/partials/layout/splash-screen/splash-screen.service';
import { Router, NavigationEnd, NavigationError } from '@angular/router';
import { Subscription } from 'rxjs';
import { TableExtendedService } from './_fillva/shared/crud-table';
import { DOCUMENT } from '@angular/common';
import { Constants } from './core/services/constants.service';
import { CommonFunctions } from './common/components/common.component';
import { Title } from '@angular/platform-browser';
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'body[root]',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  changeDetection: ChangeDetectionStrategy.Default,
})
export class AppComponent implements OnInit, OnDestroy {
  private unsubscribe: Subscription[] = []; // Read more: => https://brianflove.com/2016/12/11/anguar-2-unsubscribe-observables/

  constructor(
    private translationService: TranslationService,
    private splashScreenService: SplashScreenService,
    private router: Router,
    private constants: Constants,
    private common: CommonFunctions,
    private titleService: Title,
    @Inject(DOCUMENT) private document: HTMLDocument,
    private tableService: TableExtendedService
  ) {
    // register translations
    this.translationService.loadTranslations(
      enLang,
      chLang,
      esLang,
      jpLang,
      deLang,
      frLang
    );
  }

  ngOnInit() {
    const routerSubscription = this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        // clear filtration paginations and others
        this.tableService.setDefaults();
        // hide splash screen
        this.splashScreenService.hide();

        // scroll to top on every route change
        window.scrollTo(0, 0);

        // to display back the body content
        setTimeout(() => {
          document.body.classList.add('page-loaded');
        }, 500);
      }
    });
    this.unsubscribe.push(routerSubscription);
    this.setFaviconAndTitle();
  }

  ngOnDestroy() {
    this.unsubscribe.forEach((sb) => sb.unsubscribe());
  }

  setFaviconAndTitle() {
    const url = this.common.getUrl();
    let faviconUrl = '';
    let pageTitle = '';
    if (url.indexOf('autods') > -1) {
      faviconUrl = this.constants.WHITE_LABEL['autods']['favicon'];
      pageTitle = this.constants.WHITE_LABEL['autods']['name'];
    } else if (url.indexOf('triplemars') > -1) {
      faviconUrl = this.constants.WHITE_LABEL['triplemars']['favicon'];
      pageTitle = this.constants.WHITE_LABEL['triplemars']['name'];
    } else if (url.indexOf('academie') > -1) {
      faviconUrl = this.constants.WHITE_LABEL['academie']['favicon'];
      pageTitle = this.constants.WHITE_LABEL['academie']['name'];
    } else {
      faviconUrl = this.constants.WHITE_LABEL['fillva']['favicon'];
      pageTitle = this.constants.WHITE_LABEL['fillva']['name'];
    }
    this.document.getElementById('appFavicon').setAttribute('href', faviconUrl);
    this.titleService.setTitle(pageTitle);
  }
}
