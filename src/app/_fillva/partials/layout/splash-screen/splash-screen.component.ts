import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { CommonFunctions } from 'src/app/common/components/common.component';
import { Constants } from 'src/app/core/services/constants.service';
import { SplashScreenService } from './splash-screen.service';

@Component({
  selector: 'app-splash-screen',
  templateUrl: './splash-screen.component.html',
  styleUrls: ['./splash-screen.component.scss'],
})
export class SplashScreenComponent implements OnInit {
  @ViewChild('splashScreen', { static: true }) splashScreen: ElementRef;

  constructor(
    private el: ElementRef,
    private common: CommonFunctions,
    private constants: Constants,
    private splashScreenService: SplashScreenService
  ) {}

  ngOnInit(): void {
    this.splashScreenService.init(this.splashScreen);
  }

  logoUrl() {
    const url = this.common.getUrl();
    if (url.indexOf('autods') > -1) {
      return this.constants.WHITE_LABEL['autods']['splash_logo'];
    } else if (url.indexOf('triplemars') > -1) {
      return this.constants.WHITE_LABEL['triplemars']['splash_logo'];
    } else if (url.indexOf('academie') > -1) {
      return this.constants.WHITE_LABEL['academie']['splash_logo'];
    } else {
      return this.constants.WHITE_LABEL['fillva']['splash_logo'];
    }
  }
}
