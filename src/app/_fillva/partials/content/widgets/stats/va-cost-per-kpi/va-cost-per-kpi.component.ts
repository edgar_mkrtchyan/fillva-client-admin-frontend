import { Component, OnInit } from '@angular/core';
import { Constants } from 'src/app/core/services/constants.service';

declare let ApexCharts: any;

@Component({
  selector: 'app-va-cost-per-kpi',
  templateUrl: './va-cost-per-kpi.component.html',
  styleUrls: ['./va-cost-per-kpi.component.scss']
})
export class VaCostPerKpiComponent implements OnInit {

  constructor(private constants: Constants) { }

  ngOnInit(): void {
    this.initCostPerKPIChart();
  }

  initCostPerKPIChart() {
    const apexChart = '#costPerKPIChart';
    const options = {
      series: [4125, 2125, 1125, 125],
      chart: {
        width: 300,
        type: 'donut',
      },
      legend: false,
      labels: ['session_start', 'session_end', 'message_sent', 'form_sent'],
      responsive: [{
        breakpoint: 480,
        options: {
          chart: {
            width: 200
          },
          legend: {
            position: 'bottom'
          }
        }
      }],
      colors: [this.constants.CHART_COLORS.primary,
      this.constants.CHART_COLORS.success,
      this.constants.CHART_COLORS.warning,
      this.constants.CHART_COLORS.danger,
      this.constants.CHART_COLORS.info]
    };

    const chart = new ApexCharts(document.querySelector(apexChart), options);
    chart.render();
  }

}
