import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VaCostPerKpiComponent } from './va-cost-per-kpi.component';

describe('VaCostPerKpiComponent', () => {
  let component: VaCostPerKpiComponent;
  let fixture: ComponentFixture<VaCostPerKpiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VaCostPerKpiComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VaCostPerKpiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
