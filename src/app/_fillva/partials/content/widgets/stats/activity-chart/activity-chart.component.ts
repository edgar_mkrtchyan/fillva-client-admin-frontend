import { Component, OnInit } from '@angular/core';
import { Constants } from 'src/app/core/services/constants.service';

declare let ApexCharts: any;

@Component({
  selector: 'app-activity-chart',
  templateUrl: './activity-chart.component.html',
  styleUrls: ['./activity-chart.component.scss']
})
export class ActivityChartComponent implements OnInit {

  constructor(private constants: Constants) { }

  ngOnInit(): void {
    this.initActivityChart();
  }

  initActivityChart() {
    const apexChart = '#chart_2';
    const options = {
      series: [{
        name: 'Team Activity Average per Hour',
        data: [12, 20, 28, 32, 60, 32, 14]
      }],
      chart: {
        height: 350,
        type: 'area'
      },
      dataLabels: {
        enabled: false
      },
      stroke: {
        curve: 'smooth'
      },
      xaxis: {
        type: 'datetime',
        categories: ['2018-09-19T00:00:00.000Z', '2018-09-19T01:30:00.000Z', '2018-09-19T02:30:00.000Z', '2018-09-19T03:30:00.000Z', '2018-09-19T04:30:00.000Z', '2018-09-19T05:30:00.000Z', '2018-09-19T06:30:00.000Z']
      },
      tooltip: {
        x: {
          format: 'dd/MM/yy HH:mm'
        },
      },
      colors: [this.constants.CHART_COLORS.primary, this.constants.CHART_COLORS.success]
    };

    const chart = new ApexCharts(document.querySelector(apexChart), options);
    chart.render();
  }
}
