import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CostPerTeamComponent } from './cost-per-team.component';

describe('CostPerTeamComponent', () => {
  let component: CostPerTeamComponent;
  let fixture: ComponentFixture<CostPerTeamComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CostPerTeamComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CostPerTeamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
