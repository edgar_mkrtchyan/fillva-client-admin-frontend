import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VaActivityComponent } from './va-activity.component';

describe('VaActivityComponent', () => {
  let component: VaActivityComponent;
  let fixture: ComponentFixture<VaActivityComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VaActivityComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VaActivityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
