import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnChanges, OnDestroy, OnInit, SimpleChange, SimpleChanges } from '@angular/core';
import { Subscription } from 'rxjs';
import { CommonFunctions } from 'src/app/common/components/common.component';
import { VAProductivityActivity, VAProductivityActivityObject } from 'src/app/common/interfaces/va-activity-productivity.interface';
import { ConfigurationService } from 'src/app/core/services/configuration.service';
import { Constants } from 'src/app/core/services/constants.service';
import { DashboardService } from 'src/app/core/services/dashboard.service';
import { UserAccountsService } from 'src/app/core/services/user-accounts.service';
import { UserService } from 'src/app/core/services/user.service';
import { VADashboardService } from 'src/app/core/services/va-dashboard.service';

declare let ApexCharts: any;

@Component({
  selector: 'app-va-activity',
  templateUrl: './va-activity.component.html',
  styleUrls: ['./va-activity.component.scss']
})
export class VaActivityComponent implements OnInit, OnDestroy, OnChanges {

  @Input() startDate: string;
  @Input() endDate: string;
  getDataInProgress: boolean;
  getDataSubscription: Subscription;
  data: VAProductivityActivityObject[];
  private unsubscribe: Subscription[] = [];
  errorMessage: string;
  chart: any;
  isError: boolean;

  constructor(private constants: Constants,
              private config: ConfigurationService,
              private common: CommonFunctions,
              private user: UserService,
              private ws: VADashboardService) {
    this.getDataInProgress = false;
    this.getDataSubscription = null;
    this.errorMessage = '';
    this.chart = null;
    this.isError = false;
    this.data = [];
  }

  ngOnInit(): void {

  }

  ngOnDestroy(): void {
    this.unsubscribe.forEach((cb) => cb.unsubscribe());
  }

  initVaActivityGraph(dates, activity, productivity) {
    const apexChart = '#vaActivityGraph';
    const options = {
      series: [{
        name: 'Activity',
        data: activity
      },
      {
        name: 'Productivity',
        data: productivity
      }],
      chart: {
        height: 350,
        type: 'area',
        toolbar: {
          show: false
        }
      },
      dataLabels: {
        enabled: false
      },
      stroke: {
        curve: 'smooth'
      },
      xaxis: {
        type: 'datetime',
        categories: dates
      },
      yaxis: {
        floating: false
      },
      tooltip: {
        x: {
          format: 'dd/MM/yy HH:mm'
        },
      },
      colors: [this.constants.CHART_COLORS.primary, this.constants.CHART_COLORS.success, this.constants.CHART_COLORS.warning]
    };

    const chart = new ApexCharts(document.querySelector(apexChart), options);
    chart.render();
  }

  getData() {
    const payload = new URLSearchParams();
    payload.append('startTime', this.startDate);
    payload.append('endTime', this.endDate);
    this.getDataInProgress = true;
    this.isError = false;
    this.getDataSubscription = this.ws.getVAProductivityActivity(payload, this.user.userid).pipe()
      .subscribe(
        (imResponse: VAProductivityActivity) => {
          this.data = imResponse.data;
          if (this.data.length > 0) {
            const dates = this.data.map((datum) => {
              return datum.date;
            });
            const activity = this.data.map((datum) => {
              return Math.floor(datum.activity).toFixed(0);
            });
            const productivity = this.data.map((datum) => {
              return Math.floor(datum.productivity).toFixed(0);
            });
            this.initVaActivityGraph(dates, activity, productivity);
          } else {
            this.errorMessage = 'No Data';
          }
          this.getDataInProgress = false;
          if (this.config.isDev) {
            console.log(`RecentKpiHitStatsComponent.getRecentKPIHit Completed`);
          }
        },
        (error: HttpErrorResponse) => {
          this.getDataInProgress = false;
          this.isError = true;
          if (this.config.isDev) {
            console.log(`RecentKpiHitStatsComponent.getRecentKPIHit Failed`);
          }
        }
      );
    this.unsubscribe.push(this.getDataSubscription);
  }

  ngOnChanges(changes: SimpleChanges) {
    const startDate: SimpleChange = changes.startDate;
    const endDate: SimpleChange = changes.endDate;
    if (startDate) {
      this.startDate = startDate.currentValue;
    }
    if (endDate) {
      this.endDate = endDate.currentValue;
    }
    this.getData();
  }

}
