import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KpisSelectModalComponent } from './kpis-select-modal.component';

describe('KpisSelectModalComponent', () => {
  let component: KpisSelectModalComponent;
  let fixture: ComponentFixture<KpisSelectModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ KpisSelectModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(KpisSelectModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
