import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnChanges, OnDestroy, OnInit, SimpleChange, SimpleChanges } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { KPISCount, KPISCountObject } from 'src/app/common/interfaces/kpis-count.interface';
import { BusinessInsightsService } from 'src/app/core/services/business-insights.service';
import { ConfigurationService } from 'src/app/core/services/configuration.service';
import { Constants } from 'src/app/core/services/constants.service';
import { KpisSelectModalComponent } from './kpis-select-modal/kpis-select-modal.component';

declare let ApexCharts: any;

@Component({
  selector: 'app-kpi-count',
  templateUrl: './kpi-count.component.html',
  styleUrls: ['./kpi-count.component.scss']
})
export class KpiCountComponent implements OnInit, OnDestroy, OnChanges {

  @Input() startDate: string;
  @Input() endDate: string;
  @Input() employeeId: any;
  getDataInProgress: boolean;
  getDataSubscription: Subscription;
  private unsubscribe: Subscription[] = [];
  data: KPISCountObject;
  isError: boolean;
  chart: any;
  selectedKPIS: Array<string>;

  constructor(private ws: BusinessInsightsService,
              private constants: Constants,
              private modalService: NgbModal,
              private config: ConfigurationService) {
    this.data = null;
    this.isError = false;
    this.chart = null;
    this.selectedKPIS = [];
  }

  ngOnInit(): void {

  }

  ngOnDestroy(): void {
    this.unsubscribe.forEach((cb) => cb.unsubscribe());
  }

  initColumnChart(seriesData, dates) {
    const that = this;
    if (that.chart) {
      that.chart.destroy();
    }
    const apexChart = '#kpisCountChart';
    const options = {
      series: seriesData,
      chart: {
        type: 'bar',
        height: 350,
        toolbar: {
          show: false
        }
      },
      plotOptions: {
        bar: {
          horizontal: false,
          columnWidth: '55%',
          endingShape: 'rounded'
        },
      },
      dataLabels: {
        enabled: false
      },
      stroke: {
        show: true,
        width: 2,
        colors: ['transparent']
      },
      xaxis: {
        categories: dates,
      },
      yaxis: {
        title: {
          text: 'count'
        }
      },
      fill: {
        opacity: 1
      },
      tooltip: {
        y: {
          formatter: (val) => {
            return val;
          }
        }
      },
      colors: [this.constants.CHART_COLORS.primary, this.constants.CHART_COLORS.success, this.constants.CHART_COLORS.warning]
    };

    that.chart = new ApexCharts(document.querySelector(apexChart), options);
    that.chart.render();
  }

  getData() {
    this.isError = false;
    this.getDataInProgress = true;
    const payload = new URLSearchParams();
    payload.append('startTime', this.startDate);
    payload.append('endTime', this.endDate);
    if (this.employeeId !== 1) {
      payload.append('userId', this.employeeId);
    }
    if (this.selectedKPIS.length === 0) {
      ['start_shift', 'stop_shift'].forEach(kpi => {
        payload.append('kpis[]', kpi);
      });
    } else {
      this.selectedKPIS.forEach(kpi => {
        payload.append('kpis[]', kpi);
      });
    }
    this.getDataSubscription = this.ws.getKPISCount(payload).pipe()
      .subscribe(
        (imResponse: KPISCount) => {
          this.getDataInProgress = false;
          this.data = imResponse.data;
          if (this.data.dates.length > 0) {
            const dates = this.data.dates.map((datum) => {
              return datum.split(' ')[0];
            });
            this.initColumnChart(this.data.kpis, dates);
          }
          if (this.config.isDev) {
            console.log(`TopKpiStatsComponent.getData Completed`);
          }
        },
        (error: HttpErrorResponse) => {
          this.getDataInProgress = false;
          this.isError = true;
          if (this.config.isDev) {
            console.log(`TopKpiStatsComponent.getData Failed`);
          }
        }
      );
    this.unsubscribe.push(this.getDataSubscription);
  }

  ngOnChanges(changes: SimpleChanges) {
    const employeeId: SimpleChange = changes.employeeId;
    const startDate: SimpleChange = changes.startDate;
    const endDate: SimpleChange = changes.endDate;
    if (employeeId && employeeId.currentValue) {
      this.employeeId = employeeId.currentValue;
      this.getData();
    }
    if (startDate || endDate) {
      if (startDate) {
        this.startDate = startDate.currentValue;
      }
      if (endDate) {
        this.endDate = endDate.currentValue;
      }
      if (this.employeeId) {
        this.getData();
      }
    }
  }

  openKPISelectModal() {
    const modalRef = this.modalService.open(KpisSelectModalComponent, { size: 'lg' });

    modalRef.result.then((res) => {
      if (res.action && res.action === 'refresh') {
        this.selectedKPIS = res.data;
        this.getData();
      }
    });
  }

}
