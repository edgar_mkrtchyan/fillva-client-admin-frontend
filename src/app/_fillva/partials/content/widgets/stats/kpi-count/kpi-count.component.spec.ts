import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KpiCountComponent } from './kpi-count.component';

describe('KpiCountComponent', () => {
  let component: KpiCountComponent;
  let fixture: ComponentFixture<KpiCountComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ KpiCountComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(KpiCountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
