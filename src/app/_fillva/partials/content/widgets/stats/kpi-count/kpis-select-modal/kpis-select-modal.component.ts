import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-kpis-select-modal',
  templateUrl: './kpis-select-modal.component.html',
  styleUrls: ['./kpis-select-modal.component.scss']
})
export class KpisSelectModalComponent implements OnInit {

  kpisList: Array<string>;
  selectedKPIs: Array<string>;

  constructor(public activeModal: NgbActiveModal) {
    this.kpisList = [
      'stop_shift', 'start_shift', 'list item', 'leave feedback', 'Saved', 'Request Order Cancellation',
      'Place Order', 'Update listing', 'Add tracking', 'Save', 'Start chat', 'Sign in', '0Unread messages', '1Unread messages',
      '2Unread messages', '3Unread messages', '4Unread messages', '5Unread messages', 'Messages', 'Send offers', 'שלם עכשיו',
      'Pay Now', 'Chat with us', 'Back to Homepage', 'Try again', 'Check my order',
      'Send message and go to next', 'Send message', 'Send', 'Submit', 'system'
    ];
    this.selectedKPIs = [];
  }

  ngOnInit(): void {
  }

  toggleWidgetInList($event: any, key: string) {
    const checked = $event.target.checked;
    if (!checked) {
      const index = this.selectedKPIs.indexOf(key);
      if (index > -1) {
        this.selectedKPIs.splice(index, 1);
      }
    } else {
      this.selectedKPIs.push(key);
    }
  }

  selectedToShow(key: string) {
    return this.selectedKPIs.indexOf(key) > -1;
  }

  closeModal(refresh?: string) {
    if (refresh) {
      this.activeModal.close({ action: 'refresh' });
    } else {
      this.activeModal.close();
    }
  }

  save() {
    this.activeModal.close({ action: 'refresh', data: this.selectedKPIs });
  }

}
