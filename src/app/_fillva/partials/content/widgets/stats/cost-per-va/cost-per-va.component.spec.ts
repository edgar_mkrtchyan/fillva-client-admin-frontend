import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CostPerVaComponent } from './cost-per-va.component';

describe('CostPerVaComponent', () => {
  let component: CostPerVaComponent;
  let fixture: ComponentFixture<CostPerVaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CostPerVaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CostPerVaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
