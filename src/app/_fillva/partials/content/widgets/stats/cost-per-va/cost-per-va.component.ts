import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnChanges, OnDestroy, OnInit, SimpleChange, SimpleChanges } from '@angular/core';
import { Subscription } from 'rxjs';
import { CostPerEmployee, CostPerEmployeeObject } from 'src/app/common/interfaces/cost-per-employee.interface';
import { ConfigurationService } from 'src/app/core/services/configuration.service';
import { Constants } from 'src/app/core/services/constants.service';
import { FinanceService } from 'src/app/core/services/finance.service';

declare let ApexCharts: any;

@Component({
  selector: 'app-cost-per-va',
  templateUrl: './cost-per-va.component.html',
  styleUrls: ['./cost-per-va.component.scss']
})
export class CostPerVaComponent implements OnInit, OnDestroy, OnChanges {

  @Input() startDate: string;
  @Input() endDate: string;
  getDataInProgress: boolean;
  getDataSubscription: Subscription;
  private unsubscribe: Subscription[] = [];
  data: CostPerEmployeeObject[];
  isError: boolean;
  chart: any;
  noData: boolean;

  constructor(private constants: Constants,
              private config: ConfigurationService,
              private ws: FinanceService) {
    this.data = [];
    this.isError = false;
    this.chart = null;
    this.getDataInProgress = false;
    this.noData = false;
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.unsubscribe.forEach((cb) => cb.unsubscribe());
  }

  initCostPerVAChart(seriesData, labelsData) {
    const that = this;
    if (that.chart) {
      that.chart.destroy();
    }
    const apexChart = '#costPerEmployeeChart';
    const options = {
      series: seriesData,
      chart: {
        width: 500,
        type: 'pie',
      },
      labels: labelsData,
      responsive: [{
        breakpoint: 480,
        options: {
          chart: {
            width: 200
          },
          legend: {
            position: 'right'
          }
        }
      }],
      colors: [this.constants.CHART_COLORS.primary,
      this.constants.CHART_COLORS.success,
      this.constants.CHART_COLORS.warning,
      this.constants.CHART_COLORS.danger,
      this.constants.CHART_COLORS.info]
    };

    that.chart = new ApexCharts(document.querySelector(apexChart), options);
    that.chart.render();
  }

  getData() {
    this.isError = false;
    this.getDataInProgress = true;
    this.noData = false;
    this.data = [];
    const payload = new URLSearchParams();
    payload.append('startTime', this.startDate);
    payload.append('endTime', this.endDate);
    this.getDataSubscription = this.ws.getCostPerEmployee(payload).pipe()
      .subscribe(
        (imResponse: CostPerEmployee) => {
          this.data = imResponse.data;
          this.getDataInProgress = false;
          if (this.data.length > 0) {
            const data = this.data.map((datum) => {
              return datum.cost;
            });
            const employees = this.data.map((datum) => {
              return datum.employee;
            });
            const hasValues = data.filter((val) => parseFloat(val) > 0);
            setTimeout(() => {
              if (hasValues.length > 0) {
                this.initCostPerVAChart(data, employees);
              } else {
                this.noData = true;
              }
            }, 99);
          } else {
            this.getDataInProgress = false;
          }
          if (this.config.isDev) {
            console.log(`CostPerTeamComponent.getData Completed`);
          }
        },
        (error: HttpErrorResponse) => {
          this.getDataInProgress = false;
          this.isError = true;
          if (this.config.isDev) {
            console.log(`CostPerTeamComponent.getData Failed`);
          }
        }
      );
    this.unsubscribe.push(this.getDataSubscription);
  }

  ngOnChanges(changes: SimpleChanges) {
    const startDate: SimpleChange = changes.startDate;
    const endDate: SimpleChange = changes.endDate;
    if (startDate || endDate) {
      if (startDate) {
        this.startDate = startDate.currentValue;
      }
      if (endDate) {
        this.endDate = endDate.currentValue;
      }
      this.getData();
    }
  }
}
