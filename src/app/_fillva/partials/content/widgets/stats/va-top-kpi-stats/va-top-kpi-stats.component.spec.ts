import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VaTopKpiStatsComponent } from './va-top-kpi-stats.component';

describe('VaTopKpiStatsComponent', () => {
  let component: VaTopKpiStatsComponent;
  let fixture: ComponentFixture<VaTopKpiStatsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VaTopKpiStatsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VaTopKpiStatsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
