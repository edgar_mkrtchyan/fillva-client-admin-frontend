import { Component, OnInit } from '@angular/core';
import { Constants } from 'src/app/core/services/constants.service';

declare let ApexCharts: any;

@Component({
  selector: 'app-va-top-kpi-stats',
  templateUrl: './va-top-kpi-stats.component.html',
  styleUrls: ['./va-top-kpi-stats.component.scss']
})
export class VaTopKpiStatsComponent implements OnInit {

  constructor(private constants: Constants) { }

  ngOnInit(): void {
    this.initTopKPIChart();
  }

  initTopKPIChart() {
    const apexChart = '#vaTopKPIStats';
    const options = {
      series: [{
        name: 'Cases',
        data: [44, 55, 41, 37, 22, 43, 21]
      }, {
        name: 'Messages',
        data: [53, 32, 33, 52, 13, 43, 32]
      }, {
        name: 'Orders',
        data: [12, 17, 11, 9, 15, 11, 20]
      }, {
        name: 'Item Listing',
        data: [9, 7, 5, 8, 6, 9, 4]
      }, {
        name: 'Market Research',
        data: [25, 12, 19, 32, 25, 24, 10]
      }],
      chart: {
        type: 'bar',
        height: 350,
        stacked: true,
      },
      plotOptions: {
        bar: {
          horizontal: true,
        },
      },
      stroke: {
        width: 1,
        colors: ['#fff']
      },
      title: {
        text: ''
      },
      xaxis: {
        categories: ['05.01.2021', '05.02.2021', '05.03.2021', '05.04.2021', '05.05.2021', '05.06.2021', '05.07.2021'],
        labels: {
          formatter: (val) => {
            return val + 'K';
          }
        }
      },
      yaxis: {
        title: {
          text: undefined
        },
      },
      tooltip: {
        y: {
          formatter: (val) => {
            return val + 'K';
          }
        }
      },
      fill: {
        opacity: 1
      },
      legend: {
        position: 'top',
        horizontalAlign: 'left',
        offsetX: 40
      },
      colors: [
        this.constants.CHART_COLORS.primary,
        this.constants.CHART_COLORS.success,
        this.constants.CHART_COLORS.warning,
        this.constants.CHART_COLORS.danger,
        this.constants.CHART_COLORS.info
      ]
    };

    const chart = new ApexCharts(document.querySelector(apexChart), options);
    chart.render();
  }

}
