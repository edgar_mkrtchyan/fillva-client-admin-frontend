import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnChanges, OnDestroy, OnInit, SimpleChange, SimpleChanges, ViewEncapsulation } from '@angular/core';
import { Subscription } from 'rxjs';
import { CommonFunctions } from 'src/app/common/components/common.component';
import { ActivityObject, KPIsObject, RecentKPIHit } from 'src/app/common/interfaces/recent-kpi-hit.interface';
import { ConfigurationService } from 'src/app/core/services/configuration.service';
import { Constants } from 'src/app/core/services/constants.service';
import { DashboardService } from 'src/app/core/services/dashboard.service';

declare let ApexCharts: any;

@Component({
  selector: 'app-recent-kpi-hit-stats',
  templateUrl: './recent-kpi-hit-stats.component.html',
  styleUrls: ['./recent-kpi-hit-stats.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class RecentKpiHitStatsComponent implements OnInit, OnDestroy, OnChanges {

  @Input() startDate: string;
  @Input() endDate: string;
  getRecentKPIHitInProgress: boolean;
  getRecentKPIHitSubscription: Subscription;
  recentKPIHitData: RecentKPIHit;
  private unsubscribe: Subscription[] = [];
  errorMessage: string;
  chart: any;
  isError: boolean;

  constructor(private constants: Constants,
              private config: ConfigurationService,
              private common: CommonFunctions,
              private ws: DashboardService) {
    this.getRecentKPIHitInProgress = false;
    this.getRecentKPIHitSubscription = null;
    this.errorMessage = '';
    this.chart = null;
    this.isError = false;
  }

  ngOnInit(): void {
    // this.getRecentKPIHit();
  }

  ngOnDestroy(): void {
    this.unsubscribe.forEach((cb) => cb.unsubscribe());
  }

  initRecentKPIHitGraph(activity: ActivityObject[], timeSeries: Array<string>, kpis: KPIsObject[]) {
    const that = this;
    if (that.chart) {
      that.chart.destroy();
    }
    const apexChart = '#recentKPIHitGraph';
    const options = {
      series: activity,
      chart: {
        height: 350,
        type: 'area',
        toolbar: {
          show: false
        }
      },
      dataLabels: {
        enabled: false
      },
      stroke: {
        curve: 'straight'
      },
      xaxis: {
        categories: timeSeries,
        labels: {
          formatter: function (value, timestamp, opts) {
            if (value && value.indexOf(' ') > -1) {
              return that.common.convertChartDateIntoLocalTimeZone(value);
            } else {
              return value;
            }
          }
        }
      },
      yaxis: {
        title: {
          text: 'Activity, %'
        }
      },
      tooltip: {
        x: {
          format: 'dd/MM/yy HH:mm'
        },
        custom: function({ series, seriesIndex, dataPointIndex, w }) {
          if (kpis[dataPointIndex].kpi_name === '') {
            return '';
          } else {
            return '<span class="kpi-name-container">' + kpis[dataPointIndex].kpi_name + '</span>';
          }
        }
      },
      colors: [
        this.constants.CHART_COLORS.primary,
        this.constants.CHART_COLORS.success,
        this.constants.CHART_COLORS.warning
      ]
    };

    that.chart = new ApexCharts(document.querySelector(apexChart), options);
    that.chart.render();
  }

  getRecentKPIHit() {
    const payload = new URLSearchParams();
    payload.append('startTime', this.startDate);
    payload.append('endTime', this.endDate);
    this.getRecentKPIHitInProgress = true;
    this.isError = false;
    this.getRecentKPIHitSubscription = this.ws.getRecentKPIHit(payload).pipe()
      .subscribe(
        (imResponse: RecentKPIHit) => {
          this.recentKPIHitData = imResponse;
          if (this.recentKPIHitData.activity.length > 0) {
            // Counting the averages
            const averages = [];
            let sum = 0;
            for (let index = 0; index < this.recentKPIHitData.time.length; index++) {
              sum = 0;
              this.recentKPIHitData.activity.forEach(activity => {
                sum += activity.data[index];
              });
              averages.push((sum / this.recentKPIHitData.activity.length).toFixed(2));
            }
            // Adding averages to the main data set
            this.recentKPIHitData.activity.push({
              name: 'Average',
              data: averages
            });
            this.initRecentKPIHitGraph(this.recentKPIHitData.activity, this.recentKPIHitData.time, this.recentKPIHitData.kpis);
          } else {
            this.errorMessage = 'No Data';
          }
          this.getRecentKPIHitInProgress = false;
          if (this.config.isDev) {
            console.log(`RecentKpiHitStatsComponent.getRecentKPIHit Completed`);
          }
        },
        (error: HttpErrorResponse) => {
          this.getRecentKPIHitInProgress = false;
          this.isError = true;
          if (this.config.isDev) {
            console.log(`RecentKpiHitStatsComponent.getRecentKPIHit Failed`);
          }
        }
      );
    this.unsubscribe.push(this.getRecentKPIHitSubscription);
  }

  ngOnChanges(changes: SimpleChanges) {
    const startDate: SimpleChange = changes.startDate;
    const endDate: SimpleChange = changes.endDate;
    if (startDate) {
      this.startDate = startDate.currentValue;
    }
    if (endDate) {
      this.endDate = endDate.currentValue;
    }
    this.getRecentKPIHit();
  }

}
