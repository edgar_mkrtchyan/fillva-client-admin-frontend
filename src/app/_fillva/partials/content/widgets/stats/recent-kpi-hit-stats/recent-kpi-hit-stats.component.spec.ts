import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecentKpiHitStatsComponent } from './recent-kpi-hit-stats.component';

describe('RecentKpiHitStatsComponent', () => {
  let component: RecentKpiHitStatsComponent;
  let fixture: ComponentFixture<RecentKpiHitStatsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RecentKpiHitStatsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RecentKpiHitStatsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
