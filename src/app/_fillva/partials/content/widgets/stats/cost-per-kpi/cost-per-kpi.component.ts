import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnChanges, OnDestroy, OnInit, SimpleChange, SimpleChanges } from '@angular/core';
import { Subscription } from 'rxjs';
import { CostPerKPI, CostPerKPIObject } from 'src/app/common/interfaces/cost-per-kpi.interface';
import { BusinessInsightsService } from 'src/app/core/services/business-insights.service';
import { ConfigurationService } from 'src/app/core/services/configuration.service';
import { Constants } from 'src/app/core/services/constants.service';

declare let ApexCharts: any;

@Component({
  selector: 'app-cost-per-kpi',
  templateUrl: './cost-per-kpi.component.html',
  styleUrls: ['./cost-per-kpi.component.scss']
})
export class CostPerKpiComponent implements OnInit, OnDestroy, OnChanges {

  @Input() startDate: string;
  @Input() endDate: string;
  @Input() employeeId: any;
  getDataInProgress: boolean;
  getDataSubscription: Subscription;
  private unsubscribe: Subscription[] = [];
  data: CostPerKPIObject[];
  isError: boolean;
  chart: any;

  constructor(private ws: BusinessInsightsService,
              private constants: Constants,
              private config: ConfigurationService) {
    this.data = [];
    this.isError = false;
    this.chart = null;
  }

  ngOnInit(): void {

  }

  initCostPerKPIChart(seriesData, labelStrings) {
    const that = this;
    if (that.chart) {
      that.chart.destroy();
    }
    const apexChart = '#costPerKPIChart';
    const options = {
      series: seriesData,
      chart: {
        width: 300,
        type: 'donut',
      },
      legend: false,
      labels: labelStrings,
      responsive: [{
        breakpoint: 480,
        options: {
          chart: {
            width: 200
          },
          legend: {
            position: 'bottom'
          }
        }
      }],
      colors: [this.constants.CHART_COLORS.primary,
              this.constants.CHART_COLORS.success,
              this.constants.CHART_COLORS.warning,
              this.constants.CHART_COLORS.danger,
              this.constants.CHART_COLORS.info]
    };

    that.chart = new ApexCharts(document.querySelector(apexChart), options);
    that.chart.render();
  }

  ngOnDestroy(): void {
    this.unsubscribe.forEach((cb) => cb.unsubscribe());
  }

  getData() {
    this.isError = false;
    this.getDataInProgress = true;
    const payload = new URLSearchParams();
    payload.append('startTime', this.startDate);
    payload.append('endTime', this.endDate);
    if (this.employeeId !== 1) {
      payload.append('userId', this.employeeId);
    }
    this.getDataSubscription = this.ws.getCostPerKPI(payload).pipe()
      .subscribe(
        (imResponse: CostPerKPI) => {
          this.getDataInProgress = false;
          this.data = imResponse.data;
          if (this.data.length > 0) {
            const seriesData = this.data.map((datum) => {
              return datum.count;
            });
            const labels = this.data.map((datum) => {
              return datum.kpi_name;
            });
            this.initCostPerKPIChart(seriesData, labels);
          }
          if (this.config.isDev) {
            console.log(`CostPerKpiComponent.getData Completed`);
          }
        },
        (error: HttpErrorResponse) => {
          this.getDataInProgress = false;
          this.isError = true;
          if (this.config.isDev) {
            console.log(`CostPerKpiComponent.getData Failed`);
          }
        }
      );
    this.unsubscribe.push(this.getDataSubscription);
  }

  ngOnChanges(changes: SimpleChanges) {
    const employeeId: SimpleChange = changes.employeeId;
    const startDate: SimpleChange = changes.startDate;
    const endDate: SimpleChange = changes.endDate;
    if (employeeId && employeeId.currentValue) {
      this.employeeId = employeeId.currentValue;
      this.getData();
    }
    if (startDate || endDate) {
      if (startDate) {
        this.startDate = startDate.currentValue;
      }
      if (endDate) {
        this.endDate = endDate.currentValue;
      }
      if (this.employeeId) {
        this.getData();
      }
    }
  }

}
