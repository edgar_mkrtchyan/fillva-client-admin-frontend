import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CostPerKpiComponent } from './cost-per-kpi.component';

describe('CostPerKpiComponent', () => {
  let component: CostPerKpiComponent;
  let fixture: ComponentFixture<CostPerKpiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CostPerKpiComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CostPerKpiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
