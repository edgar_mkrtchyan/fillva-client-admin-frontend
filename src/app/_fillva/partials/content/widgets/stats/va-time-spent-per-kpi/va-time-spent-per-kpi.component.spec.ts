import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VaTimeSpentPerKpiComponent } from './va-time-spent-per-kpi.component';

describe('VaTimeSpentPerKpiComponent', () => {
  let component: VaTimeSpentPerKpiComponent;
  let fixture: ComponentFixture<VaTimeSpentPerKpiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VaTimeSpentPerKpiComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VaTimeSpentPerKpiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
