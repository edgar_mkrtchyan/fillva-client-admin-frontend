import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnChanges, OnDestroy, OnInit, SimpleChange, SimpleChanges } from '@angular/core';
import { Subscription } from 'rxjs';
import { ProductivityChartData, ProductivityChartObject } from 'src/app/common/interfaces/productivity-chart.interface';
import { ProductivityStatisticsObject } from 'src/app/common/interfaces/productivity-statistics.interface';
import { ConfigurationService } from 'src/app/core/services/configuration.service';
import { ProductivityService } from 'src/app/core/services/productivity.service';

declare let AmCharts: any;

@Component({
  selector: 'app-productivity-chart',
  templateUrl: './productivity-chart.component.html',
  styleUrls: ['./productivity-chart.component.scss']
})
export class ProductivityChartComponent implements OnInit, OnDestroy, OnChanges {

  @Input() startDate: string;
  @Input() endDate: string;
  @Input() employeeId: number;
  getDataInProgress: boolean;
  getDataSubscription: Subscription;
  private unsubscribe: Subscription[] = [];
  data: ProductivityChartObject[];

  constructor(private ws: ProductivityService,
              private config: ConfigurationService) {
    this.data = [];
  }

  ngOnInit(): void {

  }

  ngOnDestroy(): void {
    this.unsubscribe.forEach((cb) => cb.unsubscribe());
  }

  getData() {
    const payload = new URLSearchParams();
    payload.append('startTime', this.startDate);
    payload.append('endTime', this.endDate);
    this.getDataInProgress = true;
    this.getDataSubscription = this.ws.getProductivityChartData(payload, this.employeeId).pipe()
      .subscribe(
        (imResponse: ProductivityChartData) => {
          this.getDataInProgress = false;
          this.data = imResponse.data;
          this.generateProductivityChart(this.data);
          if (this.config.isDev) {
            console.log(`RecentKpiHitStatsComponent.getData Completed`);
          }
        },
        (error: HttpErrorResponse) => {
          if (this.config.isDev) {
            console.log(`RecentKpiHitStatsComponent.getData Failed`);
          }
        }
      );
    this.unsubscribe.push(this.getDataSubscription);
  }

  generateProductivityChart(data: ProductivityChartObject[]) {
    const chartData = generateChartData(data);

    function generateChartData(dataSet: ProductivityChartObject[]) {
      const chartDataSet = [];

      // tslint:disable-next-line:prefer-for-of
      for (let i = 0; i < dataSet.length; i++) {
        chartDataSet.push({
          date: dataSet[i].date.split(' ')[0],
          value: dataSet[i].productivity,
          volume: dataSet[i].activity
        });
      }
      return chartDataSet;
    }

    const chart = AmCharts.makeChart('productivityChart', {
      type: 'stock',
      theme: 'light',
      categoryAxesSettings: {
        minPeriod: 'mm'
      },

      dataSets: [{
        color: '#b0de09',
        fieldMappings: [{
          fromField: 'value',
          toField: 'value'
        }, {
          fromField: 'volume',
          toField: 'volume'
        }],

        dataProvider: chartData,
        categoryField: 'date'
      }],

      panels: [{
        showCategoryAxis: false,
        title: 'Productivity',
        percentHeight: 70,

        stockGraphs: [{
          id: 'g1',
          valueField: 'value',
          type: 'smoothedLine',
          lineThickness: 2,
          bullet: 'round'
        }],

        stockLegend: {
          valueTextRegular: ' ',
          markerType: 'none'
        }
      }, {
        title: 'Activity',
        percentHeight: 30,
        stockGraphs: [{
          valueField: 'volume',
          type: 'column',
          cornerRadiusTop: 2,
          fillAlphas: 1
        }],

        stockLegend: {
          valueTextRegular: ' ',
          markerType: 'none'
        }
      }],

      chartScrollbarSettings: {
        enabled: false,
        graph: 'g1',
        usePeriod: '10mm',
        position: 'top',
      },

      chartCursorSettings: {
        valueBalloonsEnabled: true
      },

      panelsSettings: {
        usePrefixes: true
      },

      export: {
        enabled: false,
        position: 'bottom-right'
      }
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    const employeeId: SimpleChange = changes.employeeId;
    const startDate: SimpleChange = changes.startDate;
    const endDate: SimpleChange = changes.endDate;
    if (employeeId && employeeId.currentValue) {
      this.employeeId = employeeId.currentValue;
      this.getData();
    }
    if (startDate || endDate) {
      if (startDate) {
        this.startDate = startDate.currentValue;
      }
      if (endDate) {
        this.endDate = endDate.currentValue;
      }
      if (this.employeeId) {
        this.getData();
      }
    }
  }
}
