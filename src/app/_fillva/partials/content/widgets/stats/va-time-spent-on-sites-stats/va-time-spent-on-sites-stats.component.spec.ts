import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VaTimeSpentOnSitesStatsComponent } from './va-time-spent-on-sites-stats.component';

describe('VaTimeSpentOnSitesStatsComponent', () => {
  let component: VaTimeSpentOnSitesStatsComponent;
  let fixture: ComponentFixture<VaTimeSpentOnSitesStatsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VaTimeSpentOnSitesStatsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VaTimeSpentOnSitesStatsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
