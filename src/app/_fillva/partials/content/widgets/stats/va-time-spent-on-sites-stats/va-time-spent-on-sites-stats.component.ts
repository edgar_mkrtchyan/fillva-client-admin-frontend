import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnChanges, OnDestroy, OnInit, SimpleChange, SimpleChanges } from '@angular/core';
import { Subscription } from 'rxjs';
import { TimeSpentOnSites, TimeSpentOnSitesObject } from 'src/app/common/interfaces/time-spent-on-sites.interface';
import { ConfigurationService } from 'src/app/core/services/configuration.service';
import { Constants } from 'src/app/core/services/constants.service';
import { DashboardService } from 'src/app/core/services/dashboard.service';
import { ProductivityService } from 'src/app/core/services/productivity.service';

declare let ApexCharts: any;

@Component({
  selector: 'app-va-time-spent-on-sites-stats',
  templateUrl: './va-time-spent-on-sites-stats.component.html',
  styleUrls: ['./va-time-spent-on-sites-stats.component.scss']
})
export class VaTimeSpentOnSitesStatsComponent implements OnInit, OnDestroy, OnChanges {

  @Input() startDate: string;
  @Input() endDate: string;
  @Input() employeeId: number;
  getDataInProgress: boolean;
  getDataSubscription: Subscription;
  private unsubscribe: Subscription[] = [];
  data: TimeSpentOnSitesObject[];
  chart: any;
  colors: Array<string> = [
    this.constants.CHART_COLORS.primary,
    this.constants.CHART_COLORS.success,
    this.constants.CHART_COLORS.gray,
    this.constants.CHART_COLORS.warning,
    this.constants.CHART_COLORS.info
  ];
  isError: boolean;

  constructor(private constants: Constants,
              private ws: ProductivityService,
              private config: ConfigurationService) {
    this.data = [];
    this.isError = false;
    this.chart = null;
  }

  ngOnInit(): void {
    // if (this.employeeId) {
    //   this.getData();
    // }
    // this.initTimeSpentChart();
  }

  initTimeSpentChart(websites: Array<string>, data: Array<number>) {
    const that = this;
    if (that.chart) {
      that.chart.destroy();
    }
    const apexChart = '#vaTimeSpentOnWebsites';
    const options = {
      series: data,
      chart: {
        width: 300,
        type: 'pie',
      },
      labels: websites,
      legend: false,
      responsive: [{
        breakpoint: 480,
        options: {
          chart: {
            width: 400
          },
          legend: {
            position: 'top'
          }
        }
      }],
      colors: [
        this.constants.CHART_COLORS.primary,
        this.constants.CHART_COLORS.success,
        this.constants.CHART_COLORS.warning,
        this.constants.CHART_COLORS.gray,
        this.constants.CHART_COLORS.info
      ]
    };

    that.chart = new ApexCharts(document.querySelector(apexChart), options);
    that.chart.render();
  }

  ngOnDestroy(): void {
    this.unsubscribe.forEach((cb) => cb.unsubscribe());
  }

  getData() {
    this.getDataInProgress = true;
    const payload = new URLSearchParams();
    payload.append('startTime', this.startDate);
    payload.append('endTime', this.endDate);
    this.getDataSubscription = this.ws.getTimeSpentOnWebsites(payload, this.employeeId).pipe()
      .subscribe(
        (imResponse: TimeSpentOnSites) => {
          this.getDataInProgress = false;
          this.data = imResponse.data;
          if (this.data.length > 0) {
            const websites = this.data.map((website) => {
              return website.url;
            });
            const data = this.data.map((website) => {
              return website.value;
            });
            // Calculating if there are "Other" sites
            const currentSum = data.reduce((a, b) => a + b, 0);
            const differenceOfOther = 100 - currentSum;
            if (differenceOfOther > 0) {
              websites.push('Other');
              data.push(differenceOfOther);
              this.data.push({
                url: 'Other',
                value: differenceOfOther
              });
            }
            // Removing "Untracked" if it's 0
            // Untracked will be 0 if there are no allowed web-sites set
            const untracked = this.data.filter((website) => website.url.toLowerCase() === 'untracked');
            if (untracked.length > 0 && untracked[0].value === 0) {
              this.data = this.data.filter((website) => website.url.toLowerCase() !== 'untracked');
            }
            this.initTimeSpentChart(websites, data);
          }
          if (this.config.isDev) {
            console.log(`RecentKpiHitStatsComponent.getData Completed`);
          }
        },
        (error: HttpErrorResponse) => {
          this.getDataInProgress = false;
          this.isError = true;
          if (this.config.isDev) {
            console.log(`RecentKpiHitStatsComponent.getData Failed`);
          }
        }
      );
    this.unsubscribe.push(this.getDataSubscription);
  }

  ngOnChanges(changes: SimpleChanges) {
    const employeeId: SimpleChange = changes.employeeId;
    const startDate: SimpleChange = changes.startDate;
    const endDate: SimpleChange = changes.endDate;
    if (employeeId && employeeId.currentValue) {
      this.employeeId = employeeId.currentValue;
      this.getData();
    }
    if (startDate || endDate) {
      if (startDate) {
        this.startDate = startDate.currentValue;
      }
      if (endDate) {
        this.endDate = endDate.currentValue;
      }
      if (this.employeeId) {
        this.getData();
      }
    }
  }

}
