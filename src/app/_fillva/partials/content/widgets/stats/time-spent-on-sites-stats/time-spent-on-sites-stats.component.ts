import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnChanges, OnDestroy, OnInit, SimpleChange, SimpleChanges } from '@angular/core';
import { Subscription } from 'rxjs';
import { TimeInvestedInEachKPI, TimeInvestedInEachKPIObject } from 'src/app/common/interfaces/time-invested-in-each-kpi.interface';
import { BusinessInsightsService } from 'src/app/core/services/business-insights.service';
import { ConfigurationService } from 'src/app/core/services/configuration.service';
import { Constants } from 'src/app/core/services/constants.service';

declare let ApexCharts: any;

@Component({
  selector: 'app-time-spent-on-sites-stats',
  templateUrl: './time-spent-on-sites-stats.component.html',
  styleUrls: ['./time-spent-on-sites-stats.component.scss']
})
export class TimeSpentOnSitesStatsComponent implements OnInit, OnDestroy, OnChanges {

  @Input() startDate: string;
  @Input() endDate: string;
  @Input() employeeId: any;
  getDataInProgress: boolean;
  getDataSubscription: Subscription;
  private unsubscribe: Subscription[] = [];
  data: TimeInvestedInEachKPIObject[];
  isError: boolean;
  chart: any;

  constructor(private ws: BusinessInsightsService,
              private constants: Constants,
              private config: ConfigurationService) {
    this.data = [];
    this.isError = false;
    this.chart = null;
  }

  ngOnInit(): void {

  }

  ngOnDestroy(): void {
    this.unsubscribe.forEach((cb) => cb.unsubscribe());
  }

  initTimeSpentChart(seriesData, labelsTexts) {
    const that = this;
    if (that.chart) {
      that.chart.destroy();
    }
    const apexChart = '#timeInvestedInEachKPIChart';
    const options = {
      series: seriesData,
      chart: {
        width: 300,
        type: 'pie',
      },
      labels: labelsTexts,
      legend: false,
      responsive: [{
        breakpoint: 480,
        options: {
          chart: {
            width: 400
          },
          legend: {
            position: 'top'
          }
        }
      }],
      colors: [
                this.constants.CHART_COLORS.primary,
                this.constants.CHART_COLORS.success,
                this.constants.CHART_COLORS.warning,
                this.constants.CHART_COLORS.gray,
                this.constants.CHART_COLORS.info,
                this.constants.CHART_COLORS.danger,
                this.constants.CHART_COLORS.brand
              ]
    };

    that.chart = new ApexCharts(document.querySelector(apexChart), options);
    that.chart.render();
  }

  getData() {
    this.isError = false;
    this.getDataInProgress = true;
    const payload = new URLSearchParams();
    payload.append('startTime', this.startDate);
    payload.append('endTime', this.endDate);
    if (this.employeeId !== 1) {
      payload.append('userId', this.employeeId);
    }
    this.getDataSubscription = this.ws.getTimeInvestedInEachKPI(payload).pipe()
      .subscribe(
        (imResponse: TimeInvestedInEachKPI) => {
          this.getDataInProgress = false;
          this.data = imResponse.data;
          if (this.data.length > 0) {
            const seriesData = this.data.map((datum) => {
              return parseFloat(datum.value);
            });
            const labels = this.data.map((datum) => {
              return datum.kpi_name;
            });
            this.initTimeSpentChart(seriesData, labels);
          }
          if (this.config.isDev) {
            console.log(`TimeSpentOnSitesStatsComponent.getData Completed`);
          }
        },
        (error: HttpErrorResponse) => {
          this.getDataInProgress = false;
          this.isError = true;
          if (this.config.isDev) {
            console.log(`TimeSpentOnSitesStatsComponent.getData Failed`);
          }
        }
      );
    this.unsubscribe.push(this.getDataSubscription);
  }

  ngOnChanges(changes: SimpleChanges) {
    const employeeId: SimpleChange = changes.employeeId;
    const startDate: SimpleChange = changes.startDate;
    const endDate: SimpleChange = changes.endDate;
    if (employeeId && employeeId.currentValue) {
      this.employeeId = employeeId.currentValue;
      this.getData();
    }
    if (startDate || endDate) {
      if (startDate) {
        this.startDate = startDate.currentValue;
      }
      if (endDate) {
        this.endDate = endDate.currentValue;
      }
      if (this.employeeId) {
        this.getData();
      }
    }
  }

}
