import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TimeSpentOnSitesStatsComponent } from './time-spent-on-sites-stats.component';

describe('TimeSpentOnSitesStatsComponent', () => {
  let component: TimeSpentOnSitesStatsComponent;
  let fixture: ComponentFixture<TimeSpentOnSitesStatsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TimeSpentOnSitesStatsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TimeSpentOnSitesStatsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
