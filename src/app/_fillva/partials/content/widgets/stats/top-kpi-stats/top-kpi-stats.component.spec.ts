import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TopKpiStatsComponent } from './top-kpi-stats.component';

describe('TopKpiStatsComponent', () => {
  let component: TopKpiStatsComponent;
  let fixture: ComponentFixture<TopKpiStatsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TopKpiStatsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TopKpiStatsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
