import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnChanges, OnDestroy, OnInit, SimpleChange, SimpleChanges } from '@angular/core';
import { Subscription } from 'rxjs';
import { TopKPIS, TopKPISObject } from 'src/app/common/interfaces/top-kpis.interface';
import { BusinessInsightsService } from 'src/app/core/services/business-insights.service';
import { ConfigurationService } from 'src/app/core/services/configuration.service';
import { Constants } from 'src/app/core/services/constants.service';

declare let ApexCharts: any;

@Component({
  selector: 'app-top-kpi-stats',
  templateUrl: './top-kpi-stats.component.html',
  styleUrls: ['./top-kpi-stats.component.scss']
})
export class TopKpiStatsComponent implements OnInit, OnDestroy, OnChanges {

  @Input() startDate: string;
  @Input() endDate: string;
  @Input() employeeId: any;
  getDataInProgress: boolean;
  getDataSubscription: Subscription;
  private unsubscribe: Subscription[] = [];
  data: TopKPISObject;
  isError: boolean;
  chart: any;

  constructor(private ws: BusinessInsightsService,
              private constants: Constants,
              private config: ConfigurationService) {
    this.data = null;
    this.isError = false;
    this.chart = null;
  }

  ngOnInit(): void {

  }

  ngOnDestroy(): void {
    this.unsubscribe.forEach((cb) => cb.unsubscribe());
  }

  initTopKPIChart(seriesData, dates) {
    const that = this;
    if (that.chart) {
      that.chart.destroy();
    }
    const apexChart = '#topKPISChart';
    const options = {
      series: seriesData,
      chart: {
        type: 'bar',
        height: 350,
        stacked: true,
        toolbar: {
          show: false
        }
      },
      plotOptions: {
        bar: {
          horizontal: true,
        },
      },
      stroke: {
        width: 1,
        colors: ['#fff']
      },
      title: {
        text: ''
      },
      xaxis: {
        categories: dates,
        labels: {
          formatter: (val) => {
            return val;
          }
        }
      },
      yaxis: {
        title: {
          text: undefined
        },
      },
      tooltip: {
        y: {
          formatter: (val) => {
            return val;
          }
        }
      },
      fill: {
        opacity: 1
      },
      legend: {
        position: 'top',
        horizontalAlign: 'left',
        offsetX: 40
      },
      colors: [
                this.constants.CHART_COLORS.primary,
                this.constants.CHART_COLORS.success,
                this.constants.CHART_COLORS.warning,
                this.constants.CHART_COLORS.danger,
                this.constants.CHART_COLORS.info
              ]
    };

    that.chart = new ApexCharts(document.querySelector(apexChart), options);
    that.chart.render();
  }

  getData() {
    this.isError = false;
    this.getDataInProgress = true;
    const payload = new URLSearchParams();
    payload.append('startTime', this.startDate);
    payload.append('endTime', this.endDate);
    if (this.employeeId !== 1) {
      payload.append('userId', this.employeeId);
    }
    this.getDataSubscription = this.ws.getTopKPIS(payload).pipe()
      .subscribe(
        (imResponse: TopKPIS) => {
          this.getDataInProgress = false;
          this.data = imResponse.data;
          if (this.data.dates.length > 0) {
            const dates = this.data.dates.map((datum) => {
              return datum.split(' ')[0];
            });
            this.initTopKPIChart(this.data.kpis, dates);
          }
          if (this.config.isDev) {
            console.log(`TopKpiStatsComponent.getData Completed`);
          }
        },
        (error: HttpErrorResponse) => {
          this.getDataInProgress = false;
          this.isError = true;
          if (this.config.isDev) {
            console.log(`TopKpiStatsComponent.getData Failed`);
          }
        }
      );
    this.unsubscribe.push(this.getDataSubscription);
  }

  ngOnChanges(changes: SimpleChanges) {
    const employeeId: SimpleChange = changes.employeeId;
    const startDate: SimpleChange = changes.startDate;
    const endDate: SimpleChange = changes.endDate;
    if (employeeId && employeeId.currentValue) {
      this.employeeId = employeeId.currentValue;
      this.getData();
    }
    if (startDate || endDate) {
      if (startDate) {
        this.startDate = startDate.currentValue;
      }
      if (endDate) {
        this.endDate = endDate.currentValue;
      }
      if (this.employeeId) {
        this.getData();
      }
    }
  }

}
