import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit, EventEmitter, Output, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { CommonFunctions } from 'src/app/common/components/common.component';
import { UserObject, Users } from 'src/app/common/interfaces/user.interface';
import { ConfigurationService } from 'src/app/core/services/configuration.service';
import { UserAccountsService } from 'src/app/core/services/user-accounts.service';
import { UserService } from 'src/app/core/services/user.service';

@Component({
  selector: 'app-dashboard-employees-select',
  templateUrl: './dashboard-employees-select.component.html',
  styleUrls: ['./dashboard-employees-select.component.scss']
})
export class DashboardEmployeesSelectComponent implements OnInit, OnDestroy {

  getUsersInProgress: boolean;
  getUsersSubscription: Subscription;
  employees: UserObject[];
  private unsubscribe: Subscription[] = [];
  @Output() emitEmployeeChange: EventEmitter<any> = new EventEmitter<any>();

  constructor(private ws: UserAccountsService,
              private common: CommonFunctions,
              private config: ConfigurationService,
              private user: UserService) {
    this.getUsersInProgress = false;
    this.getUsersSubscription = null;
    this.employees = [];
  }

  ngOnInit(): void {
    this.getEmployees();
  }

  ngOnDestroy(): void {
    this.unsubscribe.forEach((u) => u.unsubscribe());
  }

  getEmployees() {
    this.getUsersInProgress = true;
    this.getUsersSubscription = this.ws.getManagerUsers(this.user.userid).pipe()
      .subscribe(
        (imResponse: Users) => {
          this.getUsersInProgress = false;
          this.employees = imResponse.users;
          if (this.employees.length > 0) {
            this.emitEmployeeChange.emit(1);
          }
          if (this.config.isDev) {
            console.log(`ManageUsersComponent.getUsers Completed`);
          }
        },
        (error: HttpErrorResponse) => {
          if (this.config.isDev) {
            console.log(`ManageUsersComponent.getUsers Failed`);
          }
        }
      );
    this.unsubscribe.push(this.getUsersSubscription);
  }

  changeSelectedEmployee(employee: UserObject) {
    this.emitEmployeeChange.emit(employee);
  }

  changeSelectedEmployeeFromSelect($event) {
    const selectedEmployeeId = $event.target.value;
    if (selectedEmployeeId === '1') {
      this.emitEmployeeChange.emit(1);
    } else {
      const selectedEmployee = this.employees.filter((employee) => employee.id === selectedEmployeeId);
      this.emitEmployeeChange.emit(selectedEmployee[0]);
    }
  }

}
