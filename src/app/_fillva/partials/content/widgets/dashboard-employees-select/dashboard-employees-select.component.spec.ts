import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardEmployeesSelectComponent } from './dashboard-employees-select.component';

describe('DashboardEmployeesSelectComponent', () => {
  let component: DashboardEmployeesSelectComponent;
  let fixture: ComponentFixture<DashboardEmployeesSelectComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DashboardEmployeesSelectComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardEmployeesSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
