import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { CommonFunctions } from 'src/app/common/components/common.component';
import { Alerts, AlertsObject } from 'src/app/common/interfaces/alerts.interface';
import { ConfigurationService } from 'src/app/core/services/configuration.service';
import { Constants } from 'src/app/core/services/constants.service';
import { DashboardService } from 'src/app/core/services/dashboard.service';

@Component({
  selector: 'app-va-alerts-component',
  templateUrl: './va-alerts-component.component.html',
  styleUrls: ['./va-alerts-component.component.scss']
})
export class VaAlertsComponentComponent implements OnInit, OnDestroy {

  getDataInProgress: boolean;
  getDataSubscription: Subscription;
  private unsubscribe: Subscription[] = [];
  data: AlertsObject[];

  constructor(private ws: DashboardService,
              public constants: Constants,
              public common: CommonFunctions,
              private config: ConfigurationService) {
    this.getDataInProgress = false;
    this.getDataSubscription = null;
    this.data = [];
  }

  ngOnInit(): void {
    this.getData();
  }

  ngOnDestroy(): void {
    this.unsubscribe.forEach((cb) => cb.unsubscribe());
  }

  getData() {
    this.getDataInProgress = true;
    this.getDataSubscription = this.ws.getAlertsData().pipe()
      .subscribe(
        (imResponse: Alerts) => {
          this.getDataInProgress = false;
          this.data = imResponse.data;
          if (this.config.isDev) {
            console.log(`RecentKpiHitStatsComponent.getData Completed`);
          }
        },
        (error: HttpErrorResponse) => {
          if (this.config.isDev) {
            console.log(`RecentKpiHitStatsComponent.getData Failed`);
          }
        }
      );
    this.unsubscribe.push(this.getDataSubscription);
  }

  definectActionIcon(action: string) {
    if (action === 'logged_in') {
      return 'green';
    } else if (action === 'logged_out') {
      return 'yellow';
    }
  }

  definectActionLabel(action: string) {
    if (action === 'logged_in') {
      return 'Logged In';
    } else if (action === 'logged_out') {
      return 'Logged Out';
    }
  }

}
