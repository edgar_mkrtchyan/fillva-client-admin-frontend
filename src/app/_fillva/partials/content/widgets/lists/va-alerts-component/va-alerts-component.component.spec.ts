import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VaAlertsComponentComponent } from './va-alerts-component.component';

describe('VaAlertsComponentComponent', () => {
  let component: VaAlertsComponentComponent;
  let fixture: ComponentFixture<VaAlertsComponentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VaAlertsComponentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VaAlertsComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
