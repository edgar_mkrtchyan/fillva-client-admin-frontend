import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnChanges, OnDestroy, OnInit, SimpleChange, SimpleChanges } from '@angular/core';
import { Subscription } from 'rxjs';
import { CommonFunctions } from 'src/app/common/components/common.component';
import { LatestKPIS, LatestKPISObject } from 'src/app/common/interfaces/latest-kpis.interface';
import { ConfigurationService } from 'src/app/core/services/configuration.service';
import { Constants } from 'src/app/core/services/constants.service';
import { DashboardService } from 'src/app/core/services/dashboard.service';
import { ProductivityService } from 'src/app/core/services/productivity.service';

@Component({
  selector: 'app-va-latest-activity',
  templateUrl: './va-latest-activity.component.html',
  styleUrls: ['./va-latest-activity.component.scss']
})
export class VaLatestActivityComponent implements OnInit, OnDestroy, OnChanges {

  @Input() startDate: string;
  @Input() endDate: string;
  @Input() employeeId: number;
  getDataInProgress: boolean;
  getDataSubscription: Subscription;
  private unsubscribe: Subscription[] = [];
  data: LatestKPISObject[];
  colourNames: Array<string>;
  isError: boolean;

  constructor(private ws: ProductivityService,
              public constants: Constants,
              public common: CommonFunctions,
              private config: ConfigurationService) {
    this.getDataInProgress = false;
    this.getDataSubscription = null;
    this.data = [];
    this.colourNames = ['text-warning', 'text-success', 'text-danger', 'text-primary'];
    this.isError = false;
  }

  ngOnInit(): void {
    // this.getData();
  }

  ngOnDestroy(): void {
    this.unsubscribe.forEach((cb) => cb.unsubscribe());
  }

  getData() {
    this.getDataInProgress = true;
    const payload = new URLSearchParams();
    payload.append('records', '10');
    this.getDataSubscription = this.ws.getLatestKPIsPerVA(payload, this.employeeId).pipe()
      .subscribe(
        (imResponse: LatestKPIS) => {
          this.getDataInProgress = false;
          this.data = imResponse.data;
          if (this.config.isDev) {
            console.log(`RecentKpiHitStatsComponent.getData Completed`);
          }
        },
        (error: HttpErrorResponse) => {
          this.isError = true;
          this.getDataInProgress = false;
          if (this.config.isDev) {
            console.log(`RecentKpiHitStatsComponent.getData Failed`);
          }
        }
      );
    this.unsubscribe.push(this.getDataSubscription);
  }

  returnFullImageUrl(imageName: string) {
    return this.config.restfulApiBase + '/screenshots/' + imageName + '.jpg';
  }

  definePercentageColor(value: number) {
    if (value < 50) {
      return 'bg-danger';
    } else if (value > 50 && value < 65) {
      return 'bg-warning';
    } else {
      return 'bg-success';
    }
  }

  returnColourName(index: number) {
    let colourName = '';
    if (index > this.colourNames.length - 1) {
      index = index - this.colourNames.length - 1;
    }
    colourName = this.colourNames[index];
    return colourName;
  }

  ngOnChanges(changes: SimpleChanges) {
    const employeeId: SimpleChange = changes.employeeId;
    const startDate: SimpleChange = changes.startDate;
    const endDate: SimpleChange = changes.endDate;
    if (employeeId && employeeId.currentValue) {
      this.employeeId = employeeId.currentValue;
      this.getData();
    }
    if (startDate || endDate) {
      if (startDate) {
        this.startDate = startDate.currentValue;
      }
      if (endDate) {
        this.endDate = endDate.currentValue;
      }
      if (this.employeeId) {
        this.getData();
      }
    }
  }

}
