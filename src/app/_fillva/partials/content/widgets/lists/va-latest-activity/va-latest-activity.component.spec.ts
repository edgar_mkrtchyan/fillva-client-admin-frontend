import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VaLatestActivityComponent } from './va-latest-activity.component';

describe('VaLatestActivityComponent', () => {
  let component: VaLatestActivityComponent;
  let fixture: ComponentFixture<VaLatestActivityComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VaLatestActivityComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VaLatestActivityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
