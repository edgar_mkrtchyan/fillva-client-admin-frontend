import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VaActivityPercentageComponent } from './va-activity-percentage.component';

describe('VaActivityPercentageComponent', () => {
  let component: VaActivityPercentageComponent;
  let fixture: ComponentFixture<VaActivityPercentageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VaActivityPercentageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VaActivityPercentageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
