import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnChanges, OnDestroy, OnInit, SimpleChange, SimpleChanges } from '@angular/core';
import { Subscription } from 'rxjs';
import { ActivityPercentage, ActivityPercentageObject } from 'src/app/common/interfaces/activity-percentage.interface';
import { ConfigurationService } from 'src/app/core/services/configuration.service';
import { Constants } from 'src/app/core/services/constants.service';
import { DashboardService } from 'src/app/core/services/dashboard.service';

@Component({
  selector: 'app-va-activity-percentage',
  templateUrl: './va-activity-percentage.component.html',
  styleUrls: ['./va-activity-percentage.component.scss']
})
export class VaActivityPercentageComponent implements OnInit, OnDestroy, OnChanges {

  getDataInProgress: boolean;
  getDataSubscription: Subscription;
  private unsubscribe: Subscription[] = [];
  data: ActivityPercentageObject[];
  @Input() startDate: string;
  @Input() endDate: string;

  constructor(private ws: DashboardService,
              public constants: Constants,
              private config: ConfigurationService) {
    this.getDataInProgress = false;
    this.getDataSubscription = null;
    this.data = [];
  }

  ngOnInit(): void {
    // this.getData();
  }

  ngOnDestroy(): void {
    this.unsubscribe.forEach((cb) => cb.unsubscribe());
  }

  getData() {
    this.getDataInProgress = true;
    const payload = new URLSearchParams();
    payload.append('startTime', this.startDate);
    payload.append('endTime', this.endDate);
    this.getDataSubscription = this.ws.getActivityPercentage(payload).pipe()
      .subscribe(
        (imResponse: ActivityPercentage) => {
          this.getDataInProgress = false;
          this.data = imResponse.data;
          if (this.config.isDev) {
            console.log(`RecentKpiHitStatsComponent.getData Completed`);
          }
        },
        (error: HttpErrorResponse) => {
          if (this.config.isDev) {
            console.log(`RecentKpiHitStatsComponent.getData Failed`);
          }
        }
      );
    this.unsubscribe.push(this.getDataSubscription);
  }

  definePercentageColor(value: number) {
    if (value < 50) {
      return 'bg-danger';
    } else if (value > 50 && value < 65) {
      return 'bg-warning';
    } else {
      return 'bg-success';
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    const startDate: SimpleChange = changes.startDate;
    const endDate: SimpleChange = changes.endDate;
    if (startDate) {
      this.startDate = startDate.currentValue;
    }
    if (endDate) {
      this.endDate = endDate.currentValue;
    }
    this.getData();
  }

}
