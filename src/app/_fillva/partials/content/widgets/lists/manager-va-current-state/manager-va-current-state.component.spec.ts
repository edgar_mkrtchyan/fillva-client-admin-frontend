import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagerVaCurrentStateComponent } from './manager-va-current-state.component';

describe('ManagerVaCurrentStateComponent', () => {
  let component: ManagerVaCurrentStateComponent;
  let fixture: ComponentFixture<ManagerVaCurrentStateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManagerVaCurrentStateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagerVaCurrentStateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
