import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { CommonFunctions } from 'src/app/common/components/common.component';
import { ManagerVACurrentState, ManagerVACurrentStateObject, VACurrentStateObject } from 'src/app/common/interfaces/va-current-state.interface';
import { ConfigurationService } from 'src/app/core/services/configuration.service';
import { Constants } from 'src/app/core/services/constants.service';
import { DashboardService } from 'src/app/core/services/dashboard.service';
import { UserService } from 'src/app/core/services/user.service';
import { VADashboardService } from 'src/app/core/services/va-dashboard.service';

@Component({
  selector: 'app-manager-va-current-state',
  templateUrl: './manager-va-current-state.component.html',
  styleUrls: ['./manager-va-current-state.component.scss']
})
export class ManagerVaCurrentStateComponent implements OnInit, OnDestroy {

  getDataInProgress: boolean;
  getDataSubscription: Subscription;
  data: ManagerVACurrentStateObject[];
  private unsubscribe: Subscription[] = [];
  errorMessage: string;
  chart: any;
  isError: boolean;
  sinceTime: string;

  constructor(private config: ConfigurationService,
              private user: UserService,
              public constants: Constants,
              public common: CommonFunctions,
              private ws: DashboardService) {
    this.getDataInProgress = false;
    this.getDataSubscription = null;
    this.errorMessage = '';
    this.chart = null;
    this.isError = false;
    this.data = [];
    this.sinceTime = '';
  }

  ngOnInit(): void {
    this.getData();
  }

  ngOnDestroy(): void {
    this.unsubscribe.forEach((cb) => cb.unsubscribe());
  }

  getData() {
    this.getDataInProgress = true;
    this.isError = false;
    this.getDataSubscription = this.ws.getCurrentlyActiveVAs().pipe()
      .subscribe(
        (imResponse: ManagerVACurrentState) => {
          this.data = imResponse.data;
          this.getDataInProgress = false;
          if (this.config.isDev) {
            console.log(`ManagerVaCurrentStateComponent.getData Completed`);
          }
        },
        (error: HttpErrorResponse) => {
          this.getDataInProgress = false;
          this.isError = true;
          if (this.config.isDev) {
            console.log(`ManagerVaCurrentStateComponent.getData Failed`);
          }
        }
      );
    this.unsubscribe.push(this.getDataSubscription);
  }

  convertSinceTime(date: number) {
    return new Date(date * 1000).toString();
  }

}
