import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { CommonFunctions } from 'src/app/common/components/common.component';
import { RecentActivity, RecentActivityObject } from 'src/app/common/interfaces/recent-activity.interface';
import { ConfigurationService } from 'src/app/core/services/configuration.service';
import { Constants } from 'src/app/core/services/constants.service';
import { DashboardService } from 'src/app/core/services/dashboard.service';

declare let $: any;

@Component({
  selector: 'app-recent-activity',
  templateUrl: './recent-activity.component.html',
  styleUrls: ['./recent-activity.component.scss']
})
export class RecentActivityComponent implements OnInit, OnDestroy {

  getDataInProgress: boolean;
  getDataSubscription: Subscription;
  private unsubscribe: Subscription[] = [];
  data: RecentActivityObject[];

  constructor(private ws: DashboardService,
              public constants: Constants,
              public common: CommonFunctions,
              private config: ConfigurationService) {
    this.getDataInProgress = false;
    this.getDataSubscription = null;
    this.data = []
  }

  ngOnInit(): void {
    this.getData();
  }

  ngOnDestroy(): void {
    this.unsubscribe.forEach((cb) => cb.unsubscribe());
  }

  getData() {
    this.getDataInProgress = true;
    this.getDataSubscription = this.ws.getRecentActivity().pipe()
      .subscribe(
        (imResponse: RecentActivity) => {
          this.getDataInProgress = false;
          this.data = imResponse.data;
          $.getScript('https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.3/js/lightbox.js', () => { });
          if (this.config.isDev) {
            console.log(`RecentKpiHitStatsComponent.getData Completed`);
          }
        },
        (error: HttpErrorResponse) => {
          if (this.config.isDev) {
            console.log(`RecentKpiHitStatsComponent.getData Failed`);
          }
        }
      );
    this.unsubscribe.push(this.getDataSubscription);
  }

  returnFullImageUrl(imageName: string) {
    return this.config.restfulApiBase + '/screenshots/' + imageName + '.jpg';
  }

  definePercentageColor(value: number) {
    if (value < 50) {
      return 'bg-danger';
    } else if (value > 50 && value < 65) {
      return 'bg-warning';
    } else {
      return 'bg-success';
    }
  }

}
