import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { CommonFunctions } from 'src/app/common/components/common.component';
import { VACurrentState, VACurrentStateObject } from 'src/app/common/interfaces/va-current-state.interface';
import { ConfigurationService } from 'src/app/core/services/configuration.service';
import { Constants } from 'src/app/core/services/constants.service';
import { UserService } from 'src/app/core/services/user.service';
import { VADashboardService } from 'src/app/core/services/va-dashboard.service';

@Component({
  selector: 'app-va-current-state',
  templateUrl: './va-current-state.component.html',
  styleUrls: ['./va-current-state.component.scss']
})
export class VaCurrentStateComponent implements OnInit, OnDestroy {

  getDataInProgress: boolean;
  getDataSubscription: Subscription;
  data: VACurrentStateObject;
  private unsubscribe: Subscription[] = [];
  errorMessage: string;
  chart: any;
  isError: boolean;
  sinceTime: string;

  constructor(private config: ConfigurationService,
              private user: UserService,
              public common: CommonFunctions,
              private ws: VADashboardService) {
    this.getDataInProgress = false;
    this.getDataSubscription = null;
    this.errorMessage = '';
    this.chart = null;
    this.isError = false;
    this.data = null;
    this.sinceTime = '';
  }

  ngOnInit(): void {
    this.getData();
  }

  ngOnDestroy(): void {
    this.unsubscribe.forEach((cb) => cb.unsubscribe());
  }

  getData() {
    this.getDataInProgress = true;
    this.isError = false;
    this.getDataSubscription = this.ws.getCurrentlyActiveState(this.user.userid).pipe()
      .subscribe(
        (imResponse: VACurrentState) => {
          this.data = imResponse.data;
          if (!this.data.inShift) {
            this.errorMessage = 'You are not currently in Shift';
          } else {
            this.sinceTime = new Date(this.data.since * 1000).toString();
          }
          this.getDataInProgress = false;
          if (this.config.isDev) {
            console.log(`RecentKpiHitStatsComponent.getRecentKPIHit Completed`);
          }
        },
        (error: HttpErrorResponse) => {
          this.getDataInProgress = false;
          this.isError = true;
          if (this.config.isDev) {
            console.log(`RecentKpiHitStatsComponent.getRecentKPIHit Failed`);
          }
        }
      );
    this.unsubscribe.push(this.getDataSubscription);
  }

}
