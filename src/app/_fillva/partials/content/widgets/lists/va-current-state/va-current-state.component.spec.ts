import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VaCurrentStateComponent } from './va-current-state.component';

describe('VaCurrentStateComponent', () => {
  let component: VaCurrentStateComponent;
  let fixture: ComponentFixture<VaCurrentStateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VaCurrentStateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VaCurrentStateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
