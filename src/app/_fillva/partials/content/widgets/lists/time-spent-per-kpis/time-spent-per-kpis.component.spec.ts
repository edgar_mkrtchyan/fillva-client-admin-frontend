import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TimeSpentPerKpisComponent } from './time-spent-per-kpis.component';

describe('TimeSpentPerKpisComponent', () => {
  let component: TimeSpentPerKpisComponent;
  let fixture: ComponentFixture<TimeSpentPerKpisComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TimeSpentPerKpisComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TimeSpentPerKpisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
