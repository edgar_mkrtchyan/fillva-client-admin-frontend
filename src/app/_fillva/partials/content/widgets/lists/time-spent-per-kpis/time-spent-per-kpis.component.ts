import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnChanges, OnDestroy, OnInit, SimpleChange, SimpleChanges } from '@angular/core';
import { Subscription } from 'rxjs';
import { TimeSpentPerKPI, TimeSpentPerKPIObject } from 'src/app/common/interfaces/time-spent-per-kpi.interface';
import { BusinessInsightsService } from 'src/app/core/services/business-insights.service';
import { ConfigurationService } from 'src/app/core/services/configuration.service';

@Component({
  selector: 'app-time-spent-per-kpis',
  templateUrl: './time-spent-per-kpis.component.html',
  styleUrls: ['./time-spent-per-kpis.component.scss']
})
export class TimeSpentPerKpisComponent implements OnInit, OnDestroy, OnChanges {

  @Input() startDate: string;
  @Input() endDate: string;
  @Input() employeeId: any;
  getDataInProgress: boolean;
  getDataSubscription: Subscription;
  private unsubscribe: Subscription[] = [];
  data: TimeSpentPerKPIObject[];
  isError: boolean;

  constructor(private ws: BusinessInsightsService,
              private config: ConfigurationService) {
    this.data = [];
    this.isError = false;
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.unsubscribe.forEach((cb) => cb.unsubscribe());
  }

  getData() {
    this.isError = false;
    this.getDataInProgress = true;
    const payload = new URLSearchParams();
    payload.append('startTime', this.startDate);
    payload.append('endTime', this.endDate);
    if (this.employeeId !== 1) {
      payload.append('userId', this.employeeId);
    }
    this.getDataSubscription = this.ws.getTimeSpentPerKPI(payload).pipe()
      .subscribe(
        (imResponse: TimeSpentPerKPI) => {
          this.getDataInProgress = false;
          this.data = imResponse.data;
          if (this.config.isDev) {
            console.log(`RecentKpiHitStatsComponent.getData Completed`);
          }
        },
        (error: HttpErrorResponse) => {
          this.getDataInProgress = false;
          this.isError = true;
          if (this.config.isDev) {
            console.log(`RecentKpiHitStatsComponent.getData Failed`);
          }
        }
      );
    this.unsubscribe.push(this.getDataSubscription);
  }

  ngOnChanges(changes: SimpleChanges) {
    const employeeId: SimpleChange = changes.employeeId;
    const startDate: SimpleChange = changes.startDate;
    const endDate: SimpleChange = changes.endDate;
    if (employeeId && employeeId.currentValue) {
      this.employeeId = employeeId.currentValue;
      this.getData();
    }
    if (startDate || endDate) {
      if (startDate) {
        this.startDate = startDate.currentValue;
      }
      if (endDate) {
        this.endDate = endDate.currentValue;
      }
      if (this.employeeId) {
        this.getData();
      }
    }
  }

}
