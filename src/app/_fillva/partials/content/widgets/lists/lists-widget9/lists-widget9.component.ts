import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { Subscription } from 'rxjs';
import { CommonFunctions } from 'src/app/common/components/common.component';
import { LatestKPIS, LatestKPISObject } from 'src/app/common/interfaces/latest-kpis.interface';
import { RecentActivityObject } from 'src/app/common/interfaces/recent-activity.interface';
import { ConfigurationService } from 'src/app/core/services/configuration.service';
import { Constants } from 'src/app/core/services/constants.service';
import { DashboardService } from 'src/app/core/services/dashboard.service';

@Component({
  selector: 'app-lists-widget9',
  templateUrl: './lists-widget9.component.html',
  styleUrls: ['./lists-widget9.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ListsWidget9Component implements OnInit, OnDestroy {
  getDataInProgress: boolean;
  getDataSubscription: Subscription;
  private unsubscribe: Subscription[] = [];
  data: LatestKPISObject[];
  colourNames: Array<string>;

  constructor(private ws: DashboardService,
              public constants: Constants,
              public common: CommonFunctions,
              private config: ConfigurationService) {
    this.getDataInProgress = false;
    this.getDataSubscription = null;
    this.data = [];
    this.colourNames = ['text-warning', 'text-success', 'text-danger', 'text-primary'];
  }

  ngOnInit(): void {
    this.getData();
  }

  ngOnDestroy(): void {
    this.unsubscribe.forEach((cb) => cb.unsubscribe());
  }

  getData() {
    this.getDataInProgress = true;
    this.getDataSubscription = this.ws.getLatestKPIs(10).pipe()
      .subscribe(
        (imResponse: LatestKPIS) => {
          this.getDataInProgress = false;
          this.data = imResponse.data;
          if (this.config.isDev) {
            console.log(`RecentKpiHitStatsComponent.getData Completed`);
          }
        },
        (error: HttpErrorResponse) => {
          if (this.config.isDev) {
            console.log(`RecentKpiHitStatsComponent.getData Failed`);
          }
        }
      );
    this.unsubscribe.push(this.getDataSubscription);
  }

  returnFullImageUrl(imageName: string) {
    return this.config.restfulApiBase + '/screenshots/' + imageName + '.jpg';
  }

  definePercentageColor(value: number) {
    if (value < 50) {
      return 'bg-danger';
    } else if (value > 50 && value < 65) {
      return 'bg-warning';
    } else {
      return 'bg-success';
    }
  }

  returnColourName(index: number) {
    let colourName = '';
    if (index > this.colourNames.length - 1) {
      index = index - this.colourNames.length - 1;
    }
    colourName = this.colourNames[index];
    return colourName;
  }
}
