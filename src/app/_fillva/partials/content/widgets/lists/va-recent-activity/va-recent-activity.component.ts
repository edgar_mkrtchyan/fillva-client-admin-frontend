import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnChanges, OnDestroy, OnInit, SimpleChange, SimpleChanges } from '@angular/core';
import { Subscription } from 'rxjs';
import { CommonFunctions } from 'src/app/common/components/common.component';
import { RecentActivity, RecentActivityObject } from 'src/app/common/interfaces/recent-activity.interface';
import { ConfigurationService } from 'src/app/core/services/configuration.service';
import { DashboardService } from 'src/app/core/services/dashboard.service';
import { ProductivityService } from 'src/app/core/services/productivity.service';

declare let $: any;

@Component({
  selector: 'app-va-recent-activity',
  templateUrl: './va-recent-activity.component.html',
  styleUrls: ['./va-recent-activity.component.scss']
})
export class VaRecentActivityComponent implements OnInit, OnDestroy, OnChanges {

  @Input() startDate: string;
  @Input() endDate: string;
  @Input() employeeId: number;
  getDataInProgress: boolean;
  getDataSubscription: Subscription;
  private unsubscribe: Subscription[] = [];
  data: RecentActivityObject[];
  colourNames: Array<string>;

  constructor(private ws: ProductivityService,
              public common: CommonFunctions,
              private config: ConfigurationService) {
    this.data = [];
    this.colourNames = ['text-warning', 'text-success', 'text-danger', 'text-primary'];
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.unsubscribe.forEach((cb) => cb.unsubscribe());
  }

  getData() {
    const payload = new URLSearchParams();
    payload.append('startTime', this.startDate);
    payload.append('endTime', this.endDate);
    payload.append('type', 'productivity');
    this.getDataInProgress = true;
    this.getDataSubscription = this.ws.getRecentActivity(payload, this.employeeId).pipe()
      .subscribe(
        (imResponse: RecentActivity) => {
          this.getDataInProgress = false;
          this.data = imResponse.data;
          // Splitting into chunks of 6
          this.data = this.chunks(this.data, 6);
          $.getScript('https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.3/js/lightbox.js', () => { });
          if (this.config.isDev) {
            console.log(`VaRecentActivityComponent.getData Completed`);
          }
        },
        (error: HttpErrorResponse) => {
          if (this.config.isDev) {
            console.log(`VaRecentActivityComponent.getData Failed`);
          }
        }
      );
    this.unsubscribe.push(this.getDataSubscription);
  }

  ngOnChanges(changes: SimpleChanges) {
    const employeeId: SimpleChange = changes.employeeId;
    const startDate: SimpleChange = changes.startDate;
    const endDate: SimpleChange = changes.endDate;
    if (employeeId && employeeId.currentValue) {
      this.employeeId = employeeId.currentValue;
      this.getData();
    }
    if (startDate || endDate) {
      if (startDate) {
        this.startDate = startDate.currentValue;
      }
      if (endDate) {
        this.endDate = endDate.currentValue;
      }
      if (this.employeeId) {
        this.getData();
      }
    }
  }

  returnFullImageUrl(imageName: string) {
    return this.config.restfulApiBase + '/screenshots/' + imageName + '.jpg';
  }

  definePercentageColor(value: number) {
    if (value < 50) {
      return 'bg-danger';
    } else if (value > 50 && value < 65) {
      return 'bg-warning';
    } else {
      return 'bg-success';
    }
  }

  returnColourName(index: number) {
    let colourName = '';
    if (index > this.colourNames.length - 1) {
      index = index - this.colourNames.length - 1;
    }
    colourName = this.colourNames[index];
    return colourName;
  }

  chunks(array, size) {
    let results = [];
    results = [];
    while (array.length) {
      results.push(array.splice(0, size));
    }
    return results;
  }

}
