import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VaRecentActivityComponent } from './va-recent-activity.component';

describe('VaRecentActivityComponent', () => {
  let component: VaRecentActivityComponent;
  let fixture: ComponentFixture<VaRecentActivityComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VaRecentActivityComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VaRecentActivityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
