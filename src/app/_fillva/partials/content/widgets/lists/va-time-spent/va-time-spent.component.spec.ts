import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VaTimeSpentComponent } from './va-time-spent.component';

describe('VaTimeSpentComponent', () => {
  let component: VaTimeSpentComponent;
  let fixture: ComponentFixture<VaTimeSpentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VaTimeSpentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VaTimeSpentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
