import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnChanges, OnDestroy, OnInit, SimpleChange, SimpleChanges } from '@angular/core';
import { Subscription } from 'rxjs';
import { TimeSpent, TimeSpentObject } from 'src/app/common/interfaces/time-spent.interface';
import { ConfigurationService } from 'src/app/core/services/configuration.service';
import { Constants } from 'src/app/core/services/constants.service';
import { DashboardService } from 'src/app/core/services/dashboard.service';

@Component({
  selector: 'app-va-time-spent',
  templateUrl: './va-time-spent.component.html',
  styleUrls: ['./va-time-spent.component.scss']
})
export class VaTimeSpentComponent implements OnInit, OnDestroy, OnChanges {

  @Input() startDate: string;
  @Input() endDate: string;
  getDataInProgress: boolean;
  getDataSubscription: Subscription;
  private unsubscribe: Subscription[] = [];
  data: TimeSpentObject[];

  constructor(private ws: DashboardService,
              public constants: Constants,
              private config: ConfigurationService) {
    this.getDataInProgress = false;
    this.getDataSubscription = null;
    this.data = [];
  }

  ngOnInit(): void {
    // this.getData();
  }

  ngOnDestroy(): void {
    this.unsubscribe.forEach((cb) => cb.unsubscribe());
  }

  getData() {
    const payload = new URLSearchParams();
    payload.append('startTime', this.startDate);
    payload.append('endTime', this.endDate);
    this.getDataInProgress = true;
    this.getDataSubscription = this.ws.getProductivityData(payload).pipe()
      .subscribe(
        (imResponse: TimeSpent) => {
          this.getDataInProgress = false;
          this.data = imResponse.data;
          if (this.config.isDev) {
            console.log(`RecentKpiHitStatsComponent.getData Completed`);
          }
        },
        (error: HttpErrorResponse) => {
          if (this.config.isDev) {
            console.log(`RecentKpiHitStatsComponent.getData Failed`);
          }
        }
      );
    this.unsubscribe.push(this.getDataSubscription);
  }

  definePercentageColor(value: number) {
    if (value < 50) {
      return 'bg-danger';
    } else if (value > 50 && value < 65) {
      return 'bg-warning';
    } else {
      return 'bg-success';
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    const startDate: SimpleChange = changes.startDate;
    const endDate: SimpleChange = changes.endDate;
    if (startDate) {
      this.startDate = startDate.currentValue;
    }
    if (endDate) {
      this.endDate = endDate.currentValue;
    }
    this.getData();
  }

}
