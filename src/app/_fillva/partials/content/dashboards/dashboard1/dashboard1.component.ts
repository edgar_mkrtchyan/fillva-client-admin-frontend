import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, Subscription } from 'rxjs';
import { CommonFunctions } from 'src/app/common/components/common.component';
import { BaseMetrics, BaseMetricsObject } from 'src/app/common/interfaces/base-metrics.interface';
import { DateObject } from 'src/app/common/interfaces/date.interface';
import { ConfigurationService } from 'src/app/core/services/configuration.service';
import { DashboardService } from 'src/app/core/services/dashboard.service';
import { UserService } from 'src/app/core/services/user.service';
import { AuthService, UserModel } from 'src/app/modules/auth';
import { WidgetsManagementModalComponent } from 'src/app/pages/widgets-management-modal/widgets-management-modal.component';

declare let moment: any;
declare let $: any;

@Component({
  selector: 'app-dashboard1',
  templateUrl: './dashboard1.component.html',
  styleUrls: ['./dashboard1.component.scss']
})
export class Dashboard1Component implements OnInit, OnDestroy {

  widgetDownloaded: boolean;
  user$: Observable<UserModel>;
  selectedWidgetsToShow: Array<string>;
  private unsubscribe: Subscription[] = [];
  getBaseMetricsInProgress: boolean;
  getBaseMetricsSubscription: Subscription;
  baseMetrics: BaseMetricsObject;
  selectedStartDate: string;
  selectedEndDate: string;

  constructor(public common: CommonFunctions,
              private config: ConfigurationService,
              private modalService: NgbModal,
              private ws: DashboardService,
              private userService: UserService,
              private auth: AuthService) {
    this.widgetDownloaded = false;
    this.user$ = this.auth.currentUserSubject.asObservable();
    this.selectedWidgetsToShow = this.getSavedWidgets();
    this.unsubscribe = [];
    this.getBaseMetricsInProgress = false;
    this.getBaseMetricsSubscription = null;
    this.baseMetrics = null;
    this.selectedStartDate = moment().subtract(6, 'days').startOf('day');
    this.selectedEndDate = moment().endOf('day');
    this.selectedStartDate = Math.floor(parseInt(this.selectedStartDate.valueOf(), 10) / 1000).toString();
    this.selectedEndDate = Math.floor(parseInt(this.selectedEndDate.valueOf(), 10) / 1000).toString();
  }

  ngOnInit(): void {
    if (this.common.isManager()) {
      this.getBaseMetrics();
    } else if (this.common.isVA()) {
      this.getBaseMetricsForVA();
    }
  }

  ngOnDestroy(): void {
    this.unsubscribe.forEach((cb) => cb.unsubscribe());
  }

  downloadWidget() {
    this.widgetDownloaded = true;
  }

  generateExtensionDownloadLink() {
    if (this.config.isDev) {
      return '/assets/extension/fillva-chrome-extension-dev.zip';
    } else {
      return 'https://chrome.google.com/webstore/detail/fillva/hcdlhhbojjpjfipjncodkmjobolaglmj';
    }
  }

  getSavedWidgets() {
    if (localStorage.getItem('selected_widgets')) {
      return JSON.parse(localStorage.getItem('selected_widgets'));
    } else {
      return [];
    }
  }

  isWidgetSelectedToBeShown(key: string) {
    if (this.selectedWidgetsToShow.length > 0) {
      return this.selectedWidgetsToShow.indexOf(key) > -1;
    } else {
      return true;
    }
  }

  defineClasses(classes: Array<string>) {
    let matches = 0;
    classes.forEach(cl => {
      if (this.selectedWidgetsToShow.indexOf(cl) > - 1) {
        matches++;
      }
    });
    switch (matches) {
      case 1:
        return 'col-g-12 col-xxl-12';
        break;
      case 2:
        return 'col-g-6 col-xxl-6';
        break;
      case 3:
        return 'col-g-4 col-xxl-4';
        break;
    }
  }

  openManageWidgetsModal() {
    const modalRef = this.modalService.open(WidgetsManagementModalComponent);

    modalRef.result.then((res) => {
      if (res === 'refresh') {

      }
    });
  }

  getBaseMetrics() {
    const payload = new URLSearchParams();
    payload.append('startTime', this.selectedStartDate);
    payload.append('endTime', this.selectedEndDate);
    this.getBaseMetricsInProgress = true;
    this.getBaseMetricsSubscription = this.ws.getBaseMetrics(payload).pipe()
      .subscribe(
        (imResponse: BaseMetrics) => {
          this.getBaseMetricsInProgress = false;
          this.baseMetrics = imResponse.metrics;
          if (this.config.isDev) {
            console.log(`Dashboard1Component.getBaseMetrics Completed`);
          }
        },
        (error: HttpErrorResponse) => {
          if (this.config.isDev) {
            console.log(`Dashboard1Component.getBaseMetrics Failed`);
          }
        }
      );
    this.unsubscribe.push(this.getBaseMetricsSubscription);
  }

  getBaseMetricsForVA() {
    const payload = new URLSearchParams();
    payload.append('startTime', this.selectedStartDate);
    payload.append('endTime', this.selectedEndDate);
    this.getBaseMetricsInProgress = true;
    let apiurl = this.ws.getBaseMetrics(payload).pipe();
    if (this.common.isVA()) {
      apiurl = this.ws.getBaseMetricsForVA(payload, this.userService.userid).pipe();
    }
    this.getBaseMetricsSubscription = apiurl
      .subscribe(
        (imResponse: BaseMetrics) => {
          this.getBaseMetricsInProgress = false;
          this.baseMetrics = imResponse.metrics;
          if (this.config.isDev) {
            console.log(`Dashboard1Component.getBaseMetrics Completed`);
          }
        },
        (error: HttpErrorResponse) => {
          if (this.config.isDev) {
            console.log(`Dashboard1Component.getBaseMetrics Failed`);
          }
        }
      );
    this.unsubscribe.push(this.getBaseMetricsSubscription);
  }

  dateChanged($event: DateObject) {
    this.selectedStartDate = $event.startTime;
    this.selectedEndDate = $event.endTime;
    this.selectedStartDate = Math.floor(parseInt(this.selectedStartDate.valueOf(), 10) / 1000).toString();
    this.selectedEndDate = Math.floor(parseInt(this.selectedEndDate.valueOf(), 10) / 1000).toString();
    if (this.common.isManager()) {
      this.getBaseMetrics();
    } else if (this.common.isVA()) {
      this.getBaseMetricsForVA();
    }
  }

  definePositionOrNagative(value: string) {
    if (value.indexOf('-') > -1) {
      return 'symbol-light-danger';
    } else {
      return 'symbol-light-success';
    }
  }

  defineArrowDirection(value: string) {
    if (value.indexOf('-') > -1) {
      return '&darr;';
    } else {
      return '&uarr;';
    }
  }

  differenceAvailable(value: string) {
    if (value === '00:00' || value === '0') {
      return false;
    } else {
      return true;
    }
  }
}
