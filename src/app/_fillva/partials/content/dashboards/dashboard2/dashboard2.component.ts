import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DateObject } from 'src/app/common/interfaces/date.interface';
import { UserObject } from 'src/app/common/interfaces/user.interface';
import { WidgetsManagementModalComponent } from 'src/app/pages/widgets-management-modal/widgets-management-modal.component';

declare let moment: any;
declare let $: any;

@Component({
  selector: 'app-dashboard2',
  templateUrl: './dashboard2.component.html',
})
export class Dashboard2Component implements OnInit {

  selectedStartDate: string;
  selectedEndDate: string;
  selectedWidgetsToShow: Array<string>;
  selectedEmployee: UserObject;
  selectedEmployeeId: number;

  constructor(private modalService: NgbModal) {
    this.selectedStartDate = moment().subtract(6, 'days').startOf('day');
    this.selectedEndDate = moment().endOf('day');
    this.selectedStartDate = Math.floor(parseInt(this.selectedStartDate.valueOf(), 10) / 1000).toString();
    this.selectedEndDate = Math.floor(parseInt(this.selectedEndDate.valueOf(), 10) / 1000).toString();
    this.selectedEmployee = null;
  }

  ngOnInit(): void {
    this.selectedWidgetsToShow = this.getSavedWidgets();
  }

  dateChanged($event: DateObject) {
    this.selectedStartDate = $event.startTime;
    this.selectedEndDate = $event.endTime;
    this.selectedStartDate = Math.floor(parseInt(this.selectedStartDate.valueOf(), 10) / 1000).toString();
    this.selectedEndDate = Math.floor(parseInt(this.selectedEndDate.valueOf(), 10) / 1000).toString();
  }

  openManageWidgetsModal() {
    const modalRef = this.modalService.open(WidgetsManagementModalComponent);

    modalRef.result.then((res) => {
      if (res === 'refresh') {

      }
    });
  }

  isWidgetSelectedToBeShown(key: string) {
    if (this.selectedWidgetsToShow.length > 0) {
      return this.selectedWidgetsToShow.indexOf(key) > -1;
    } else {
      return true;
    }
  }

  getSavedWidgets() {
    if (localStorage.getItem('selected_widgets')) {
      return JSON.parse(localStorage.getItem('selected_widgets'));
    } else {
      return [];
    }
  }

  defineClasses(classes: Array<string>) {
    let matches = 0;
    classes.forEach(cl => {
      if (this.selectedWidgetsToShow.indexOf(cl) > - 1) {
        matches++;
      }
    });
    switch (matches) {
      case 1:
        return 'col-g-12 col-xxl-12';
        break;
      case 2:
        return 'col-g-6 col-xxl-6';
        break;
      case 3:
        return 'col-g-4 col-xxl-4';
        break;
    }
  }

  perVA($event: any) {

  }

  selectEmployee(employee: UserObject) {
    if (employee === 1) {
      this.selectedEmployeeId = 1;
    } else {
      this.selectedEmployeeId = employee.id;
    }
  }
}
