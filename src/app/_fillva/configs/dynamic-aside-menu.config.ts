export const DynamicAsideMenuConfig = {
  items: [
    {
      title: 'Dashboard',
      root: true,
      icon: 'flaticon2-architecture-and-city',
      page: '/dashboard',
      bullet: 'dot',
    },
    {
      title: 'Business Insights',
      root: true,
      icon: 'flaticon-business',
      page: '/business-insights',
      bullet: 'dot',
      visible: ['ADMIN', 'MANAGER']
    },
    {
      title: 'Productivity',
      root: true,
      icon: 'flaticon-statistics',
      page: '/productivity',
      bullet: 'dot',
      visible: ['ADMIN', 'MANAGER']
    },
    {
      title: 'Track My Team',
      root: true,
      icon: 'flaticon-dashboard',
      page: '/track-my-team',
      bullet: 'dot',
      visible: ['MANAGER']
    },
    {
      title: 'Screenshots',
      root: true,
      icon: 'flaticon-tabs',
      page: '/screenshots',
      bullet: 'dot',
      visible: ['ADMIN', 'MANAGER']
    },
    {
      title: 'Finance',
      root: true,
      icon: 'flaticon-coins',
      page: '/finance',
      bullet: 'dot',
      visible: ['ADMIN', 'MANAGER'],
      demo: true
    },
    {
      title: 'Finance',
      root: true,
      icon: 'flaticon-coins',
      page: '/employee/finance',
      bullet: 'dot',
      visible: ['VA']
    },
    {
      section: 'Administration',
      visible: ['ADMIN']
    },
    {
      title: 'Settings',
      root: true,
      icon: 'flaticon-settings',
      bullet: 'dot',
      visible: ['ADMIN', 'MANAGER'],
      submenu: [
        {
          title: 'User Management',
          root: true,
          page: '/user-management',
          bullet: 'dot',
          visible: ['ADMIN', 'MANAGER']
        },
        {
          title: 'Teams',
          root: true,
          icon: 'flaticon-presentation',
          page: '/manager/teams',
          bullet: 'dot',
          visible: ['MANAGER']
        },
        {
          title: 'Custom KPI Events',
          root: true,
          page: '/manager/settings/custom-kpi-events',
          bullet: 'dot',
          visible: ['MANAGER']
        }
      ]
    },
    {
      title: 'Install Extension',
      root: true,
      icon: 'flaticon2-download-1',
      page: '/assets/extension.zip',
      bullet: 'dot',
      download: true
    }
  ]
};
